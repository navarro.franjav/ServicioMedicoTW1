package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroMedicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioMedico;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioUsuario;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServicioMedicoTest {
    RepositorioMedico repositorioMedico = mock(RepositorioMedico.class);
    RepositorioUsuario repositorioUsuario = mock(RepositorioUsuario.class);
    ServicioMedico servicioMedico = new ServicioMedicoImpl(repositorioMedico, repositorioUsuario);

    @Test
    public void siElIdMedicoCorrespondeAUnMedicoExistenteTraerMedico(){
        Long idMedico = 2L;
        when(repositorioMedico.buscarMedicoPorElId(idMedico)).thenReturn(new Medico());
        Medico medico = whenBuscoMedicoPorId(idMedico);
        thenMedicoGuardadoExitosamente(medico);
    }

    private Medico whenBuscoMedicoPorId(Long idMedico) {
        return servicioMedico.buscarPorId(idMedico);
    }

    private void thenMedicoGuardadoExitosamente(Medico medico) {
        assertThat(medico).isNotNull();
    }

}
