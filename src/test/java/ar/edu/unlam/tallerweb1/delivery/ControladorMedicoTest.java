package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ControladorMedicoTest {

    ServicioMedico servicioMedico = mock(ServicioMedico.class);
    ServicioEspecialidad servicioEspecialidad = mock(ServicioEspecialidad.class);
    ServicioLogin servicioLogin = mock(ServicioLogin.class);
    ServicioTurno servicioTurno = mock(ServicioTurno.class);
    ServicioEstudio servicioEstudio = mock(ServicioEstudio.class);
    ServicioRegistro servicioRegistro = mock(ServicioRegistro.class);
    ServicioPaciente servicioPaciente = mock(ServicioPaciente.class);
    HttpServletRequest request = new MockHttpServletRequest();

    ControladorMedico controladorMedico = new ControladorMedico(servicioMedico, servicioEspecialidad, servicioLogin
                                            , servicioTurno, servicioEstudio, servicioRegistro, servicioPaciente);

    @Test
    public void siElUsuarioTieneElRolMedicoIrAHomeMedico(){
        ModelAndView mav = whenIngresoConUnUsuarioConRolMedico();
        thenElIngresoEsExitoso(mav);
    }

    private void thenElIngresoEsExitoso(ModelAndView mav) {

        assertThat(mav.getViewName()).isEqualTo("Medico/home-medico");
    }

    private ModelAndView whenIngresoConUnUsuarioConRolMedico() {
        this.request.setAttribute("ROL", 2);
        this.request.setAttribute("ID", 1);
        Medico medico = new Medico();
        medico.setId(1L);
        when(servicioMedico.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"))).thenReturn(medico);

        return controladorMedico.irAHomeMedico(request);
    }

    @Test
    public void siElUsuarioTieneUnRolQueNoEsMedicoQueLoRedirigaAlHome() throws AccesoDenegadoException {
        this.request.setAttribute("ROL", 3);
        doThrow(AccesoDenegadoException.class).when(servicioLogin).esMedico(request);

        ModelAndView mav = whenIngresoConUnUsuarioConUnRolQueNoEsMedico(request);
        thenElUsuarioEsRedirigidoAlHome(mav);
    }

    private void thenElUsuarioEsRedirigidoAlHome(ModelAndView mav) {
        assertThat(mav.getViewName()).isEqualTo("redirect:/home");
    }

    private ModelAndView whenIngresoConUnUsuarioConUnRolQueNoEsMedico(HttpServletRequest request){

        return controladorMedico.irAHomeMedico(request);
    }
}
