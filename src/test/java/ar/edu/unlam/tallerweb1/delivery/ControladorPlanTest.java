package ar.edu.unlam.tallerweb1.delivery;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioEspecialidad;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioLogin;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlan;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlanEspecialidad;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class ControladorPlanTest {
    ServicioLogin servicioLogin= mock(ServicioLogin.class);
    ServicioEspecialidad servicioEspecialidad= mock(ServicioEspecialidad.class);
    ServicioPlanEspecialidad servicioPlanEspecialidad= mock(ServicioPlanEspecialidad.class);
    HttpServletRequest request = new MockHttpServletRequest();
    ServicioPlan servicioPlan = mock(ServicioPlan.class);
    ControladorPlan controladorPlan = new ControladorPlan(servicioPlan, servicioLogin, servicioEspecialidad, servicioPlanEspecialidad);
    @Test
    public void siElPlanNoExisteQueSePuedaAgregar(){
        DatosPlanDTO datosPlan= datosPlan();
        ModelAndView mav=whenCreoPlan(datosPlan);
        thenElRegistroEsExitoso(mav);
    }

    private DatosPlanDTO datosPlan() {
        DatosPlanDTO datosPlan=new DatosPlanDTO();
        datosPlan.setId(1L);
        datosPlan.setNombre("Plan Test 200");
        datosPlan.setDescripcion("plan prueba");
        datosPlan.setPrecio(200.0);
        List<Long> listaIdEspecialidades = new ArrayList<>();
        listaIdEspecialidades.addAll(Arrays.asList(1L, 2L, 3L));
        datosPlan.setListaEspecialidadesSeleccionadas(listaIdEspecialidades);
        return datosPlan;
    }

    private ModelAndView whenCreoPlan(DatosPlanDTO datosPlan) {
        this.request.setAttribute("ROL", 1);
        return this.controladorPlan.registrarPlan(datosPlan, request);
    }
    private void thenElRegistroEsExitoso(ModelAndView mav) {
        assertThat(mav.getViewName()).isEqualTo("redirect:/lista-planes");
    }


}