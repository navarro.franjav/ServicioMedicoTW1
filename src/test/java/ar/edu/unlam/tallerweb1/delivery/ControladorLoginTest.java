//package ar.edu.unlam.tallerweb1.delivery;
//
//import ar.edu.unlam.tallerweb1.domain.servicios.serviciologin;
//import ar.edu.unlam.tallerweb1.domain.usuarios.usuario;
//import org.junit.before;
//import org.junit.test;
//import org.springframework.web.servlet.modelandview;
//import javax.servlet.http.httpservletrequest;
//import javax.servlet.http.httpsession;
//import static org.mockito.mockito.*;
//import static org.assertj.core.api.assertions.assertthat;
//
//public class controladorlogintest {
//    private serviciologin serviciologin;
//    private httpservletrequest request;
//    private httpsession sesion;
//    private controladorlogin controladorlogin;
//
//    @before
//    public void init() {
//        serviciologin = mock(serviciologin.class);
//        sesion = mock(httpsession.class);
//        request = mock(httpservletrequest.class);
//        controladorlogin = new controladorlogin(this.serviciologin);
//    }
//
//    @test
//    public void dadounusuarioexistentequesepuedainiciarsesion() {
//
//        string rol = "admin";
//
//        datoslogin datoslogin = dadoquetengodatosdeloginvalidos();
//        usuario usuarioesperado = dadoquetengounusuarioconrol(rol);
//
//        modelandview vista = cuandoquierovalidarellogin(datoslogin, usuarioesperado, rol);
//
//        entoncesmedevuelvelavistacorrecta(vista);
//
//        entoncesiniciosesion(rol);
//    }
//
//    //cuando
//
//    private datoslogin dadoquetengodatosdeloginvalidos() {
//        return new datoslogin();
//    }
//
//    private usuario dadoquetengounusuarioconrol(string rol) {
//        usuario usuario = new usuario();
//        usuario.setrol(rol);
//        return usuario;
//    }
//
//    //dado
//    private modelandview cuandoquierovalidarellogin(datoslogin datoslogin, usuario usuarioesperado,string rol) {
//        when(serviciologin.consultarusuario(any(), any())).thenreturn(usuarioesperado);
//        when(request.getsession()).thenreturn(sesion);
//        when(sesion.getattribute("rol")).thenreturn(rol);
//        return controladorlogin.validarlogin(datoslogin, request);
//    }
//
//    //entonces
//    private static void entoncesmedevuelvelavistacorrecta(modelandview vista) {
//        assertthat(vista.getviewname()).isequalto("redirect:/home");
//    }
//    private void entoncesiniciosesion(string rol) {
//        assertthat(sesion.getattribute("rol")).isequalto(rol);
//    }
//}
