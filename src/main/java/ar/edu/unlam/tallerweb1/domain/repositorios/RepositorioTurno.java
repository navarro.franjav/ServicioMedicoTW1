package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface RepositorioTurno {
    List<Turno> listar();

    void guardar(Turno nuevoTurno);
    void eliminarTurnosPorElMedicoHospitalId(Long idMedicoHospital);

    List<Turno> listarDisponibles();

    Turno buscarPorId(Long turnoId);

    void asignar(Turno turno);

    List<Turno> turnosPaciente(Paciente paciente);

    List<Turno> proximosTurnosPaciente(Paciente paciente) ;

    void update(Turno turno);

    List<Turno> listarTurnosConPacienteAsignadoSegunLaFecha(LocalDate fecha);

    List<Turno> listarTurnosAsignadosPorIdMedicoYFecha(Long idMedico, LocalDate fecha);

    List<Turno> listarTUrnosAsignadosPorIdMedico(Long idMedico);
    List<Turno> listarTurnosdelDia(LocalDate fechaDehoy);

    List<Paciente> listarPacientePorIdMedico(Medico medico);

    Turno buscarPorFechaHoraYMedicoHospital(LocalDate fecha, LocalTime hora, Medico_Hospital medicoHospital);

    List<Turno> obtenerTurnosPorMedicoHospitalId(Long idMedicoHospital);
}
