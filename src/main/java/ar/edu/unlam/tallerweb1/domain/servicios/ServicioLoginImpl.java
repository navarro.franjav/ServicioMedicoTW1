package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioUsuario;
import ar.edu.unlam.tallerweb1.domain.clases.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

// Implelemtacion del Servicio de usuarios, la anotacion @Service indica a Spring que esta clase es un componente que debe
// ser manejado por el framework, debe indicarse en applicationContext que busque en el paquete ar.edu.unlam.tallerweb1.servicios
// para encontrar esta clase.
// La anotacion @Transactional indica que se debe iniciar una transaccion de base de datos ante la invocacion de cada metodo del servicio,
// dicha transaccion esta asociada al transaction manager definido en el archivo spring-servlet.xml y el mismo asociado al session factory definido
// en hibernateCOntext.xml. De esta manera todos los metodos de cualquier dao invocados dentro de un servicio se ejecutan en la misma transaccion
@Service("servicioLogin")
@Transactional
public class ServicioLoginImpl implements ServicioLogin {

	private RepositorioUsuario servicioLoginDao;

	@Autowired
	public ServicioLoginImpl(RepositorioUsuario servicioLoginDao){
		this.servicioLoginDao = servicioLoginDao;
	}

	@Override
	public Usuario consultarUsuario (String email, String password) {
		return servicioLoginDao.buscarUsuario(email, password);
	}

	@Override
	public void esAdmin(HttpServletRequest request) throws AccesoDenegadoException {
		if (esNulo(request)||!request.getSession().getAttribute("ROL").equals(1)) {
			throw new AccesoDenegadoException("El usuario no tiene permisos de administrador.");
		}
	}

	@Override
	public void esMedico(HttpServletRequest request) throws AccesoDenegadoException {
		if(esNulo(request)||!request.getSession().getAttribute("ROL").equals(2)){
			throw new AccesoDenegadoException("El usuario no tiene permisos de médico");
		}
	}

	@Override
	public void esPaciente(HttpServletRequest request) throws AccesoDenegadoException {
		if(esNulo(request)||!request.getSession().getAttribute("ROL").equals(3)){
			throw new AccesoDenegadoException("El usuario no tiene permisos de paciente");
		}
	}

	@Override
	public void esRecepcionista(HttpServletRequest request) throws AccesoDenegadoException {
		if (esNulo(request)||!request.getSession().getAttribute("ROL").equals(4)){
			throw new AccesoDenegadoException("El usuario no tiene permisos de recepcionista");
		}
	}


	private boolean esNulo(HttpServletRequest request) {
		return request.getSession().getAttribute("ROL")==null;
	}
}
