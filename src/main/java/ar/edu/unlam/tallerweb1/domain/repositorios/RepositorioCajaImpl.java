package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RepositorioCajaImpl implements RepositorioCaja{
    private SessionFactory sessionFactory;

    @Override
    public void guardarCaja(Caja caja) {
            this.sessionFactory.getCurrentSession().save(caja);
    }
}
