package ar.edu.unlam.tallerweb1.domain.servicios;

import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;

public interface ServicioEmail {
    void enviarEmail(JavaMailSender javaMailSender, String mail, String reminder, String contenidoDelEmail) throws MessagingException;
}
