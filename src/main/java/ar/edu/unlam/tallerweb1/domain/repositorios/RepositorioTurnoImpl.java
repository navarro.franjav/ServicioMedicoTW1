package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.criterion.Restrictions;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository("repositorioTurno")

public class RepositorioTurnoImpl implements RepositorioTurno {
    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioTurnoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Turno> listar() {
        LocalDate fecha = LocalDate.now();
        String hql = "FROM Turno t WHERE t.fecha >= :fecha order by t.fecha";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("fecha", fecha)
                .getResultList();
    }

    @Override
    public void guardar(Turno nuevoTurno) {
        sessionFactory.getCurrentSession().save(nuevoTurno);
    }

    @Override
    public void eliminarTurnosPorElMedicoHospitalId(Long idMedicoHospital) {
        LocalDate fechaDeHoy = LocalDate.now();
        String consulta = "delete from Turno t where t.medicoHospital.id = :idMedicoHospital and t.paciente is null";
        this.sessionFactory.getCurrentSession().createQuery(consulta)
                .setParameter("idMedicoHospital", idMedicoHospital)
                .executeUpdate();

        String consulta2 = "delete from Turno t where t.medicoHospital.id = :idMedicoHospital and t.fecha > :fechaDeHoy";
        this.sessionFactory.getCurrentSession().createQuery(consulta2)
                .setParameter("idMedicoHospital", idMedicoHospital)
                .setParameter("fechaDeHoy", fechaDeHoy)
                .executeUpdate();
    }

    @Override
    public List<Turno> listarDisponibles() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Turno.class);
        criteria.add(Restrictions.isNull("paciente"));

        LocalDate currentDate = LocalDate.now();
        criteria.add(Restrictions.gt("fecha", currentDate));

        return criteria.list();
    }

    @Override
    public Turno buscarPorId(Long turnoId) {
        return (Turno) sessionFactory.getCurrentSession().createCriteria(Turno.class)
                .add(Restrictions.eq("id", turnoId))
                .uniqueResult();
    }

    @Override
    public void asignar(Turno turno) { sessionFactory.getCurrentSession().update(turno); }

    @Override
    public List<Turno> turnosPaciente(Paciente paciente) {
        String hql = "FROM Turno t WHERE t.paciente = :paciente ORDER BY t.fecha DESC";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("paciente", paciente)
                .getResultList();
    }

    @Override
    public List<Turno> proximosTurnosPaciente(Paciente paciente) {
        LocalDate currentDate = LocalDate.now();

        String hql = "FROM Turno t WHERE t.paciente = :paciente AND t.fecha >= :fecha ORDER BY t.fecha DESC";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("paciente", paciente)
                .setParameter("fecha", currentDate)
                .getResultList();
    }
    @Override
    public void update(Turno turno) {
        sessionFactory.getCurrentSession().update(turno);
    }

    @Override
    public List<Turno> listarTurnosConPacienteAsignadoSegunLaFecha(LocalDate fecha) {
        String hql = "FROM Turno t WHERE t.fecha = :fecha and t.paciente is not null ";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("fecha", fecha)
                .getResultList();
    }

    @Override
    public List<Turno> listarTurnosAsignadosPorIdMedicoYFecha(Long idMedico, LocalDate fecha) {
        String hql = "FROM Turno t WHERE t.fecha = :fecha and t.medicoHospital.medico.id = :idMedico and t.paciente is not null ";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("fecha", fecha)
                .setParameter("idMedico", idMedico)
                .getResultList();
    }

    @Override
    public List<Turno> listarTUrnosAsignadosPorIdMedico(Long idMedico) {
        LocalDate fechaDeHoy = LocalDate.now();
        String hql = "FROM Turno t WHERE t.fecha > :fechaDeHoy and t.medicoHospital.medico.id = :idMedico and t.paciente is not null ORDER BY t.fecha DESC ";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("idMedico", idMedico)
                .setParameter("fechaDeHoy", fechaDeHoy)
                .getResultList();
    }

    @Override
    public List<Turno> listarTurnosdelDia(LocalDate fechaDehoy) {
        String hql = "FROM Turno t WHERE t.fecha = :fecha and t.paciente is not null";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("fecha", fechaDehoy)
                .getResultList();    
    }

    @Override
    public List<Paciente> listarPacientePorIdMedico(Medico medico) {
        return this.sessionFactory.getCurrentSession().createCriteria(Turno.class, "t")
                .createAlias("t.medicoHospital", "mh")
                .setProjection(Projections.distinct(Projections.property("t.paciente")))
                .add(Restrictions.isNotNull("t.paciente"))
                .add(Restrictions.eq("mh.medico", medico))
                .list();
    }

    @Override
    public Turno buscarPorFechaHoraYMedicoHospital(LocalDate fecha, LocalTime hora, Medico_Hospital medicoHospital) {
        return (Turno) this.sessionFactory.getCurrentSession().createCriteria(Turno.class)
                .add(Restrictions.eq("fecha", fecha))
                .add(Restrictions.eq("hora", hora))
                .add(Restrictions.eq("medicoHospital", medicoHospital))
                .uniqueResult();
    }

    @Override
    public List<Turno> obtenerTurnosPorMedicoHospitalId(Long idMedicoHospital) {
        return this.sessionFactory.getCurrentSession().createCriteria(Turno.class)
                .add(Restrictions.eq("medicoHospital.id", idMedicoHospital))
                .addOrder(Order.asc("fecha"))
                .addOrder(Order.asc("hora"))
                .list();
    }

}

