package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosAsignarTurnoPacienteRecepcionDTO;
import ar.edu.unlam.tallerweb1.delivery.DatosRegistroMedicoDTO;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.delivery.DatosAsignarTurnoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.*;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioDiaTrabajo;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPaciente;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPlanEspecialidad;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioTurno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
@Service
@Transactional
public class ServicioTurnoImpl implements ServicioTurno {
    private RepositorioTurno repositorioTurno;
    private RepositorioPaciente repositorioPaciente;

    private RepositorioDiaTrabajo repositorioDiaTrabajo;
    private RepositorioPlanEspecialidad repositorioPlanEspecialidad;

    @Autowired
    public ServicioTurnoImpl(RepositorioTurno repositorioTurno,RepositorioPaciente repositorioPaciente, RepositorioDiaTrabajo repositorioDiaTrabajo, RepositorioPlanEspecialidad repositorioPlanEspecialidad){
        this.repositorioTurno=repositorioTurno;
        this.repositorioPaciente=repositorioPaciente;
        this.repositorioDiaTrabajo = repositorioDiaTrabajo;
        this.repositorioPlanEspecialidad=repositorioPlanEspecialidad;
    }

    @Override
    public Turno buscarTurnoPorId(Long idTurno) {
return repositorioTurno.buscarPorId(idTurno)   ; }

    @Override
    public List<Turno> listarTurnos() {
            return this.repositorioTurno.listar();
    }

    @Override
    public List<Turno> turnosDisponibles() { return this.repositorioTurno.listarDisponibles(); }


    @Override
    public void generarTurnos(Long idMedicoHospital) {
        List<DiaTrabajo> diasTrabajo = repositorioDiaTrabajo.obtenerDiasMedicoHospital(idMedicoHospital);


        for (DiaTrabajo diaTrabajo : diasTrabajo) {
            LocalDate fecha = obtenerFechaSiguiente(diaTrabajo.getNombreDia());
            LocalTime horaActual = diaTrabajo.getHoraInicio();

            while (horaActual.isBefore(diaTrabajo.getHoraFin())) {
                // Crea un nuevo turno
                Turno turno = new Turno();
                turno.setFecha(fecha);
                turno.setHora(horaActual);
                turno.setMedicoHospital(diaTrabajo.getMedicoHospital());

                if(validarTurno(turno)){
                    repositorioTurno.guardar(turno);
                }

                // Avanza la hora al siguiente turno
                horaActual = horaActual.plusMinutes(30);
            }
        }


    }

    private boolean validarTurno(Turno turno) {
        Turno turnoABuscar = this.repositorioTurno.buscarPorFechaHoraYMedicoHospital(turno.getFecha(), turno.getHora(), turno.getMedicoHospital() );

        if (turnoABuscar == null){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void eliminarTurnosPorElMedicoHospitalId(Long idMedicoHospital) {
        repositorioTurno.eliminarTurnosPorElMedicoHospitalId(idMedicoHospital);
    }

    @Override
    public void asignar(DatosAsignarTurnoDTO datosAsignarTurno) throws AccesoDenegadoException {
        if(!datosAsignarTurno.getPaciente().getActivo()){
            throw new AccesoDenegadoException("Paciente Inactivo");
        }else{
            Turno turno = obtenerTurno(datosAsignarTurno.getTurnoId());
            turno.setPaciente(datosAsignarTurno.getPaciente());
            turno.setMotivo(datosAsignarTurno.getMotivo());
            turno.setValorConsulta(calcularValorConsulta(turno));
            repositorioTurno.asignar(turno);
        }
    }

    public Double calcularValorConsulta(Turno turno) {
        Paciente paciente = turno.getPaciente();
        Plan plan = paciente.getPlan();
        List<Plan_Especialidad> planEspecialidadList = repositorioPlanEspecialidad.buscarPorPlan(plan.getId());
        Double valorConsulta = turno.getMedicoHospital().getEspecialidad().getCostoEspecialidad(); // Valor predeterminado

        for (Plan_Especialidad planEspecialidad : planEspecialidadList) {
            if (planEspecialidad.getEspecialidad().getId() == turno.getMedicoHospital().getEspecialidad().getId()) {
                valorConsulta = 0.0;
                break; // Salir del bucle si se encuentra una coincidencia
            }
        }

        return valorConsulta;
    }



    @Override
    public List<Turno> turnosPaciente(Paciente paciente) {
        List <Turno> lista = this.repositorioTurno.turnosPaciente(paciente);
        return lista;
    }

    @Override
    public void cancelarTurno(Long id, Long uId) throws AccesoDenegadoException {

        Turno turno = this.repositorioTurno.buscarPorId(id);
        if(turno.getPaciente().getUsuario().getId() == uId){
            turno.setPaciente(null);
            this.repositorioTurno.update(turno);
        }else{
            throw new AccesoDenegadoException("Accion no autorizada");
        }
    }

    @Override
    public List<Turno> proximosTurnosPaciente(Paciente paciente) {
        List<Turno> turnos = this.repositorioTurno.proximosTurnosPaciente(paciente);
        return turnos;
    }

    @Override
    public List<Turno> listarTurnosConPacienteAsignadoSegunLaFecha(LocalDate fecha) {
        return repositorioTurno.listarTurnosConPacienteAsignadoSegunLaFecha(fecha);
    }

    @Override
    public List<Turno> listarTurnosAsignadosPorIdMedicoYFecha(Long idMedico, LocalDate fecha) {

        return repositorioTurno.listarTurnosAsignadosPorIdMedicoYFecha(idMedico, fecha);
    }

    @Override
    public List<Turno> listarTurnosAsignadosPorIdMedico(Long idMedico) {
        return repositorioTurno.listarTUrnosAsignadosPorIdMedico(idMedico);
    }




    @Override
    public List<Turno> ListarturnosDelDia(LocalDate fechaDeHoy) {
        return repositorioTurno.listarTurnosdelDia(fechaDeHoy);
    }

    @Override
    public void cambiarEstadoDelPago(Turno turno) {
        turno.setPagado(true);
        repositorioTurno.update(turno);
    }
    @Override
    public List<Paciente> listarPacientesPorIdMedico(Medico medico) {
        return repositorioTurno.listarPacientePorIdMedico(medico);
    }

    @Override
    public DatosAsignarTurnoDTO getDatosdelTurnoPorId(Long id) {
        Turno turnoAModificar = repositorioTurno.buscarPorId(id);
        DatosAsignarTurnoDTO datosTurno= cargarDatosDelTurno(turnoAModificar);
        return datosTurno;
    }

    @Override
    public DatosAsignarTurnoDTO cargarDatosDelTurno(Turno turnoAModificar) {
        DatosAsignarTurnoDTO datosTurno = new DatosAsignarTurnoDTO();
        datosTurno.setPaciente(turnoAModificar.getPaciente());
        datosTurno.setMotivo(turnoAModificar.getMotivo());
        datosTurno.setTurnoId(turnoAModificar.getId());
        return datosTurno;
    }

    @Override
    public void actualizarTurno(DatosAsignarTurnoPacienteRecepcionDTO datosTurno) {
        Turno turnoAModificar= repositorioTurno.buscarPorId(datosTurno.getIdTurno());

        turnoAModificar.setMotivo(datosTurno.getMotivo());
        turnoAModificar.setPaciente(repositorioPaciente.buscarPorDNI(datosTurno.getDNIPaciente()));
        repositorioTurno.update(turnoAModificar);

    }

    @Override
    public Turno modificarTurno(Turno turnoAModificar, DatosAsignarTurnoDTO datosTurno) {
        turnoAModificar.getPaciente().setId(datosTurno.getTurnoId());
        turnoAModificar.setId(datosTurno.getTurnoId());
        turnoAModificar.setMotivo(datosTurno.getMotivo());

        return turnoAModificar;
    }


    private Turno obtenerTurno(Long turnoId) {
        Turno turno = repositorioTurno.buscarPorId(turnoId);
        return turno;
    }

    private LocalDate obtenerFechaSiguiente(String nombreDia) {
        LocalDate fechaActual = LocalDate.now();
        DayOfWeek diaSemanaActual = fechaActual.getDayOfWeek();
        DayOfWeek diaSemanaBuscado = obtenerDiaSemana(nombreDia);

        int diasHastaSiguiente = (diaSemanaBuscado.getValue() - diaSemanaActual.getValue() + 7) % 7;

        return fechaActual.plusDays(diasHastaSiguiente);
    }

    private DayOfWeek obtenerDiaSemana(String nombreDia) {
        switch (nombreDia) {
            case "LUNES":
                return DayOfWeek.MONDAY;
            case "MARTES":
                return DayOfWeek.TUESDAY;
            case "MIERCOLES":
                return DayOfWeek.WEDNESDAY;
            case "JUEVES":
                return DayOfWeek.THURSDAY;
            case "VIERNES":
                return DayOfWeek.FRIDAY;
            case "SABADO":
                return DayOfWeek.SATURDAY;
            case "DOMINGO":
                return DayOfWeek.SUNDAY;
            default:
                throw new IllegalArgumentException("Día de la semana inválido: " + nombreDia);
        }
    }

}
