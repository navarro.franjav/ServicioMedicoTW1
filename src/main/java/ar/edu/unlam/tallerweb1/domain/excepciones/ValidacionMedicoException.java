package ar.edu.unlam.tallerweb1.domain.excepciones;

public class ValidacionMedicoException extends Exception{
    public ValidacionMedicoException(String mensaje){
        super(mensaje);
    }
}
