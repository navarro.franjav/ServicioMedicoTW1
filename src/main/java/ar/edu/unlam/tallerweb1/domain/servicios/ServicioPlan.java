package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.delivery.DatosPlanDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.excepciones.PlanExisteException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ServicioPlan {
    Plan guardarPlan(DatosPlanDTO datosNuevoPlan) throws PlanExisteException;

    List<Plan> listarPlanes();

    void eliminarPlanPorId(Long id) throws AccesoDenegadoException;

    DatosPlanDTO getDatosPlanPorId(Long id);

    void modificarPlan(DatosPlanDTO datosPlanModificado);
}
