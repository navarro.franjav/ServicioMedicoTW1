package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;
import ar.edu.unlam.tallerweb1.domain.excepciones.ClienteNoEncontradoException;
import ar.edu.unlam.tallerweb1.delivery.DatosPacientePlanDTO;
import ar.edu.unlam.tallerweb1.delivery.DatosRegistroPacienteDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Usuario;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPaciente;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPlan;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioUsuario;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service("servicioCliente")
@Transactional
public class ServicioPacienteImpl implements ServicioPaciente {
    private RepositorioPaciente repositorioPaciente;
    private RepositorioUsuario repositorioUsuario;
    private RepositorioPlan repositorioPlan;

    @Autowired
    public ServicioPacienteImpl(RepositorioPaciente repositorioPaciente, RepositorioUsuario repositorioUsuario, RepositorioPlan repositorioPlan ){
        this.repositorioUsuario=repositorioUsuario;
        this.repositorioPaciente=repositorioPaciente;
        this.repositorioPlan=repositorioPlan;
    }

    @Override
    public List<Paciente> listarPacientes() {
        List<Paciente> lista = this.repositorioPaciente.listar();
        return lista;
    }

    @Override
    public Paciente buscar(Long id) {
        Paciente pacienteBuscado = this.repositorioPaciente.buscar(id);
        return pacienteBuscado;
    }

    @Override
    public void eliminar(Long id) throws ClienteNoEncontradoException {
        Paciente pacienteBuscado = this.buscar(id);
        if (pacienteBuscado != null) {
            String nuevoEmail = "Eliminado el " + obtenerFechaHoraActual();
            pacienteBuscado.getUsuario().setEmail(nuevoEmail);

            this.repositorioPaciente.guardar(pacienteBuscado);
        } else {
            throw new ClienteNoEncontradoException();
        }
    }

    private String obtenerFechaHoraActual() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date fechaHoraActual = new Date();
        return dateFormat.format(fechaHoraActual);
    }


    @Override
    public Paciente buscarPorUsuarioId(Long id) {
       try {
           Paciente pacienteBuscado = this.repositorioPaciente.buscarPorUsuarioId(id);
           return pacienteBuscado;
       }catch (HibernateException ex){
           return null;
       }
    }

    @Override
    public void guardarPaciente(DatosRegistroPacienteDTO datosPaciente) throws ParseException {
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date fnac = dateFormat.parse(datosPaciente.getFechanac());

            if (datosPaciente.getIdPaciente() == null){
                Usuario usuario = new Usuario();
                usuario.setApellido(datosPaciente.getApellido());
                usuario.setDni(datosPaciente.getDni());
                usuario.setEmail(datosPaciente.getEmail());
                usuario.setNombre(datosPaciente.getNombre());
                usuario.setTelefono(datosPaciente.getTelefono());
                usuario.setPassword(datosPaciente.getPassword());
                usuario.setRol(3);
                Paciente paciente = new Paciente();
                paciente.setActivo(datosPaciente.getActivo());
                paciente.setCp(datosPaciente.getCp());
                paciente.setFechanac(fnac);
                paciente.setUsuario(usuario);

                Plan plan = this.repositorioPlan.buscarPlan(datosPaciente.getPlanId());
                paciente.setPlan(plan);
                this.repositorioPaciente.guardar(paciente);
            } else {
                Paciente paciente = this.repositorioPaciente.buscar(datosPaciente.getIdPaciente());
                Usuario usuario = paciente.getUsuario();
                usuario.setApellido(datosPaciente.getApellido());
                usuario.setDni(datosPaciente.getDni());
                usuario.setEmail(datosPaciente.getEmail());
                usuario.setNombre(datosPaciente.getNombre());
                usuario.setTelefono(datosPaciente.getTelefono());
                usuario.setPassword(datosPaciente.getPassword());
                usuario.setRol(3);
                paciente.setActivo(datosPaciente.getActivo());
                paciente.setCp(datosPaciente.getCp());
                paciente.setFechanac(fnac);
                Plan plan = this.repositorioPlan.buscarPlan(datosPaciente.getPlanId());
                paciente.setPlan(plan);
                paciente.setUsuario(usuario);

                this.repositorioPaciente.editar(paciente);
            }
        }catch (ParseException e){}

    }

    @Override
    public DatosRegistroPacienteDTO cargarDatosPaciente(Long id) {
        DatosRegistroPacienteDTO datosPaciente = new DatosRegistroPacienteDTO();
        Paciente paciente = this.repositorioPaciente.buscar(id);
        datosPaciente.setIdPaciente(id);
        datosPaciente.setActivo(paciente.getActivo());
        datosPaciente.setApellido(paciente.getUsuario().getApellido());
        datosPaciente.setDni(paciente.getUsuario().getDni());
        datosPaciente.setCp(paciente.getCp());
        datosPaciente.setEmail(paciente.getUsuario().getEmail());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fechaNacimiento = dateFormat.format(paciente.getFechanac());
        datosPaciente.setFechanac(fechaNacimiento);

        datosPaciente.setPlanId(paciente.getPlan().getId());
        datosPaciente.setPassword(paciente.getUsuario().getPassword());
        datosPaciente.setTelefono(paciente.getUsuario().getTelefono());
        datosPaciente.setNombre(paciente.getUsuario().getNombre());
        return datosPaciente;
    }

    @Override
    public void cambiarPlan(DatosPacientePlanDTO datosPacientePlan) {
        Paciente paciente = this.repositorioPaciente.buscar(datosPacientePlan.getPacienteId());
        Plan plan = this.repositorioPlan.buscarPlan(datosPacientePlan.getPlanId());
        paciente.setPlan(plan);
        this.repositorioPaciente.editar(paciente);
    }

    @Override
    public Paciente buscarPorEmail(String email) {
        Usuario usuario = this.repositorioUsuario.buscarUsuarioPorElEmail(email);
        Paciente paciente = this.repositorioPaciente.buscarPorUsuarioId(usuario.getId());
        return paciente;
    }

    @Override
    public List<RegistroClinico> historialClinico(Paciente paciente) {
        List<RegistroClinico> historial = this.repositorioPaciente.getHistorial(paciente);
        return historial;
    }


}
