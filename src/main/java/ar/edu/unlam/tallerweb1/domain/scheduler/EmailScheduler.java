package ar.edu.unlam.tallerweb1.domain.scheduler;

import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioTurno;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioEmail;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioTurno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Component
@EnableScheduling
public class EmailScheduler {

    private JavaMailSender javaMailSender;
    private ServicioEmail servicioEmail;
    private ServicioTurno servicioTurno;

    @Autowired
    public EmailScheduler(JavaMailSender javaMailSender, ServicioEmail servicioEmail, ServicioTurno servicioTurno) {
        this.javaMailSender = javaMailSender;
        this.servicioEmail = servicioEmail;
        this.servicioTurno = servicioTurno;
    }


    @Scheduled(cron = "0 15 * * * ?")
    public void enviarEmailsRecordatorioTurnos() throws MessagingException {
        LocalDate DosDiasDespues = LocalDate.now().plusDays(2);

        List<Turno> turnosAsignadosEnDosDias = servicioTurno.listarTurnosConPacienteAsignadoSegunLaFecha(DosDiasDespues);



        for (Turno turno : turnosAsignadosEnDosDias) {
            String contenidoDelEmail = generarContenidoDelEmail(turno);
            String emailPaciente = turno.getPaciente().getUsuario().getEmail();
            String asunto = "Recordatorio de turno " ;

            servicioEmail.enviarEmail(javaMailSender, emailPaciente, asunto, contenidoDelEmail);
        }
    }

    private String generarContenidoDelEmail(Turno turno) {
        String mensaje = "<html><body>Buenos dias " + turno.getPaciente().getUsuario().getApellido() +", " + turno.getPaciente().getUsuario().getNombre() ;
        mensaje +=  "<h5> Le recordamos que tiene un turno en 2 dias <h5>";
        mensaje += "<p> Usted tiene turno agendado con el profesional: " + turno.getMedicoHospital().getMedico().getUsuario().getApellido() + ", " + turno.getMedicoHospital().getMedico().getUsuario().getNombre();
        mensaje += " el dia " + turno.getFecha() +" a las " + turno.getHora() + "<p>";
        mensaje += "<p> El lugar de la consulta es en el " + turno.getMedicoHospital().getHospital().getNombre() + " - " + turno.getMedicoHospital().getHospital().getDirecion() + ", " + turno.getMedicoHospital().getHospital().getCiudad() + "<p>";
        mensaje += "<p> Si no puede asistir, recuerde cancelar el turno <p>";
        mensaje += "<p> Cualquier consulta llame al numero del hospital : " + turno.getMedicoHospital().getHospital().getTelefono() + "<p><body><html>";
        return mensaje;
    }
}
