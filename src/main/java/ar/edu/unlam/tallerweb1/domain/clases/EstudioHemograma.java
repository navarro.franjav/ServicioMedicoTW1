package ar.edu.unlam.tallerweb1.domain.clases;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class EstudioHemograma{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Estudio estudio;
    private double hemoglobinaSangreTotal;
    private double hematocrito;
    private double basofilos;
    private double eosinofilos;
    private double linfocitos;
    private double monocitos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    public double getHemoglobinaSangreTotal() {
        return hemoglobinaSangreTotal;
    }

    public void setHemoglobinaSangreTotal(double hemoglobinaSangreTotal) {
        this.hemoglobinaSangreTotal = hemoglobinaSangreTotal;
    }

    public double getHematocrito() {
        return hematocrito;
    }

    public void setHematocrito(double hematocrito) {
        this.hematocrito = hematocrito;
    }

    public double getBasofilos() {
        return basofilos;
    }

    public void setBasofilos(double basofilos) {
        this.basofilos = basofilos;
    }

    public double getEosinofilos() {
        return eosinofilos;
    }

    public void setEosinofilos(double eosinofilos) {
        this.eosinofilos = eosinofilos;
    }

    public double getLinfocitos() {
        return linfocitos;
    }

    public void setLinfocitos(double linfocitos) {
        this.linfocitos = linfocitos;
    }

    public double getMonocitos() {
        return monocitos;
    }

    public void setMonocitos(double monocitos) {
        this.monocitos = monocitos;
    }

}
