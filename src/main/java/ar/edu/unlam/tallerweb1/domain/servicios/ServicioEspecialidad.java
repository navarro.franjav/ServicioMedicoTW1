package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosEspecialidadDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;

import java.util.List;

public interface ServicioEspecialidad {
    List<Especialidad> listarEspecialidades();
    Especialidad getEspecialidadById(Long idEspecialidad);
    void guardarEspecialidad(DatosEspecialidadDTO datosNuevaEspecialidad);
    void eliminarEspecialidad(Long id);
    void actualizarEspecialidad(DatosEspecialidadDTO datosEspecialidad);
}
