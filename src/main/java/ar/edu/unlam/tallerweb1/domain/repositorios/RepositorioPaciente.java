package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;

import java.util.List;

public interface RepositorioPaciente {
    void guardar(Paciente nuevoPaciente);

    List<Paciente> listar();

    Paciente buscar(Long id);

    void eliminar(Paciente paciente);

    void editar(Paciente pacienteEditado);

    Paciente buscarPorUsuarioId(Long id);

    List<Paciente> buscarPacientesActivos();

    List<Paciente> buscarPacientesInactivos();

    Paciente buscarPorDNI(Long dniPaciente);
    List<RegistroClinico> getHistorial(Paciente paciente);
}
