package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.*;
import ar.edu.unlam.tallerweb1.domain.clases.*;

import java.util.List;

public interface ServicioEstudio {
    DatosEstudioDTO seleccionarEstudio(DatosSeleccionarEstudioDTO datosSeleccionarEstudioDTO);
    void cargarHemograma(DatosEstudioHemogramaDTO datosEstudioHemogramaDTO);
    Estudio obtenerEstudioPorELId(Long idEstudio);
    List<TipoEstudio> listarTiposEstudios();
    TipoEstudio obtenerTipoEstudioPorElId(Long idTipoEstudio);
    EstudioHemograma obtenerHemogramaPorElIdEstudio(Long idEstudio);
    void cargarEstudioColesterol(DatosEstudioColesterolDTO datosEstudioColesterolDTO);
    EstudioColesterol obtenerEstudioColesterolPorELIdEstudio(Long idEstudio);
    void cargarHepatograma(DatosEstudioHepatogramaDTO datosEstudioHepatogramaDTO);
    EstudioHepatograma obtenerHepatogramaPorElIdEstudio(Long idEstudio);
    List<Estudio> obtenerEstudiosPorElIdPaciente(Long idPaciente);
    List<Estudio> obtenerLosUltimosTresEstudiosPorIdPaciente(Long idPaciente);
}
