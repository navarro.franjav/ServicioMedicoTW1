package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosEspecialidadDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioEspecialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service("servicioEspecialidad")
@Transactional
public class ServicioEspecialidadImpl implements  ServicioEspecialidad{

    private RepositorioEspecialidad repositorioEspecialidad;

    @Autowired
    public ServicioEspecialidadImpl(RepositorioEspecialidad repositorioEspecialidad){
        this.repositorioEspecialidad = repositorioEspecialidad;
    }
    @Override
    public List<Especialidad> listarEspecialidades() {
        return this.repositorioEspecialidad.listarEspecialidades();
    }

    @Override
    public Especialidad getEspecialidadById(Long idEspecialidad) {
        return this.repositorioEspecialidad.getEspecialidadPorId(idEspecialidad);
    }

    @Override
    public void guardarEspecialidad(DatosEspecialidadDTO datosNuevaEspecialidad) {
        Especialidad nuevaEspecialidad= crearEspecialidad(datosNuevaEspecialidad);
        this.repositorioEspecialidad.guardarEspecialidad(nuevaEspecialidad);
    }

    @Override
    public void eliminarEspecialidad(Long id) {
        Especialidad especialidad=this.repositorioEspecialidad.getEspecialidadPorId(id);
        this.repositorioEspecialidad.eliminarEspecialidad(especialidad);
    }

    @Override
    public void actualizarEspecialidad(DatosEspecialidadDTO datosEspecialidad) {
        Especialidad especialidadAModificar=this.repositorioEspecialidad.getEspecialidadPorId(datosEspecialidad.getId());
        Especialidad especialidadModificada=modificarEspecialidad(especialidadAModificar, datosEspecialidad);
        this.repositorioEspecialidad.modificar(especialidadModificada);
    }

    private Especialidad modificarEspecialidad(Especialidad especialidadAModificar, DatosEspecialidadDTO datosEspecialidad) {
        especialidadAModificar.setDescripcion(datosEspecialidad.getDescripcion());
        especialidadAModificar.setCostoEspecialidad(datosEspecialidad.getCostoEspecialidad());
        return especialidadAModificar;
    }


    private Especialidad crearEspecialidad(DatosEspecialidadDTO datosNuevaEspecialidad) {
        Especialidad especialidad= new Especialidad();
        especialidad.setDescripcion(datosNuevaEspecialidad.getDescripcion());
        especialidad.setCostoEspecialidad(datosNuevaEspecialidad.getCostoEspecialidad());
        return especialidad;
    }

}
