package ar.edu.unlam.tallerweb1.domain.excepciones;

public class EspecialidadException extends Exception {
    public EspecialidadException(String message){
        super(message);
    }
}
