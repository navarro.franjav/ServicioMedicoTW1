package ar.edu.unlam.tallerweb1.domain.configuracion;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDate;

public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("year", localDate.getYear());
        jsonGenerator.writeStringField("month", localDate.getMonth().name());
        jsonGenerator.writeNumberField("monthValue", localDate.getMonthValue());
        jsonGenerator.writeNumberField("dayOfMonth", localDate.getDayOfMonth());
        jsonGenerator.writeStringField("dayOfWeek", localDate.getDayOfWeek().name());
        //jsonGenerator.writeStringField("era", localDate.getEra().name());
        jsonGenerator.writeNumberField("dayOfYear", localDate.getDayOfYear());
        jsonGenerator.writeBooleanField("leapYear", localDate.isLeapYear());
        jsonGenerator.writeObjectField("chronology", localDate.getChronology());
        jsonGenerator.writeEndObject();
    }
}
