package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.*;

import java.util.List;

public interface RepositorioEstudio {
    void cargarHemograma(EstudioHemograma hemograma);
    Estudio obtenerEstudioPorELiD(Long idEstudio);

    List<TipoEstudio> listarTiposEstudios();

    TipoEstudio obtenerTipoEstudioPorElId(Long idEstudio);

    EstudioHemograma obtenerHemogramaPorElIdEstudio(Long idEstudio);

    void cargarEstudioColesterol(EstudioColesterol estudioColesterol);

    EstudioColesterol obtenerEstudioColesterolPorElIdEstudio(Long idEstudio);

    void cargarHepatograma(EstudioHepatograma estudioHepatograma);

    EstudioHepatograma obtenerHepatogramaPorElIdEstudio(Long idEstudio);

    List<Estudio> obtenerEstudiosPorElIdPaciente(Long idPaciente);

    List<Estudio> obtenerLosUltimosTresEstudiosPorIdPaciente(Long idPaciente);
}
