package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("repositorioMedicoHospital")
public class RepositorioMedicoHospitalImpl implements  RepositorioMedicoHospital{
    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioMedicoHospitalImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void guardarMedicoHospital(Medico_Hospital medicoHospital) {
        this.sessionFactory.getCurrentSession().save(medicoHospital);
    }

    @Override
    public List<Medico_Hospital> listarMedicoHospital() {
        return this.sessionFactory.getCurrentSession().createCriteria(Medico_Hospital.class)
                .setFetchMode("medico", FetchMode.JOIN)
                .setFetchMode("hospital", FetchMode.JOIN)
                .list();
    }

    @Override
    public List<Medico_Hospital> listarMedicoHospitalPorElIdMedico(Long id) {
        return this.sessionFactory.getCurrentSession().createCriteria(Medico_Hospital.class)
                .setFetchMode("medico", FetchMode.JOIN)
                .setFetchMode("hospital", FetchMode.JOIN)
                .add(Restrictions.eq("medico.id", id))
                .add(Restrictions.eq("activo", true))
                .list();
    }

    @Override
    public Medico_Hospital obtenerElUltimoCreado() {
        return (Medico_Hospital) this.sessionFactory.getCurrentSession().createCriteria(Medico_Hospital.class)
                .setFetchMode("medico",FetchMode.JOIN)
                .setFetchMode("hospital", FetchMode.JOIN)
                .setFetchMode("medico.usuario",FetchMode.JOIN )
                .addOrder(Order.desc("id"))
                .setMaxResults(1).uniqueResult();
    }

    @Override
    public Medico_Hospital obtenerPorId(Long medicoHospitalId) {
        return  (Medico_Hospital) this.sessionFactory.getCurrentSession().createCriteria(Medico_Hospital.class)
                .setFetchMode("medico",FetchMode.JOIN)
                .setFetchMode("hospital", FetchMode.JOIN)
                .setFetchMode("medico.usuario",FetchMode.JOIN )
                .add(Restrictions.eq("id", medicoHospitalId))
                .setMaxResults(1).uniqueResult();
    }

    @Override
    public Long obtenerIdMedico(Long idMedicoHospital) {
        return (Long) this.sessionFactory.getCurrentSession().createCriteria(Medico_Hospital.class)
                .setProjection(Projections.property("medico.id"))
                .add(Restrictions.eq("id", idMedicoHospital))
                .uniqueResult();
    }

    public void eliminarMedicoHospital(Medico_Hospital medicoHospital) {
        this.sessionFactory.getCurrentSession().update(medicoHospital);
    }
}
