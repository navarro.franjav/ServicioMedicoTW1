package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.delivery.DatosPlanDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.excepciones.PlanExisteException;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Transactional
public class ServicioPlanImpl implements ServicioPlan {

    private RepositorioPlan repositorioPlan;

    @Autowired
    public ServicioPlanImpl(RepositorioPlan repositorioPlan){
        this.repositorioPlan=repositorioPlan;
    }
    @Override
    public List<Plan> listarPlanes() {
        List<Plan> listaPlanes=this.repositorioPlan.obtenerPlanes();
        return listaPlanes;
    }

    @Override
    public Plan guardarPlan(DatosPlanDTO datosNuevoPlan) throws PlanExisteException {
        if(planExiste(datosNuevoPlan)){
            throw new PlanExisteException("El plan que desea ingresar ya existe, intente ingresando uno distito.");
        }
        Plan nuevoPlan = crearPlan(datosNuevoPlan);
        repositorioPlan.guardar(nuevoPlan);
        return nuevoPlan;
    }

    private boolean planExiste(DatosPlanDTO datosNuevoPlan) {
        Plan planEncontrado= repositorioPlan.buscarPlanPorNombre(datosNuevoPlan.getNombre());
        if (planEncontrado!=null){
            return true;
        } else return false;
    }

    private Plan crearPlan(DatosPlanDTO datosNuevoPlan) {
        Plan nuevoPlan=new Plan();
        nuevoPlan.setNombre(datosNuevoPlan.getNombre());
        nuevoPlan.setPrecio(datosNuevoPlan.getPrecio());
        nuevoPlan.setDescripcion(datosNuevoPlan.getDescripcion());
        return nuevoPlan;
    }

    @Override
    public void eliminarPlanPorId(Long id) throws AccesoDenegadoException {
        Plan planAEliminar=repositorioPlan.buscarPlan(id);
        repositorioPlan.eliminarPlan(planAEliminar);
    }

    @Override
    public DatosPlanDTO getDatosPlanPorId(Long id) {
        Plan planAEditar = repositorioPlan.buscarPlan(id);
        DatosPlanDTO datosPlan = cargarDatosPlan(planAEditar);
        return datosPlan;
    }

    @Override
    public void modificarPlan(DatosPlanDTO datosPlanModificado) {
        Plan planAModificar = repositorioPlan.buscarPlan(datosPlanModificado.getId());

        editarPlan(planAModificar, datosPlanModificado);

        repositorioPlan.modificarPlan(planAModificar);
    }

    private void editarPlan(Plan planAModificar, DatosPlanDTO datosPlanModificado) {
        planAModificar.setDescripcion(datosPlanModificado.getDescripcion());
        planAModificar.setNombre(datosPlanModificado.getNombre());
        planAModificar.setPrecio(datosPlanModificado.getPrecio());
    }

    private DatosPlanDTO cargarDatosPlan(Plan planAEditar) {
        DatosPlanDTO datosPlan= new DatosPlanDTO();
        datosPlan.setNombre(planAEditar.getNombre());
        datosPlan.setDescripcion(planAEditar.getDescripcion());
        datosPlan.setPrecio(planAEditar.getPrecio());
        datosPlan.setId(planAEditar.getId());
        return datosPlan;
    }
}
