package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroClinicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioRegistro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Transactional
public class ServicioRegistroImpl implements ServicioRegistro{

    private RepositorioRegistro repositorioRegistro;
    private ServicioPaciente servicioPaciente;
    private ServicioMedico servicioMedico;

    @Autowired
    public ServicioRegistroImpl(RepositorioRegistro repositorioRegistro, ServicioPaciente servicioPaciente, ServicioMedico servicioMedico ){
        this.repositorioRegistro = repositorioRegistro;
        this.servicioPaciente = servicioPaciente;
        this.servicioMedico = servicioMedico;
    }

    @Override
    public RegistroClinico buscarPorId(Long idRegistro) {
        RegistroClinico registroClinico = this.repositorioRegistro.buscarPorId(idRegistro);
        return registroClinico;
    }

    @Override
    public RegistroClinico buscarPorCodigo(String codigoRegistro) {
        RegistroClinico registroClinico = this.repositorioRegistro.buscarPorCodigo(codigoRegistro);
        return registroClinico;
    }

    @Override
    public void crearRegistro(DatosRegistroClinicoDTO registroDTO) throws ParseException {
        Paciente paciente = this.servicioPaciente.buscar(registroDTO.getIdPaciente());
        Medico medico = this.servicioMedico.buscarPorId(registroDTO.getIdMedico());
        long timestamp = new Date().getTime();
        String codigo = getMD5Hash(String.valueOf(timestamp)+paciente.getUsuario().getNombre()+paciente.getUsuario().getDni());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha = dateFormat.parse(registroDTO.getFecha());

        RegistroClinico registro = new RegistroClinico();
        registro.setCodigo(codigo);
        registro.setMotivo(registroDTO.getMotivo());
        registro.setFecha(fecha);
        registro.setDiagnostico(registroDTO.getDiagnostico());
        registro.setMedico(medico);
        registro.setPaciente(paciente);
        registro.setObservaciones(registroDTO.getObservaciones());


        this.repositorioRegistro.crear(registro);
    }


    private static String getMD5Hash(String input) {
        try {
            // Obtener una instancia de MessageDigest con el algoritmo MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // Convertir la cadena de entrada en bytes y calcular el hash
            byte[] messageDigest = md.digest(input.getBytes());

            // Convertir el hash en una representación hexadecimal
            BigInteger no = new BigInteger(1, messageDigest);
            StringBuilder hashString = new StringBuilder(no.toString(16));
            while (hashString.length() < 32) {
                hashString.insert(0, "0");
            }

            return hashString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

}
