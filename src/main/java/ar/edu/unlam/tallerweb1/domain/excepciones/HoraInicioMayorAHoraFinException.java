package ar.edu.unlam.tallerweb1.domain.excepciones;

public class HoraInicioMayorAHoraFinException extends Exception{
    public HoraInicioMayorAHoraFinException(String mensaje){
        super(mensaje);
    }
}
