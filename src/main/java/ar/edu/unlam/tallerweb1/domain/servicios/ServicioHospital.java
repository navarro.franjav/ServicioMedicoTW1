package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosHospitalDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Hospital;

import java.util.List;

public interface ServicioHospital {
    void guardarHospital(DatosHospitalDTO nuevoHospital);

    List<Hospital> listarHospitales();

    Hospital getHospitalPorId(Long id);

    void guardarCambiosHospital(Hospital hospital);

    void eliminarHospital(Long id);

    void actualizarHospital(DatosHospitalDTO datosHospital);
}
