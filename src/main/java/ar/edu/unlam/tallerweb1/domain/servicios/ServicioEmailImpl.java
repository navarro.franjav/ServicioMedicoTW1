package ar.edu.unlam.tallerweb1.domain.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service("servicioEmail")
public class ServicioEmailImpl implements ServicioEmail{


    @Override
    public void enviarEmail(JavaMailSender javaMailSender, String mail, String reminder, String contenidoDelEmail) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("frnavarro@alumno.unlam.edu.ar");
            helper.setTo(mail);
            helper.setSubject(reminder);
            helper.setText(contenidoDelEmail, true);
            javaMailSender.send(message);
        }
        catch (MessagingException e) {
            throw e;
        }
    }
}
