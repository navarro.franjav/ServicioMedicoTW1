package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioCaja;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioEspecialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Transactional
@Service
public class ServicioCajaImpl implements ServicioCaja {

    private RepositorioCaja repositorioCaja;

    @Autowired
    public ServicioCajaImpl(RepositorioCaja repositorioCaja){
        this.repositorioCaja = repositorioCaja;
    }

    @Override
    public void CrearCaja() {
        Caja caja = new Caja();
        caja.setFecha(LocalDate.now());
        repositorioCaja.guardarCaja(caja);

    }
}
