package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;

public interface RepositorioCaja {
    void guardarCaja(Caja caja);
}
