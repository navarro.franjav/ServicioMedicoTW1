package ar.edu.unlam.tallerweb1.domain.clases;

import javax.persistence.*;

@Entity
public class EstudioColesterol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Estudio estudio;
    private double colesterolLDL;
    private double colesterolTotal;
    private double trigliceridos;
    private double colesterolHDL;
    private double colesterolNoHDL;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    public double getColesterolLDL() {
        return colesterolLDL;
    }

    public void setColesterolLDL(double colesterolLDL) {
        this.colesterolLDL = colesterolLDL;
    }

    public double getColesterolTotal() {
        return colesterolTotal;
    }

    public void setColesterolTotal(double colesterolTotal) {
        this.colesterolTotal = colesterolTotal;
    }

    public double getTrigliceridos() {
        return trigliceridos;
    }

    public void setTrigliceridos(double trigliceridos) {
        this.trigliceridos = trigliceridos;
    }

    public double getColesterolHDL() {
        return colesterolHDL;
    }

    public void setColesterolHDL(double colesterolHDL) {
        this.colesterolHDL = colesterolHDL;
    }

    public double getColesterolNoHDL() {
        return colesterolNoHDL;
    }

    public void setColesterolNoHDL(double colesterolNoHDL) {
        this.colesterolNoHDL = colesterolNoHDL;
    }
}
