package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Plan_Especialidad;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("repositorioPlanEspecialidad")
public class RepositorioPlanEspecialidadImpl implements RepositorioPlanEspecialidad{
    private SessionFactory sessionFactory;
    @Autowired
    public RepositorioPlanEspecialidadImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Plan_Especialidad> buscarPorPlan(Long id) {
        return this.sessionFactory.getCurrentSession().createCriteria(Plan_Especialidad.class)
                .setFetchMode("plan",FetchMode.JOIN)
                .setFetchMode("especialidad",FetchMode.JOIN)
                .add(Restrictions.eq("plan.id",id))
                .list();
    }

    @Override
    public void guardar(Plan_Especialidad planEspecialidad) {
        this.sessionFactory.getCurrentSession().save(planEspecialidad);
    }

    @Override
    public List<Plan_Especialidad> getPlanEspecialidades() {
        return this.sessionFactory.getCurrentSession().createCriteria(Plan_Especialidad.class).list();
    }

}
