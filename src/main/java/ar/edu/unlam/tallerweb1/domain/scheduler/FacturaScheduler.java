package ar.edu.unlam.tallerweb1.domain.scheduler;

import ar.edu.unlam.tallerweb1.domain.servicios.ServicioFactura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class FacturaScheduler {
    private ServicioFactura  servicioFactura;
    @Autowired
    public FacturaScheduler(ServicioFactura servicioFactura){
        this.servicioFactura = servicioFactura;
    }


    @Scheduled(cron = "0 0 0 3 * ?") // Se ejecuta el 3 de cada mes
    public void generarFacturasMensuales() {
        servicioFactura.generarFacturasMensuales();
    }

    @Scheduled(cron = "0 0 0 19 * ?") // Se ejecuta el 19 de cada mes
    public void verificarFacturas() {
        servicioFactura.verificarFacturaImpaga();
    }

    @Scheduled(cron = "0 45 * * * ?") // Se ejecuta a las 0 AM todos los días
    public void verificarFacturasPagas() {servicioFactura.verificarFacturaPagas();
    }

}
