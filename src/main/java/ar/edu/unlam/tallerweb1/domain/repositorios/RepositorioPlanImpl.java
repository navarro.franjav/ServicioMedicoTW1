package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RepositorioPlanImpl implements RepositorioPlan {

    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioPlanImpl(SessionFactory sessionFactory) {
        this.sessionFactory=sessionFactory;
    }
    @Override
    public void guardar(Plan nuevoPlan) {
        sessionFactory.getCurrentSession().save(nuevoPlan);
    }

    @Override
    public List<Plan> obtenerPlanes() {
        return this.sessionFactory.getCurrentSession().createCriteria(Plan.class).list();
    }

    @Override
    public void eliminarPlan(Plan planAEliminar) {
        this.sessionFactory.getCurrentSession().delete(planAEliminar);
    }

    @Override
    public Plan buscarPlan(Long id) {
        return (Plan) this.sessionFactory.getCurrentSession().createCriteria(Plan.class)
                .add(Restrictions.eq("id",id))
                    .uniqueResult();

    }

    @Override
    public void modificarPlan(Plan planAModificar) {
        this.sessionFactory.getCurrentSession().update(planAModificar);
    }

    @Override
    public Plan buscarPlanPorNombre(String nombre) {
        return (Plan) this.sessionFactory.getCurrentSession().createCriteria(Plan.class)
                .add(Restrictions.eq("nombre",nombre))
                .uniqueResult();
    }

}
