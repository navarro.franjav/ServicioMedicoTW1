package ar.edu.unlam.tallerweb1.domain.enums;

import java.time.LocalTime;

public enum Estudios {
    HEMOGRAMA("Hemograma");

    private final String descripcion;

    private Estudios(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
