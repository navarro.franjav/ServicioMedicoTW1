package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroMedicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.clases.Usuario;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioMedico;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service("servicioMedico")
@Transactional
public class ServicioMedicoImpl implements  ServicioMedico{
    private RepositorioMedico repositorioMedico;
    private RepositorioUsuario repositorioUsuario;

    @Autowired
    public ServicioMedicoImpl(RepositorioMedico repositorioMedico, RepositorioUsuario repositorioUsuario){
        this.repositorioMedico = repositorioMedico;
        this.repositorioUsuario = repositorioUsuario;
    }

    @Override
    public void guardarMedico(DatosRegistroMedicoDTO datosMedico) throws ValidacionMedicoException {
            Usuario usuarioNuevo = crearUsuarioMedico(datosMedico);
            Medico medicoNuevo = crearMedico(usuarioNuevo, datosMedico);
            try{
                validarEmail(medicoNuevo.getUsuario().getEmail());
                validarDni(medicoNuevo.getUsuario().getDni());
                repositorioMedico.guardarMedico(medicoNuevo);
            }
            catch (ValidacionMedicoException ex){
                throw ex;
            }

    }

    private void validarDni(Long dni) throws ValidacionMedicoException {
        if (repositorioUsuario.buscarUsuarioPorElDni(dni) != null){
            throw new ValidacionMedicoException("El dni ingresado ya está registrado");
        }
    }

    private void validarEmail(String email) throws ValidacionMedicoException {
        if (repositorioUsuario.buscarUsuarioPorElEmail(email) != null){
            throw new ValidacionMedicoException("El email ingresado ya se encuentra registrado");
        }
    }

    private Medico modificarMedico(Medico medicoAModificar, DatosRegistroMedicoDTO datosMedico) {
        medicoAModificar.getUsuario().setPassword(datosMedico.getPassword());
        medicoAModificar.getUsuario().setDni(datosMedico.getDni());
        medicoAModificar.getUsuario().setEmail(datosMedico.getEmail());
        medicoAModificar.getUsuario().setTelefono(datosMedico.getTelefono());
        medicoAModificar.getUsuario().setNombre(datosMedico.getNombre());
        medicoAModificar.getUsuario().setApellido(datosMedico.getApellido());
        return medicoAModificar;
    }

    @Override
    public List<Medico> listarMedicos() {
        return repositorioMedico.listarMedicos();
    }

    @Override
    public void eliminarMedicoPorElId(Long id) {
        String nuevoEmail = "Eliminado el " + obtenerFechaHoraActual();
        Medico medicoAEliminar = repositorioMedico.buscarMedicoPorElId(id);
        medicoAEliminar.getUsuario().setEmail(nuevoEmail);
        repositorioMedico.modificar(medicoAEliminar);
    }
    private String obtenerFechaHoraActual() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date fechaHoraActual = new Date();
        return dateFormat.format(fechaHoraActual);
    }

    @Override
    public DatosRegistroMedicoDTO getDatosMedicoPorElId(Long id) {

        Medico medicoAModificar = repositorioMedico.buscarMedicoPorElId(id);
        DatosRegistroMedicoDTO datosMedico = cargarDatosMedico(medicoAModificar);

        return datosMedico;
    }

    @Override
    public void actualizarMedico(DatosRegistroMedicoDTO datosMedico) throws ValidacionMedicoException {
        Medico medicoAModificar = repositorioMedico.buscarMedicoPorElId(datosMedico.getIdMedico());

        try{
            if (!medicoAModificar.getUsuario().getEmail().equals(datosMedico.getEmail())){
                validarEmail(datosMedico.getEmail());
            }
            if (!medicoAModificar.getUsuario().getDni().equals(datosMedico.getDni())){
                validarDni(datosMedico.getDni());
            }
            Medico medicoModificado = modificarMedico(medicoAModificar, datosMedico);
            repositorioMedico.modificar(medicoModificado);
        }catch (ValidacionMedicoException ex){
            throw ex;
        }
    }

    @Override
    public Medico buscarPorId(Long id) {
        return repositorioMedico.buscarMedicoPorElId(id);
    }

    @Override
    public Medico buscarPorUsuarioId(Long idUsuario) {
        return repositorioMedico.buscarPorUsuarioId(idUsuario);
    }

    private DatosRegistroMedicoDTO cargarDatosMedico( Medico medicoAModificar) {
        DatosRegistroMedicoDTO datosMedico = new DatosRegistroMedicoDTO();
        datosMedico.setTelefono(medicoAModificar.getUsuario().getTelefono());
        datosMedico.setApellido(medicoAModificar.getUsuario().getApellido());
        datosMedico.setEmail(medicoAModificar.getUsuario().getEmail());
        datosMedico.setDni(medicoAModificar.getUsuario().getDni());
        datosMedico.setNombre(medicoAModificar.getUsuario().getNombre());
        datosMedico.setPassword(medicoAModificar.getUsuario().getPassword());
        datosMedico.setIdMedico(medicoAModificar.getId());

        return datosMedico;
    }

    private Medico crearMedico(Usuario usuarioNuevo, DatosRegistroMedicoDTO datosMedico) {
        Medico medicoNuevo = new Medico();
        medicoNuevo.setUsuario(usuarioNuevo);
        return medicoNuevo;
    }

    private Usuario crearUsuarioMedico(DatosRegistroMedicoDTO datosMedico) {
        Usuario usuarioNuevo = new Usuario();
        usuarioNuevo.setApellido(datosMedico.getApellido());
        usuarioNuevo.setDni(datosMedico.getDni());
        usuarioNuevo.setEmail(datosMedico.getEmail());
        usuarioNuevo.setNombre(datosMedico.getNombre());
        usuarioNuevo.setTelefono(datosMedico.getTelefono());
        usuarioNuevo.setPassword(datosMedico.getPassword());
        usuarioNuevo.setRol(2);

        return usuarioNuevo;
    }
}
