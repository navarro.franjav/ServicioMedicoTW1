package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Hospital;

import java.util.List;

public interface RepositorioHospital {
   void guardarHospital(Hospital nuevoHospital) ;

   List<Hospital> listar();

   Hospital getHospitalPorId(Long id);

   void eliminarHospital(Hospital hospital);

   void modificarHospital(Hospital hospital);
}
