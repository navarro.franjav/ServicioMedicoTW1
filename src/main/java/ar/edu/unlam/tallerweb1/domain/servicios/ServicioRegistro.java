package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroClinicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;

import java.text.ParseException;

public interface ServicioRegistro {
    RegistroClinico buscarPorId(Long idRegistro);

    RegistroClinico buscarPorCodigo(String codigoRegistro);

    void crearRegistro(DatosRegistroClinicoDTO registroDTO) throws ParseException;
}
