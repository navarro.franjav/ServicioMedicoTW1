package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosMedicoHospitalDTO;
import ar.edu.unlam.tallerweb1.delivery.DatosMedicoHospitalDiaDTO;
import ar.edu.unlam.tallerweb1.domain.clases.DiaTrabajo;
import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioDiaTrabajo;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioHospital;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioMedico;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioMedicoHospital;
import ar.edu.unlam.tallerweb1.domain.repositorios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("servicioMedicoHospital")
@Transactional
public class ServicioMedicoHospitalImpl implements ServicioMedicoHospital{

    private RepositorioMedicoHospital repositorioMedicoHosptial;
    private RepositorioHospital repositorioHospital;
    private RepositorioMedico repositorioMedico;
    private RepositorioDiaTrabajo repositorioDiaTrabajo;
    private RepositorioEspecialidad repositorioEspecialidad;
    private RepositorioTurno repositorioTurno;

    @Autowired
    public ServicioMedicoHospitalImpl(RepositorioMedicoHospital repositorioMedicoHosptial, RepositorioHospital repositorioHospital
                                        , RepositorioMedico repositorioMedico, RepositorioDiaTrabajo repositorioDiaTrabajo
                                        , RepositorioEspecialidad repositorioEspecialidad, RepositorioTurno repositorioTurno){
        this.repositorioMedicoHosptial = repositorioMedicoHosptial;
        this.repositorioHospital = repositorioHospital;
        this.repositorioMedico = repositorioMedico;
        this.repositorioDiaTrabajo = repositorioDiaTrabajo;
        this.repositorioEspecialidad = repositorioEspecialidad;
        this.repositorioTurno = repositorioTurno;
    }
    @Override
    public void guardarMedicoHospital(DatosMedicoHospitalDTO datosMedicoHospitalDTO) {
        Medico_Hospital medicoHospitalNuevo = new Medico_Hospital();
        medicoHospitalNuevo.setHospital(repositorioHospital.getHospitalPorId(datosMedicoHospitalDTO.getId_hospital()));
        medicoHospitalNuevo.setMedico(repositorioMedico.buscarMedicoPorElId(datosMedicoHospitalDTO.getId_medico()));
        medicoHospitalNuevo.setEspecialidad(repositorioEspecialidad.getEspecialidadPorId(datosMedicoHospitalDTO.getId_especialidad()));
        medicoHospitalNuevo.setActivo(true);

        repositorioMedicoHosptial.guardarMedicoHospital(medicoHospitalNuevo);
    }

    @Override
    public List<Medico_Hospital> listarMedicoHospital() {
        return repositorioMedicoHosptial.listarMedicoHospital();
    }

    @Override
    public List<Medico_Hospital> listarMedicoHospitalPorElIdMedico(Long id) {
        return repositorioMedicoHosptial.listarMedicoHospitalPorElIdMedico(id);
    }

    @Override
    public Medico_Hospital obtenerElUltimoCreado() {
        return repositorioMedicoHosptial.obtenerElUltimoCreado();
    }

    @Override
    public List<DatosMedicoHospitalDiaDTO> obtenerDatosMedicoHospitalDias(Long medicoId) {
        List<DatosMedicoHospitalDiaDTO> listaDatosHospitalMedicoDias = new ArrayList<DatosMedicoHospitalDiaDTO>();

        List<Medico_Hospital> medicoHospitales = repositorioMedicoHosptial.listarMedicoHospitalPorElIdMedico(medicoId);

        for (Medico_Hospital medicoHospital:medicoHospitales) {
            DatosMedicoHospitalDiaDTO datosMedicoHospitalDia = new DatosMedicoHospitalDiaDTO();
            List<DiaTrabajo> diasTrabajos = repositorioDiaTrabajo.obtenerDiasMedicoHospital(medicoHospital.getId());

            datosMedicoHospitalDia.setMedicoHospital(medicoHospital);
            datosMedicoHospitalDia.setListaDeDias(diasTrabajos);

            listaDatosHospitalMedicoDias.add(datosMedicoHospitalDia);
        }

        return listaDatosHospitalMedicoDias;
    }

    @Override
    public Long obtenerIdMedico(Long idMedicoHospital) {
        return repositorioMedicoHosptial.obtenerIdMedico(idMedicoHospital);
    }

    @Override
    public List<Turno> obtenerTurnosMedicoHospital(Long idMedicoHospital) {
        return this.repositorioTurno.obtenerTurnosPorMedicoHospitalId(idMedicoHospital);
    }

    @Override
    public Medico_Hospital obtenerPorId(Long idMedicoHospital) {
        return repositorioMedicoHosptial.obtenerPorId(idMedicoHospital);
    }
    
    @Override
    public void eliminarMedicoHospital(Long idMedicoHospital) {
        Medico_Hospital medicoHospitalAEliminar = repositorioMedicoHosptial.obtenerPorId(idMedicoHospital);

        medicoHospitalAEliminar.setActivo(false);

        repositorioMedicoHosptial.eliminarMedicoHospital(medicoHospitalAEliminar);
    }
}
