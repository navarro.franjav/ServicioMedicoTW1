package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Plan_Especialidad;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPlanEspecialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioPlanEspecialidadImpl implements ServicioPlanEspecialidad {
    private RepositorioPlanEspecialidad repositorioPlanEspecialidad;

    @Autowired
    public ServicioPlanEspecialidadImpl(RepositorioPlanEspecialidad repositorioPlanEspecialidad){
        this.repositorioPlanEspecialidad=repositorioPlanEspecialidad;
    }
    @Override
    public void guardarPlanEspecialidades(Plan plan, List<Especialidad> listaEspecialidadesSeleccionadas) {
            for (Especialidad especialidad : listaEspecialidadesSeleccionadas) {
                Plan_Especialidad planEspecialidad = new Plan_Especialidad();
                planEspecialidad.setPlan(plan);
                planEspecialidad.setEspecialidad(especialidad);
                this.repositorioPlanEspecialidad.guardar(planEspecialidad);
            }

    }

    @Override
    public List<Plan_Especialidad> listarPlanEspecialidades() {
        return repositorioPlanEspecialidad.getPlanEspecialidades();
    }

}

