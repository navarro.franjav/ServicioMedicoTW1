package ar.edu.unlam.tallerweb1.domain.enums;

import java.time.LocalTime;

public enum HorarioLaboral {
    OCHO_AM("8 AM", LocalTime.of(8, 0)),
    NUEVE_AM("9 AM", LocalTime.of(9, 0)),
    DIEZ_AM("10 AM", LocalTime.of(10, 0)),
    ONCE_AM("11 AM", LocalTime.of(11, 0)),
    DOCE_PM("12 PM", LocalTime.of(12, 0)),
    UNA_PM("1 PM", LocalTime.of(13, 0)),
    DOS_PM("2 PM", LocalTime.of(14, 0)),
    TRES_PM("3 PM", LocalTime.of(15, 0)),
    CUATRO_PM("4 PM", LocalTime.of(16, 0)),
    CINCO_PM("5 PM", LocalTime.of(17, 0)),
    SEIS_PM("6 PM", LocalTime.of(18, 0));

    private final String descripcion;
    private final LocalTime hora;

    private HorarioLaboral(String descripcion, LocalTime hora) {
        this.descripcion = descripcion;
        this.hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalTime getHora() {
        return hora;
    }
}
