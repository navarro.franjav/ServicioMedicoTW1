package ar.edu.unlam.tallerweb1.domain.clases;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Paciente")
public class Paciente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    private Integer cp;
    private Date fechanac;
    private Boolean activo;
    @ManyToOne
    private Plan plan;


    public Long getId() { return id; }
    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getCp() { return cp; }

    public void setCp(Integer cp) { this.cp = cp; }

    public Date getFechanac() { return fechanac; }

    public void setFechanac(Date fechanac) { this.fechanac = fechanac; }

    public Plan getPlan() {
        return plan;
    }
    public void setPlan(Plan plan) {
        this.plan = plan;
    }
    public Boolean getActivo() { return activo; }

    public void setActivo(Boolean activo) { this.activo = activo; }



}
