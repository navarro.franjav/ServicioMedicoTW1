package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.*;
import ar.edu.unlam.tallerweb1.domain.clases.*;
import ar.edu.unlam.tallerweb1.domain.enums.Estudios;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioEstudio;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioMedico;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service("servicioEstudio")
@Transactional
public class ServicioEstudioImpl implements ServicioEstudio{

    private RepositorioMedico repositorioMedico;
    private RepositorioPaciente repositorioPaciente;
    private RepositorioEstudio repositorioEstudio;
    @Autowired
    public ServicioEstudioImpl(RepositorioMedico repositorioMedico, RepositorioPaciente repositorioPaciente
                                , RepositorioEstudio repositorioEstudio){
        this.repositorioMedico = repositorioMedico;
        this.repositorioPaciente = repositorioPaciente;
        this.repositorioEstudio = repositorioEstudio;
    }



    @Override
    public DatosEstudioDTO seleccionarEstudio(DatosSeleccionarEstudioDTO datosSeleccionarEstudioDTO) {
        DatosEstudioDTO datosEstudioDTO;
        switch (repositorioEstudio.obtenerTipoEstudioPorElId(datosSeleccionarEstudioDTO.getIdEstudio()).getNombreEstudio()){
            case "Hemograma":
                datosEstudioDTO = new DatosEstudioHemogramaDTO(datosSeleccionarEstudioDTO);
                break;
            case "Colesterol":
                datosEstudioDTO = new DatosEstudioColesterolDTO(datosSeleccionarEstudioDTO);
                break;
            case "Hepatograma":
                datosEstudioDTO = new DatosEstudioHepatogramaDTO(datosSeleccionarEstudioDTO);
                break;
            default:
                datosEstudioDTO = new DatosEstudioDTO();
        }
        return datosEstudioDTO;
    }

    @Override
    public void cargarHemograma(DatosEstudioHemogramaDTO datosEstudioHemogramaDTO) {
        EstudioHemograma hemograma = cargarDatosHemograma(datosEstudioHemogramaDTO);

        this.repositorioEstudio.cargarHemograma(hemograma);
    }

    @Override
    public Estudio obtenerEstudioPorELId(Long idEstudio) {
        return this.repositorioEstudio.obtenerEstudioPorELiD(idEstudio);
    }

    @Override
    public List<TipoEstudio> listarTiposEstudios() {
        return this.repositorioEstudio.listarTiposEstudios();
    }

    @Override
    public TipoEstudio obtenerTipoEstudioPorElId(Long idTipoEstudio) {
        return this.repositorioEstudio.obtenerTipoEstudioPorElId(idTipoEstudio);
    }

    @Override
    public EstudioHemograma obtenerHemogramaPorElIdEstudio(Long idEstudio) {
        return this.repositorioEstudio.obtenerHemogramaPorElIdEstudio(idEstudio);
    }

    @Override
    public void cargarEstudioColesterol(DatosEstudioColesterolDTO datosEstudioColesterolDTO) {
        EstudioColesterol estudioColesterol = cargarDatosEstudioColesterol(datosEstudioColesterolDTO);

        this.repositorioEstudio.cargarEstudioColesterol(estudioColesterol);
    }

    @Override
    public EstudioColesterol obtenerEstudioColesterolPorELIdEstudio(Long idEstudio) {
        return this.repositorioEstudio.obtenerEstudioColesterolPorElIdEstudio(idEstudio);
    }

    @Override
    public void cargarHepatograma(DatosEstudioHepatogramaDTO datosEstudioHepatogramaDTO) {
        EstudioHepatograma estudioHepatograma = cargarDatosHepatograma(datosEstudioHepatogramaDTO);
        this.repositorioEstudio.cargarHepatograma(estudioHepatograma);
    }

    @Override
    public EstudioHepatograma obtenerHepatogramaPorElIdEstudio(Long idEstudio) {
        return this.repositorioEstudio.obtenerHepatogramaPorElIdEstudio(idEstudio);
    }

    @Override
    public List<Estudio> obtenerEstudiosPorElIdPaciente(Long idPaciente) {
        return this.repositorioEstudio.obtenerEstudiosPorElIdPaciente(idPaciente);
    }

    @Override
    public List<Estudio> obtenerLosUltimosTresEstudiosPorIdPaciente(Long idPaciente) {
        return this.repositorioEstudio.obtenerLosUltimosTresEstudiosPorIdPaciente(idPaciente);
    }

    private EstudioHepatograma cargarDatosHepatograma(DatosEstudioHepatogramaDTO datosEstudioHepatogramaDTO) {
        EstudioHepatograma estudioHepatograma = new EstudioHepatograma();
        Estudio estudio = new Estudio();
        estudio.setMedico(this.repositorioMedico.buscarMedicoPorElId(datosEstudioHepatogramaDTO.getIdMedico()));
        estudio.setPaciente(this.repositorioPaciente.buscar(datosEstudioHepatogramaDTO.getIdPaciente()));
        estudio.setFecha(LocalDate.now());
        estudio.setTipoEstudio(this.repositorioEstudio.obtenerTipoEstudioPorElId(datosEstudioHepatogramaDTO.getIdTipoEstudio()));

        estudioHepatograma.setEstudio(estudio);
        estudioHepatograma.setBilirrubinaTotal(datosEstudioHepatogramaDTO.getBilirrubinaTotal());
        estudioHepatograma.setBilirrubinaDirecta(datosEstudioHepatogramaDTO.getBilirrubinaDirecta());
        estudioHepatograma.setFosfatasasAlcalinas(datosEstudioHepatogramaDTO.getFosfatasasAlcalinas());
        estudioHepatograma.setColesterolTotal(datosEstudioHepatogramaDTO.getColesterolTotal());
        estudioHepatograma.setProteinasTotales(datosEstudioHepatogramaDTO.getProteinasTotales());
        estudioHepatograma.setAlbumina(datosEstudioHepatogramaDTO.getAlbumina());

        return estudioHepatograma;
    }

    private EstudioColesterol cargarDatosEstudioColesterol(DatosEstudioColesterolDTO datosEstudioColesterolDTO) {
        EstudioColesterol estudioColesterol = new EstudioColesterol();
        Estudio estudio = new Estudio();
        estudio.setMedico(this.repositorioMedico.buscarMedicoPorElId(datosEstudioColesterolDTO.getIdMedico()));
        estudio.setPaciente(this.repositorioPaciente.buscar(datosEstudioColesterolDTO.getIdPaciente()));
        estudio.setFecha(LocalDate.now());
        estudio.setTipoEstudio(this.repositorioEstudio.obtenerTipoEstudioPorElId(datosEstudioColesterolDTO.getIdTipoEstudio()));

        estudioColesterol.setEstudio(estudio);
        estudioColesterol.setColesterolLDL(datosEstudioColesterolDTO.getColesterolLDL());
        estudioColesterol.setColesterolTotal(datosEstudioColesterolDTO.getColesterolTotal());
        estudioColesterol.setTrigliceridos(datosEstudioColesterolDTO.getTrigliceridos());
        estudioColesterol.setColesterolHDL(datosEstudioColesterolDTO.getColesterolHDL());
        estudioColesterol.setColesterolNoHDL(datosEstudioColesterolDTO.getColesterolNoHDL());

        return estudioColesterol;

    }

    private EstudioHemograma cargarDatosHemograma(DatosEstudioHemogramaDTO datosEstudioHemogramaDTO) {
        EstudioHemograma hemograma = new EstudioHemograma();
        Estudio estudio = new Estudio();
        estudio.setMedico(this.repositorioMedico.buscarMedicoPorElId(datosEstudioHemogramaDTO.getIdMedico()));
        estudio.setPaciente(this.repositorioPaciente.buscar(datosEstudioHemogramaDTO.getIdPaciente()));
        estudio.setFecha(LocalDate.now());
        estudio.setTipoEstudio(this.repositorioEstudio.obtenerTipoEstudioPorElId(datosEstudioHemogramaDTO.getIdTipoEstudio()));

        hemograma.setEstudio(estudio);
        hemograma.setHemoglobinaSangreTotal(datosEstudioHemogramaDTO.getHemoglobinaSangreTotal());
        hemograma.setHematocrito(datosEstudioHemogramaDTO.getHematocrito());
        hemograma.setBasofilos(datosEstudioHemogramaDTO.getBasofilos());
        hemograma.setEosinofilos(datosEstudioHemogramaDTO.getEosinofilos());
        hemograma.setLinfocitos(datosEstudioHemogramaDTO.getLinfocitos());
        hemograma.setMonocitos(datosEstudioHemogramaDTO.getMonocitos());

        return hemograma;
    }
}
