package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroClinicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;

public interface RepositorioRegistro {
    RegistroClinico buscarPorId(Long idRegistro);

    RegistroClinico buscarPorCodigo(String codigoRegistro);

    void crear(RegistroClinico registroClinico);
}
