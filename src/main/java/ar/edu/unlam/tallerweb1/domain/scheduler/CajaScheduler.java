package ar.edu.unlam.tallerweb1.domain.scheduler;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.List;

@Component
@EnableScheduling
public class CajaScheduler {


    private Caja caja;
    private ServicioCaja servicioCaja;


    @Autowired
    public CajaScheduler( ServicioCaja servicioCaja) {
        this.servicioCaja=servicioCaja;
    }


    @Scheduled(cron = "0 13 16 * * ?")
    public void crearCajaDiaria()  {
        servicioCaja.CrearCaja();

    }




}
