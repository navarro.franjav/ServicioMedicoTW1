package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Plan_Especialidad;

import java.util.List;

public interface RepositorioPlanEspecialidad {

 List<Plan_Especialidad> buscarPorPlan(Long id) ;

 void guardar(Plan_Especialidad planEspecialidad);

 List<Plan_Especialidad> getPlanEspecialidades();
}
