package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository("repositorioEspecialidad")
public class RepositorioEspecialidadImpl implements  RepositorioEspecialidad{

    private SessionFactory sessionFactory;
    @Autowired
    public RepositorioEspecialidadImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Especialidad> listarEspecialidades() {
        return this.sessionFactory.getCurrentSession()
                .createCriteria(Especialidad.class)
                .list();
    }

    @Override
    public Especialidad getEspecialidadPorId(Long idEspecialidad) {
        return (Especialidad) this.sessionFactory.getCurrentSession()
                .createCriteria(Especialidad.class)
                .add(Restrictions.eq("id", idEspecialidad))
                .uniqueResult();
    }

    @Override
    public void guardarEspecialidad(Especialidad nuevaEspecialidad) {
        this.sessionFactory.getCurrentSession().save(nuevaEspecialidad);
    }

    @Override
    public void eliminarEspecialidad(Especialidad especialidad) {
        this.sessionFactory.getCurrentSession().delete(especialidad);
    }

    @Override
    public void modificar(Especialidad especialidadModificada) {
        this.sessionFactory.getCurrentSession().update(especialidadModificada);
    }
}
