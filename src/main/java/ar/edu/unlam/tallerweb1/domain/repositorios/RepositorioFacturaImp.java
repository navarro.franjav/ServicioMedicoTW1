package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Factura;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("repositorioFactura")

public class RepositorioFacturaImp implements RepositorioFactura {

    private SessionFactory sessionFactory;
    @Autowired
    public RepositorioFacturaImp(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void guardar(Factura factura) {
        sessionFactory.getCurrentSession().save(factura);
    }

    @Override
    public Factura buscarPorPaciente(Paciente paciente) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Factura.class)
                .add(Restrictions.eq("paciente", paciente))
                .addOrder(Order.desc("id"))
                .setMaxResults(1);

        return (Factura) criteria.uniqueResult();
    }

    @Override
    public List<Factura> buscarTodasPorPaciente(Paciente paciente) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Factura.class)
                .add(Restrictions.eq("paciente", paciente))
                .addOrder(Order.desc("id"));

        return criteria.list();
    }

    @Override
    public List<Factura> buscarTodas() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Factura.class)
                .addOrder(Order.desc("id"));

        return criteria.list();
    }

    @Override
    public Factura buscarPorId(Long id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Factura.class)
                .add(Restrictions.eq("id", id));

        return (Factura) criteria.uniqueResult();
    }

    @Override
    public void actualizar(Factura factura) {
        sessionFactory.getCurrentSession().update(factura);
    }
}
