package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Hospital;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RepositorioHospitalImpl implements RepositorioHospital {
    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioHospitalImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void guardarHospital(Hospital nuevoHospital) {
        this.sessionFactory.getCurrentSession().save(nuevoHospital);
    }
    public void modificarHospital(Hospital nuevoHospital) {
        this.sessionFactory.getCurrentSession().update(nuevoHospital);
    }

    @Override
    public List<Hospital> listar() {
        return sessionFactory.getCurrentSession().createCriteria(Hospital.class)
                .list();
    }

    @Override
    public Hospital getHospitalPorId(Long id) {
        return (Hospital) sessionFactory.getCurrentSession().createCriteria(Hospital.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    public void eliminarHospital(Hospital hospital) {
        sessionFactory.getCurrentSession().delete(hospital);
    }
}
