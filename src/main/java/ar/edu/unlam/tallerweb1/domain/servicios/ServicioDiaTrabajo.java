package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosDiaTrabajoDTO;
import ar.edu.unlam.tallerweb1.domain.enums.Dias;
import ar.edu.unlam.tallerweb1.domain.excepciones.HoraInicioMayorAHoraFinException;

import java.util.List;

public interface ServicioDiaTrabajo {

    List<Dias> obtenerDiasDisponiblesMedicoHospital(Long idMedicoHospital);

    void guardarDiaTrabajo(DatosDiaTrabajoDTO datosDiaTrabajo) throws HoraInicioMayorAHoraFinException;

    void eliminarDiaTrabajoPorELMedicoHospitalId(Long idMedicoHospital);
}
