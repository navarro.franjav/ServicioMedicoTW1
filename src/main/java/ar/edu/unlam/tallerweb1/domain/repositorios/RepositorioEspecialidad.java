package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;

import java.util.List;

public interface RepositorioEspecialidad {

    List<Especialidad> listarEspecialidades();

    Especialidad getEspecialidadPorId(Long idEspecialidad);

    void guardarEspecialidad(Especialidad nuevaEspecialidad);

    void eliminarEspecialidad(Especialidad especialidad);

    void modificar(Especialidad especialidadModificada);
}
