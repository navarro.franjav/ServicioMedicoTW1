package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.DiaTrabajo;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RepositorioDiaTrabajoImpl implements RepositorioDiaTrabajo{

    private SessionFactory sessionFactory;
    @Autowired
    public RepositorioDiaTrabajoImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<String> obtenerDiasCargadosMedicoHospital(Long idMedicoHospital) {
        return this.sessionFactory.getCurrentSession().createCriteria(DiaTrabajo.class)
                .setProjection(Projections.property("nombreDia"))
                .add(Restrictions.eq("medicoHospital.id", idMedicoHospital))
                .list();
    }

    @Override
    public void guardarDiaTrabajo(DiaTrabajo nuevoDiaTrabajo) {
        this.sessionFactory.getCurrentSession().save(nuevoDiaTrabajo);
    }

    @Override
    public List<DiaTrabajo> obtenerDiasMedicoHospital(Long id) {
        return this.sessionFactory.getCurrentSession().createCriteria(DiaTrabajo.class)
                .add(Restrictions.eq("medicoHospital.id", id))
                .list();
    }

    @Override
    public void eliminarDiaTrabajoPorElMedicoHospitalId(Long idMedicoHospital) {
        String consulta = "delete from DiaTrabajo dt where dt.medicoHospital.id = :idMedicoHospital";
        this.sessionFactory.getCurrentSession().createQuery(consulta)
                .setParameter("idMedicoHospital", idMedicoHospital)
                .executeUpdate();
    }
}
