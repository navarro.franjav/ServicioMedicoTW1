package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Usuario;

public interface ServicioUsuario {
    void guardarUsuario(Usuario nuevoUsuario);

    Usuario getUsuarioByDNI(Long dni);

    void modificarUsuario(Usuario usuarioEditado);


    void editarUsuario(Usuario usuarioEditado, String nombre, String apellido, String email, String telefono, Long dni, String password, int i);

    Usuario crearUsuario(String nombre, String apellido, String email, String telefono, Long dni, String password, int i);

    Usuario buscarUsuarioPorId(Long id);
}

