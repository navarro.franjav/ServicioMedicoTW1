package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("repositorioMedico")
public class RepositorioMedicoImpl implements RepositorioMedico{

    private SessionFactory sessionFactory;
    @Autowired
    public RepositorioMedicoImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void guardarMedico(Medico medicoNuevo) {
        this.sessionFactory.getCurrentSession().save(medicoNuevo);
    }

    @Override
    public List<Medico> listarMedicos() {
        return this.sessionFactory.getCurrentSession().createCriteria(Medico.class)
                .setFetchMode("usuario", FetchMode.JOIN)
                .list();
    }

    @Override
    public Medico buscarMedicoPorElId(Long id) {
        return (Medico) this.sessionFactory.getCurrentSession().createCriteria(Medico.class)
                .setFetchMode("usuario", FetchMode.JOIN)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    public void eliminarMedico(Medico medicoAEliminar) {
        this.sessionFactory.getCurrentSession().delete(medicoAEliminar);
    }

    @Override
    public void modificar(Medico medicoModificado) {
        this.sessionFactory.getCurrentSession().update(medicoModificado);
    }

    @Override
    public Medico buscarPorUsuarioId(Long idUsuario) {
        return (Medico) this.sessionFactory.getCurrentSession().createCriteria(Medico.class)
                .add(Restrictions.eq("usuario.id", idUsuario))
                .uniqueResult();
    }

}
