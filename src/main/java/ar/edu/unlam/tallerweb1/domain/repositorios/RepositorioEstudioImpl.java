package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.*;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("repositorioEstudio")
public class RepositorioEstudioImpl implements RepositorioEstudio{

    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioEstudioImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void cargarHemograma(EstudioHemograma hemograma) {
        this.sessionFactory.getCurrentSession().save(hemograma);
    }

    @Override
    public Estudio obtenerEstudioPorELiD(Long idEstudio) {
        return (Estudio) this.sessionFactory.getCurrentSession().createCriteria(Estudio.class)
                .add(Restrictions.eq("id", idEstudio))
                .uniqueResult();
    }

    @Override
    public List<TipoEstudio> listarTiposEstudios() {
        return this.sessionFactory.getCurrentSession().createCriteria(TipoEstudio.class)
                .list();
    }

    @Override
    public TipoEstudio obtenerTipoEstudioPorElId(Long idEstudio) {
        return (TipoEstudio)this.sessionFactory.getCurrentSession().createCriteria(TipoEstudio.class)
                            .add(Restrictions.eq("id", idEstudio))
                            .uniqueResult();
    }

    @Override
    public EstudioHemograma obtenerHemogramaPorElIdEstudio(Long idEstudio) {
        return (EstudioHemograma) this.sessionFactory.getCurrentSession().createCriteria(EstudioHemograma.class)
                .add(Restrictions.eq("estudio.id", idEstudio))
                .uniqueResult();
    }

    @Override
    public void cargarEstudioColesterol(EstudioColesterol estudioColesterol) {
        this.sessionFactory.getCurrentSession().save(estudioColesterol);
    }

    @Override
    public EstudioColesterol obtenerEstudioColesterolPorElIdEstudio(Long idEstudio) {
        return (EstudioColesterol) this.sessionFactory.getCurrentSession().createCriteria(EstudioColesterol.class)
                .add(Restrictions.eq("estudio.id", idEstudio))
                .uniqueResult();
    }

    @Override
    public void cargarHepatograma(EstudioHepatograma estudioHepatograma) {
        this.sessionFactory.getCurrentSession().save(estudioHepatograma);
    }

    @Override
    public EstudioHepatograma obtenerHepatogramaPorElIdEstudio(Long idEstudio) {
        return (EstudioHepatograma) this.sessionFactory.getCurrentSession().createCriteria(EstudioHepatograma.class)
                .add(Restrictions.eq("estudio.id", idEstudio))
                .uniqueResult();
    }

    @Override
    public List<Estudio> obtenerEstudiosPorElIdPaciente(Long idPaciente) {
        return this.sessionFactory.getCurrentSession().createCriteria(Estudio.class)
                .add(Restrictions.eq("paciente.id", idPaciente))
                .list();
    }

    @Override
    public List<Estudio> obtenerLosUltimosTresEstudiosPorIdPaciente(Long idPaciente) {
        return this.sessionFactory.getCurrentSession().createCriteria(Estudio.class)
                .add(Restrictions.eq("paciente.id", idPaciente))
                .addOrder(Order.desc("fecha"))
                .setMaxResults(3).list();
    }
}
