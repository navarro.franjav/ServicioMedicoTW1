package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Factura;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioFactura;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPaciente;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioPlan;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("servicioFactura")
@Transactional
public class ServicioFacturaImp implements ServicioFactura {

    private RepositorioPaciente repositorioPaciente;
    private RepositorioFactura repositorioFactura;

    @Autowired
    public ServicioFacturaImp( RepositorioPaciente repositorioPaciente, RepositorioFactura repositorioFactura ){
        this.repositorioPaciente=repositorioPaciente;
        this.repositorioFactura=repositorioFactura;
    }

    @Override
    public void generarFacturasMensuales() {
        List<Paciente> pacientesActivos = repositorioPaciente.buscarPacientesActivos();

        for (Paciente paciente : pacientesActivos) {
            generarFactura(paciente);
        }
    }

    public void generarFactura(Paciente paciente){
        Factura factura = new Factura();
        factura.setPaciente(paciente);
        factura.setEmision(new Date());

        // Establecer la fecha de vencimiento en un mes a partir de la fecha actual
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, 15);
        Date fechaVencimiento = calendar.getTime();
        factura.setVencimiento(fechaVencimiento);

        factura.setValor(paciente.getPlan().getPrecio());

        long timestamp = new Date().getTime();

        String codigo = getMD5Hash(String.valueOf(timestamp)+paciente.getUsuario().getNombre()+paciente.getUsuario().getDni());

        factura.setCodigo(codigo);

        factura.setPago(false);

        repositorioFactura.guardar(factura);
    }

    @Override
    public void verificarFacturaImpaga() {
        List<Paciente> pacientesActivos = repositorioPaciente.buscarPacientesActivos();
        Date fechaActual = new Date();

        for (Paciente paciente : pacientesActivos) {
            Factura factura = repositorioFactura.buscarPorPaciente(paciente);
            if (factura != null && factura.getVencimiento().compareTo(fechaActual) <= 0 && !factura.getPago()) {
                paciente.setActivo(false);
                repositorioPaciente.editar(paciente);
            }
        }
    }

    @Override
    public List<Factura> getFacturas(Paciente paciente) {
        List <Factura> facturas = this.repositorioFactura.buscarTodasPorPaciente(paciente);
        return facturas;
    }

    @Override
    public List<Factura> verTodas() {
        List <Factura> facturas = this.repositorioFactura.buscarTodas();
        return facturas;
    }

    @Override
    public void generarFacturaIndividual(Long id) {
        Paciente paciente = repositorioPaciente.buscar(id);
        generarFactura(paciente);
    }

    @Override
    public void pagar(Long id) {
        Factura factura = this.repositorioFactura.buscarPorId(id);
        factura.setPago(true);
        this.repositorioFactura.actualizar(factura);
    }

    @Override
    public void verificarFacturaPagas() {
        List<Paciente> pacientesInactivos = repositorioPaciente.buscarPacientesInactivos();

        for (Paciente paciente : pacientesInactivos) {
            System.out.println(paciente.getId());
            Factura factura = repositorioFactura.buscarPorPaciente(paciente);
            if ( factura != null && factura.getPago() ) {
                paciente.setActivo(true);
                repositorioPaciente.editar(paciente);
            }
        }
    }

    @Override
    public Factura getFacturaById(Long id) {
        return this.repositorioFactura.buscarPorId(id);
    }


    private static String getMD5Hash(String input) {
        try {
            // Obtener una instancia de MessageDigest con el algoritmo MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // Convertir la cadena de entrada en bytes y calcular el hash
            byte[] messageDigest = md.digest(input.getBytes());

            // Convertir el hash en una representación hexadecimal
            BigInteger no = new BigInteger(1, messageDigest);
            StringBuilder hashString = new StringBuilder(no.toString(16));
            while (hashString.length() < 32) {
                hashString.insert(0, "0");
            }

            return hashString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }


}
