package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Usuario;

// Interface que define los metodos del Repositorio de Usuarios.
public interface RepositorioUsuario {
	
	Usuario buscarUsuario(String email, String password);
	void guardar(Usuario usuario);
    Usuario buscar(String email);
	void modificar(Usuario usuario);

	Usuario buscarUsuarioDNI(Long dni);

	Usuario buscarUsuarioPorElId(Long id);

	void eliminarUsuario(Usuario usuarioAEliminar);

	Usuario buscarUsuarioPorElEmail(String email);

	Usuario buscarUsuarioPorElDni(Long dni);
}
