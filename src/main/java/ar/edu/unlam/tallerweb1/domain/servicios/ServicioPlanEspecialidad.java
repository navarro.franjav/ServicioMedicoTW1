package ar.edu.unlam.tallerweb1.domain.servicios;


import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Plan_Especialidad;

import java.util.List;

public interface ServicioPlanEspecialidad {

    void guardarPlanEspecialidades(Plan plan, List<Especialidad> listaEspecialidadesSeleccionadas);

    List<Plan_Especialidad> listarPlanEspecialidades();
}
