package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Medico;

import java.util.List;

public interface RepositorioMedico {

    void guardarMedico(Medico medicoNuevo);

    List<Medico> listarMedicos();

    Medico buscarMedicoPorElId(Long id);

    void eliminarMedico(Medico medicoAEliminar);

    void modificar(Medico medicoModificado);

    Medico buscarPorUsuarioId(Long idUsuario);
}
