package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosAsignarTurnoPacienteRecepcionDTO;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.delivery.DatosAsignarTurnoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;

import java.time.LocalDate;
import java.util.List;

public interface ServicioTurno {
     Turno buscarTurnoPorId(Long idTurno) ;
    List<Turno> listarTurnos();

    List<Turno> turnosDisponibles();

    void generarTurnos(Long idMedicoHospital);

    void eliminarTurnosPorElMedicoHospitalId(Long idMedicoHospital);

    void asignar(DatosAsignarTurnoDTO datosAsignarTurno) throws AccesoDenegadoException;

    List<Turno> turnosPaciente(Paciente paciente);

    void cancelarTurno(Long id, Long uId) throws AccesoDenegadoException;

    List<Turno> proximosTurnosPaciente(Paciente paciente);

    List<Turno> listarTurnosConPacienteAsignadoSegunLaFecha(LocalDate fecha);

    List<Turno> listarTurnosAsignadosPorIdMedicoYFecha(Long idMedico, LocalDate fecha);

    List<Turno> listarTurnosAsignadosPorIdMedico(Long id);
    Double calcularValorConsulta(Turno turno) ;
    List<Turno> ListarturnosDelDia(LocalDate fechaDeHoy);

    void cambiarEstadoDelPago(Turno turno);
    List<Paciente> listarPacientesPorIdMedico(Medico medico);

    DatosAsignarTurnoDTO getDatosdelTurnoPorId(Long id);
   DatosAsignarTurnoDTO cargarDatosDelTurno(Turno turnoAModificar) ;

    void actualizarTurno(DatosAsignarTurnoPacienteRecepcionDTO datosTurno);
 Turno modificarTurno(Turno turnoAModificar, DatosAsignarTurnoDTO datosTurno) ;
}
