package ar.edu.unlam.tallerweb1.domain.excepciones;

public class ValidacionRecepcionException extends Exception{
    public ValidacionRecepcionException(String mensaje){
        super(mensaje);
    }

}
