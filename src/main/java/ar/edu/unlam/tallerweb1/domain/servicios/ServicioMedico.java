package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroMedicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;

import java.util.List;

public interface ServicioMedico {
    void guardarMedico(DatosRegistroMedicoDTO datosMedico) throws ValidacionMedicoException;

    List<Medico> listarMedicos();
    void eliminarMedicoPorElId(Long id);

    DatosRegistroMedicoDTO getDatosMedicoPorElId(Long id);

    void actualizarMedico(DatosRegistroMedicoDTO datosMedico) throws ValidacionMedicoException;

    Medico buscarPorId(Long id);

    Medico buscarPorUsuarioId(Long idUsuario);
}
