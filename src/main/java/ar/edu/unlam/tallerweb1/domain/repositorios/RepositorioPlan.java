package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Plan;

import java.util.List;

public interface RepositorioPlan {
    void guardar(Plan nuevoPlan);

    List<Plan> obtenerPlanes();

    void eliminarPlan(Plan planAEliminar);

    Plan buscarPlan(Long id);

    void modificarPlan(Plan planAModificar);

    Plan buscarPlanPorNombre(String nombre);
}
