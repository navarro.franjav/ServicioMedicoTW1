package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Factura;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;

import java.util.List;

public interface ServicioFactura {
    void generarFacturasMensuales();
    void verificarFacturaImpaga();
    List<Factura> getFacturas(Paciente paciente);
    List<Factura> verTodas();

    void generarFacturaIndividual(Long id);

    void pagar(Long id);

    void verificarFacturaPagas();

    Factura getFacturaById(Long id);
}
