package ar.edu.unlam.tallerweb1.domain.excepciones;

public class PlanExisteException extends Exception {
    public PlanExisteException(String message){
        super(message);
    }
}
