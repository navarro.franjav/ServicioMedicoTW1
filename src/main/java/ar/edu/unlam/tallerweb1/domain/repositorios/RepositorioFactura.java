package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Factura;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;

import java.util.List;

public interface RepositorioFactura {
    void guardar(Factura factura);

    Factura buscarPorPaciente(Paciente paciente);

    List<Factura> buscarTodasPorPaciente(Paciente paciente);

    List<Factura> buscarTodas();

    Factura buscarPorId(Long id);

    void actualizar(Factura factura);
}
