package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;
import ar.edu.unlam.tallerweb1.domain.clases.Cobro;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class RepositorioCobroImp implements RepositorioCobro{
    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioCobroImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void guardarCobro(Cobro cobro) {
        sessionFactory.getCurrentSession().save(cobro);
    }

    @Override
    public Caja buscarCajaPorFechaDelDia(LocalDate fechaDeHoy) {
        return (Caja) sessionFactory.getCurrentSession().createCriteria(Caja.class)
                .add(Restrictions.eq("fecha", fechaDeHoy))
                .uniqueResult();
    }

    @Override
    public List<Cobro> listarCobrosDelDia() {
        LocalDate currentDate = LocalDate.now();

        String hql = "FROM Cobro c WHERE c.fecha = :fecha ORDER BY c.fecha DESC";
        return sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("fecha", currentDate)
                .getResultList();    }


}
