package ar.edu.unlam.tallerweb1.domain.configuracion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class ConfiguracionMail {
    @Autowired
    private Properties emailProperties;

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailProperties.getProperty("spring.mail.host"));
        mailSender.setPort(Integer.parseInt(emailProperties.getProperty("spring.mail.port")));
        mailSender.setUsername(emailProperties.getProperty("spring.mail.username"));
        mailSender.setPassword(emailProperties.getProperty("spring.mail.password"));

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        return mailSender;
    }


}

