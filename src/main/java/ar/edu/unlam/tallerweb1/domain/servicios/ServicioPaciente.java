package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;
import ar.edu.unlam.tallerweb1.domain.excepciones.ClienteNoEncontradoException;
import ar.edu.unlam.tallerweb1.delivery.DatosPacientePlanDTO;
import ar.edu.unlam.tallerweb1.delivery.DatosRegistroPacienteDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;

import java.text.ParseException;
import java.util.List;

public interface ServicioPaciente {

    List<Paciente> listarPacientes();

    Paciente buscar(Long id);

    void eliminar(Long id) throws ClienteNoEncontradoException;

    Paciente buscarPorUsuarioId(Long id);

    void guardarPaciente(DatosRegistroPacienteDTO datosPaciente) throws ParseException;

    DatosRegistroPacienteDTO cargarDatosPaciente(Long id);

    void cambiarPlan(DatosPacientePlanDTO datosPacientePlan);

    Paciente buscarPorEmail(String email);

    List<RegistroClinico> historialClinico(Paciente paciente);
}
