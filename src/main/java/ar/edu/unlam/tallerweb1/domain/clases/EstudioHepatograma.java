package ar.edu.unlam.tallerweb1.domain.clases;

import javax.persistence.*;
@Entity
public class EstudioHepatograma {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Estudio estudio;
    private double bilirrubinaTotal;
    private double bilirrubinaDirecta;
    private double fosfatasasAlcalinas;
    private double colesterolTotal;
    private double proteinasTotales;
    private double albumina;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    public double getBilirrubinaTotal() {
        return bilirrubinaTotal;
    }

    public void setBilirrubinaTotal(double bilirrubinaTotal) {
        this.bilirrubinaTotal = bilirrubinaTotal;
    }

    public double getBilirrubinaDirecta() {
        return bilirrubinaDirecta;
    }

    public void setBilirrubinaDirecta(double bilirrubinaDirecta) {
        this.bilirrubinaDirecta = bilirrubinaDirecta;
    }

    public double getFosfatasasAlcalinas() {
        return fosfatasasAlcalinas;
    }

    public void setFosfatasasAlcalinas(double fosfatasasAlcalinas) {
        this.fosfatasasAlcalinas = fosfatasasAlcalinas;
    }

    public double getColesterolTotal() {
        return colesterolTotal;
    }

    public void setColesterolTotal(double colesterolTotal) {
        this.colesterolTotal = colesterolTotal;
    }

    public double getProteinasTotales() {
        return proteinasTotales;
    }

    public void setProteinasTotales(double proteinasTotales) {
        this.proteinasTotales = proteinasTotales;
    }

    public double getAlbumina() {
        return albumina;
    }

    public void setAlbumina(double albumina) {
        this.albumina = albumina;
    }
}
