package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.delivery.DatosRegistroClinicoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("repositorioRegistro")

public class RepositorioRegistroImpl implements RepositorioRegistro{
    private SessionFactory sessionFactory;
    @Autowired
    public RepositorioRegistroImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
    public RegistroClinico buscarPorId(Long idRegistro) {
        return (RegistroClinico) sessionFactory.getCurrentSession().createCriteria(RegistroClinico.class)
                .add(Restrictions.eq("id", idRegistro))
                .uniqueResult();
    }

    @Override
    public RegistroClinico buscarPorCodigo(String codigoRegistro) {
        return (RegistroClinico) sessionFactory.getCurrentSession().createCriteria(RegistroClinico.class)
                .add(Restrictions.eq("codigo", codigoRegistro))
                .uniqueResult();
    }

    @Override
    public void crear(RegistroClinico registroClinico) {
        sessionFactory.getCurrentSession().save(registroClinico);
    }
}
