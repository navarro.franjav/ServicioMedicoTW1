package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.DiaTrabajo;

import java.util.List;

public interface RepositorioDiaTrabajo {
    List<String> obtenerDiasCargadosMedicoHospital(Long idMedicoHospital);

    void guardarDiaTrabajo(DiaTrabajo nuevoDiaTrabajo);

    List<DiaTrabajo> obtenerDiasMedicoHospital(Long id);

    void eliminarDiaTrabajoPorElMedicoHospitalId(Long idMedicoHospital);
}
