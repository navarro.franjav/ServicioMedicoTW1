package ar.edu.unlam.tallerweb1.domain.excepciones;

public class AccesoDenegadoException extends Exception {
    public AccesoDenegadoException (String message){
        super(message);
    }
}
