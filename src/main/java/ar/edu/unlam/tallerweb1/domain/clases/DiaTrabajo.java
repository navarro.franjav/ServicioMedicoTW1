package ar.edu.unlam.tallerweb1.domain.clases;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
public class DiaTrabajo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombreDia;

    private LocalTime horaInicio;

    private LocalTime horaFin;

    @ManyToOne
    private Medico_Hospital medicoHospital;

    // getters y setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreDia() {
        return nombreDia;
    }

    public void setNombreDia(String nombreDia) {
        this.nombreDia = nombreDia;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public LocalTime getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(LocalTime horaFin) {
        this.horaFin = horaFin;
    }

    public Medico_Hospital getMedicoHospital() {
        return medicoHospital;
    }

    public void setMedicoHospital(Medico_Hospital medicoHospital) {
        this.medicoHospital = medicoHospital;
    }
}
