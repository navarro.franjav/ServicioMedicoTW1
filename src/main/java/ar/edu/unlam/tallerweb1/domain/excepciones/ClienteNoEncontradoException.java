package ar.edu.unlam.tallerweb1.domain.excepciones;

public class ClienteNoEncontradoException extends Exception {
    public ClienteNoEncontradoException() {
        super("No se encontró al paciente en la base de datos");
    }
}
