package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;
import ar.edu.unlam.tallerweb1.domain.clases.Cobro;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioCobro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Transactional
@Service
public class ServicioCobroImp implements ServicioCobro{
     private RepositorioCobro repositorioCobro;
     private ServicioTurno servicioTurno;
    @Autowired
    public ServicioCobroImp(RepositorioCobro repositorioCobro, ServicioTurno servicioTurno){
        this.repositorioCobro=repositorioCobro;
    }




    @Override
    public List<Cobro> listarCobrosDelDia() {
        return this.repositorioCobro.listarCobrosDelDia();
    }




    @Override
    public void guardarCobro(Turno turno) {
        Cobro cobro= new Cobro();
        cobro.setMonto(turno.getValorConsulta());
        cobro.setTurno(turno);
        cobro.setFecha(LocalDate.now());
        repositorioCobro.guardarCobro(cobro);


    }

    private Caja buscarCajaDelDia() {
        LocalDate fechaDeHoy = LocalDate.now();
       return repositorioCobro.buscarCajaPorFechaDelDia(fechaDeHoy);
    }
}
