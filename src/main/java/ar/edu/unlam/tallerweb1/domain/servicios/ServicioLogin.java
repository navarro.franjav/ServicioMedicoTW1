package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.clases.Usuario;

import javax.servlet.http.HttpServletRequest;

// Interface que define los metodos del Servicio de Usuarios.
public interface ServicioLogin {

	Usuario consultarUsuario(String email, String password);

    void esAdmin(HttpServletRequest request) throws AccesoDenegadoException;

    void esMedico(HttpServletRequest request) throws AccesoDenegadoException;

    void esPaciente(HttpServletRequest request) throws AccesoDenegadoException;

    void esRecepcionista(HttpServletRequest request) throws AccesoDenegadoException;
}
