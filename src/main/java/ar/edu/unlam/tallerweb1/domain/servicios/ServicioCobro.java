package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.clases.Cobro;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;

import java.util.List;

public interface ServicioCobro {

    
    List<Cobro> listarCobrosDelDia();


    void guardarCobro(Turno turno);
}
