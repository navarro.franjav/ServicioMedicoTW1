package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Caja;
import ar.edu.unlam.tallerweb1.domain.clases.Cobro;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RepositorioCobro {

    void guardarCobro(Cobro cobro);

    Caja buscarCajaPorFechaDelDia(LocalDate fechaDeHoy);

    List<Cobro> listarCobrosDelDia();
}
