package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.RegistroClinico;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("repositorioCliente")

public class RepositorioPacienteImpl implements RepositorioPaciente {
    private SessionFactory sessionFactory;

    @Autowired
    public RepositorioPacienteImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void guardar(Paciente nuevoPaciente) {
        this.sessionFactory.getCurrentSession().save(nuevoPaciente);
    }

    @Override
    public List<Paciente> listar() {
        return sessionFactory.getCurrentSession()
                .createQuery("FROM Paciente p JOIN FETCH p.usuario JOIN FETCH p.plan")
                .list();
    }



    @Override
    public Paciente buscar(Long id) {
        return (Paciente) sessionFactory.getCurrentSession().createCriteria(Paciente.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    public void eliminar(Paciente paciente) {
        sessionFactory.getCurrentSession().delete(paciente);
    }

    @Override
    public void editar(Paciente pacienteEditado) {
        sessionFactory.getCurrentSession().update(pacienteEditado);
    }

    @Override
    public Paciente buscarPorUsuarioId(Long id) {
        String hql = "FROM Paciente p JOIN FETCH p.usuario JOIN FETCH p.plan WHERE p.usuario.id = :usuarioId";
        return (Paciente) sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("usuarioId", id)
                .uniqueResult();
    }

    @Override
    public List<Paciente> buscarPacientesActivos() {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Paciente> query = criteriaBuilder.createQuery(Paciente.class);
        Root<Paciente> root = query.from(Paciente.class);
        query.select(root).where(criteriaBuilder.isTrue(root.get("activo")));

        return sessionFactory.getCurrentSession().createQuery(query).getResultList();
    }

    @Override
    public List<Paciente> buscarPacientesInactivos() {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Paciente> query = criteriaBuilder.createQuery(Paciente.class);
        Root<Paciente> root = query.from(Paciente.class);
        query.select(root).where(criteriaBuilder.isFalse(root.get("activo")));

        return sessionFactory.getCurrentSession().createQuery(query).getResultList();
    }

    @Override
    public Paciente buscarPorDNI(Long dniPaciente) {
        String hql = "FROM Paciente p  WHERE p.usuario.dni = :dniPaciente";
        return (Paciente) sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("dniPaciente", dniPaciente)
                .uniqueResult();
    }
    @Override
    public List<RegistroClinico> getHistorial(Paciente paciente) {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<RegistroClinico> query = criteriaBuilder.createQuery(RegistroClinico.class);
        Root<RegistroClinico> root = query.from(RegistroClinico.class);
        query.where(criteriaBuilder.equal(root.get("paciente"), paciente));

        return sessionFactory.getCurrentSession().createQuery(query).getResultList();
    }

}