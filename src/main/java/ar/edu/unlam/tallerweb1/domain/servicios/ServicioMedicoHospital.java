package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosMedicoHospitalDTO;
import ar.edu.unlam.tallerweb1.delivery.DatosMedicoHospitalDiaDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;

import java.util.List;

public interface ServicioMedicoHospital {

    void guardarMedicoHospital(DatosMedicoHospitalDTO datosMedicoHospitalDTO);

    List<Medico_Hospital> listarMedicoHospital();

    List<Medico_Hospital> listarMedicoHospitalPorElIdMedico(Long id);

    Medico_Hospital obtenerElUltimoCreado();

    List<DatosMedicoHospitalDiaDTO> obtenerDatosMedicoHospitalDias(Long medicoId);

    Long obtenerIdMedico(Long idMedicoHospital);

    List<Turno> obtenerTurnosMedicoHospital(Long idMedicoHospital);

    Medico_Hospital obtenerPorId(Long idMedicoHospital);
    void eliminarMedicoHospital(Long idMedicoHospital);
}
