package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosDiaTrabajoDTO;
import ar.edu.unlam.tallerweb1.domain.clases.DiaTrabajo;
import ar.edu.unlam.tallerweb1.domain.enums.Dias;
import ar.edu.unlam.tallerweb1.domain.excepciones.HoraInicioMayorAHoraFinException;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioDiaTrabajo;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioMedicoHospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("servicioDiaTrabajo")
@Transactional
public class ServicioDiaTrabajoImpl implements  ServicioDiaTrabajo{
    private RepositorioDiaTrabajo repositorioDiaTrabajo;
    private RepositorioMedicoHospital repositorioMedicoHospital;

    @Autowired
    public ServicioDiaTrabajoImpl(RepositorioDiaTrabajo repositorioDiaTrabajo, RepositorioMedicoHospital repositorioMedicoHospital)
    {
        this.repositorioDiaTrabajo = repositorioDiaTrabajo;
        this.repositorioMedicoHospital = repositorioMedicoHospital;
    }


    @Override
    public List<Dias> obtenerDiasDisponiblesMedicoHospital(Long idMedicoHospital) {
        List<String> listaDiasCargados = repositorioDiaTrabajo.obtenerDiasCargadosMedicoHospital(idMedicoHospital);
        List<Dias> diasDisponibles = new ArrayList<>();

        for (Dias dia : Dias.values()) {
            if (!listaDiasCargados.contains(dia.name())){
                diasDisponibles.add(dia);
            }
        }
        return diasDisponibles;
    }

    @Override
    public void guardarDiaTrabajo(DatosDiaTrabajoDTO datosDiaTrabajo) throws HoraInicioMayorAHoraFinException {
        try {
            validarHorario(datosDiaTrabajo);
            DiaTrabajo nuevoDiaTrabajo = crearDiaTrabajo(datosDiaTrabajo);
            repositorioDiaTrabajo.guardarDiaTrabajo(nuevoDiaTrabajo);
        }
        catch (HoraInicioMayorAHoraFinException ex){
            throw  ex;
        }
    }

    @Override
    public void eliminarDiaTrabajoPorELMedicoHospitalId(Long idMedicoHospital) {
        repositorioDiaTrabajo.eliminarDiaTrabajoPorElMedicoHospitalId(idMedicoHospital);
    }

    private DiaTrabajo crearDiaTrabajo(DatosDiaTrabajoDTO datosDiaTrabajo) {
        DiaTrabajo nuevoDiaTrabajo = new DiaTrabajo();
        nuevoDiaTrabajo.setNombreDia(datosDiaTrabajo.getNombreDia());
        nuevoDiaTrabajo.setHoraInicio(datosDiaTrabajo.getHoraInicio());
        nuevoDiaTrabajo.setHoraFin(datosDiaTrabajo.getHoraFin());
        nuevoDiaTrabajo.setMedicoHospital(repositorioMedicoHospital.obtenerPorId(datosDiaTrabajo.getMedicoHospitalId()));

        return nuevoDiaTrabajo;
    }

    private void validarHorario(DatosDiaTrabajoDTO datosDiaTrabajo) throws HoraInicioMayorAHoraFinException {
        if (!datosDiaTrabajo.getHoraFin().isAfter(datosDiaTrabajo.getHoraInicio())){
            throw new HoraInicioMayorAHoraFinException("La hora de inicio debe ser menor que la hora de fin");
        }
    }
}
