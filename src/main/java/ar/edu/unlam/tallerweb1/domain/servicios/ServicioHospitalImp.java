package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.delivery.DatosEspecialidadDTO;
import ar.edu.unlam.tallerweb1.delivery.DatosHospitalDTO;
import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Hospital;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioHospital;
import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioHospitalImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import java.util.List;

@Service("servicioHospital")
@Transactional
public class ServicioHospitalImp implements ServicioHospital{
    private RepositorioHospital repositorioHospital;
    @Autowired
    public ServicioHospitalImp(RepositorioHospital repositorioHospital){
        this.repositorioHospital= repositorioHospital;
    }
    @Override
    public void guardarHospital(DatosHospitalDTO datosNuevoHospital) {
        Hospital nuevoHospital=crearHospital(datosNuevoHospital);
        this.repositorioHospital.guardarHospital(nuevoHospital);
    }

    private Hospital crearHospital(DatosHospitalDTO datosNuevoHospital) {
        Hospital hospital = new Hospital();
        hospital.setNombre(datosNuevoHospital.getNombre());
        hospital.setCodigoPostal(datosNuevoHospital.getCodigoPostal());
        hospital.setTelefono(datosNuevoHospital.getTelefono());
        hospital.setCiudad(datosNuevoHospital.getCiudad());
        hospital.setDirecion(datosNuevoHospital.getDirecion());
        return hospital;
    }

    @Override
    public List<Hospital> listarHospitales() {
        List<Hospital> listaHospitales =this.repositorioHospital.listar();
        return listaHospitales;
    }

    @Override
    public Hospital getHospitalPorId(Long id) {
        Hospital hospitalBuscado= this.repositorioHospital.getHospitalPorId(id);
        return hospitalBuscado;
    }

    @Override
    public void guardarCambiosHospital(Hospital hospital) {
        this.repositorioHospital.modificarHospital(hospital);
    }

    @Override
    public void eliminarHospital(Long id) {
        Hospital hospital=this.repositorioHospital.getHospitalPorId(id);
        this.repositorioHospital.eliminarHospital(hospital);
    }

    @Override
    public void actualizarHospital(DatosHospitalDTO datosHospital) {
        Hospital hospitalAModificar= this.repositorioHospital.getHospitalPorId(datosHospital.getId());
        Hospital hospitalModificado= modificarHospital(hospitalAModificar, datosHospital);
        this.repositorioHospital.modificarHospital(hospitalModificado);
    }

    private Hospital modificarHospital(Hospital hospitalAModificar, DatosHospitalDTO datosHospital) {
        hospitalAModificar.setNombre(datosHospital.getNombre());
        hospitalAModificar.setDirecion(datosHospital.getDirecion());
        hospitalAModificar.setCiudad(datosHospital.getCiudad());
        hospitalAModificar.setTelefono(datosHospital.getTelefono());
        hospitalAModificar.setCodigoPostal(datosHospital.getCodigoPostal());
        return hospitalAModificar;
    }
}
