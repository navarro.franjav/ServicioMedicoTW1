package ar.edu.unlam.tallerweb1.domain.servicios;

import ar.edu.unlam.tallerweb1.domain.repositorios.RepositorioUsuario;
import ar.edu.unlam.tallerweb1.domain.clases.Usuario;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("servicioUsuario")
@Transactional
public class ServicioUsuarioImpl implements ServicioUsuario {
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    public ServicioUsuarioImpl( RepositorioUsuario repositorioUsuario ){
        this.repositorioUsuario=repositorioUsuario;
    }
    @Override
    public void guardarUsuario(Usuario nuevoUsuario) {
        this.repositorioUsuario.guardar(nuevoUsuario);
    }

    @Override
    public Usuario getUsuarioByDNI(Long dni) {
        Usuario usuarioBuscado = this.repositorioUsuario.buscarUsuarioDNI(dni);
        return usuarioBuscado;
    }

    @Override
    public void modificarUsuario(Usuario usuarioEditado) {
        this.repositorioUsuario.modificar(usuarioEditado);
    }

    @Override
    public void editarUsuario(Usuario usuarioEditado, String nombre, String apellido, String email, String telefono, Long dni, String password, int i) {
        try{
            usuarioEditado.setNombre(nombre);
            usuarioEditado.setApellido(apellido);
            usuarioEditado.setEmail(email);
            usuarioEditado.setTelefono(telefono);
            usuarioEditado.setDni(dni);
            usuarioEditado.setPassword(password);
            usuarioEditado.setRol(3);
            this.modificarUsuario(usuarioEditado);
        }catch (RuntimeException  ex){

        }
    }

    @Override
    public Usuario crearUsuario(String nombre, String apellido, String email, String telefono, Long dni, String password, int i) {
        try{
            Usuario nuevoUsuario = new Usuario();
            nuevoUsuario.setNombre(nombre);
            nuevoUsuario.setApellido(apellido);
            nuevoUsuario.setEmail(email);
            nuevoUsuario.setTelefono(telefono);
            nuevoUsuario.setDni(dni);
            nuevoUsuario.setPassword(password);
            nuevoUsuario.setRol(3);
            this.guardarUsuario(nuevoUsuario);
            Usuario usuarioGuardado = this.getUsuarioByDNI(dni);
            return usuarioGuardado;
        }catch (RuntimeException  ex){

        }
        return null;
    }

    @Override
    public Usuario buscarUsuarioPorId(Long id) {
        return this.repositorioUsuario.buscarUsuarioPorElId(id);
    }


}
