package ar.edu.unlam.tallerweb1.domain.repositorios;

import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;

import java.util.List;

public interface RepositorioMedicoHospital {
    void guardarMedicoHospital(Medico_Hospital medicoHospital);

    List<Medico_Hospital> listarMedicoHospital();

    List<Medico_Hospital> listarMedicoHospitalPorElIdMedico(Long id);

    Medico_Hospital obtenerElUltimoCreado();

    Medico_Hospital obtenerPorId(Long medicoHospitalId);

    Long obtenerIdMedico(Long idMedicoHospital);

    void eliminarMedicoHospital(Medico_Hospital medicoHospital);
}
