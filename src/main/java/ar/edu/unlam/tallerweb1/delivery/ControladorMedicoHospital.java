package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.*;
import ar.edu.unlam.tallerweb1.domain.enums.Dias;
import ar.edu.unlam.tallerweb1.domain.enums.HorarioLaboral;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.HoraInicioMayorAHoraFinException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Controller
public class ControladorMedicoHospital {

    private ServicioHospital servicioHospital;
    private ServicioMedico servicioMedico;
    private ServicioMedicoHospital servicioMedicoHospital;
    private ServicioDiaTrabajo servicioDiaTrabajo;
    private ServicioEspecialidad servicioEspecialidad;
    private ServicioTurno servicioTurno;
    private ServicioLogin servicioLogin;
    @Autowired
    public ControladorMedicoHospital(ServicioHospital servicioHospital, ServicioMedico servicioMedico
                                    , ServicioMedicoHospital servicioMedicoHospital, ServicioDiaTrabajo servicioDiaTrabajo
                                    ,ServicioEspecialidad servicioEspecialidad, ServicioTurno servicioTurno, ServicioLogin servicioLogin){
        this.servicioHospital = servicioHospital;
        this.servicioMedico = servicioMedico;
        this.servicioMedicoHospital = servicioMedicoHospital;
        this.servicioDiaTrabajo = servicioDiaTrabajo;
        this.servicioEspecialidad = servicioEspecialidad;
        this.servicioTurno = servicioTurno;
        this.servicioLogin= servicioLogin;
    }

    @RequestMapping(path = "/lista-medico-hospital",method = RequestMethod.GET)
    public ModelAndView irAListaMedicoHospital(@RequestParam("id") Long id, HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            model.put("medico", buscarMedicoPorElId(id));
            List<DatosMedicoHospitalDiaDTO> listaDatosMedicoHospital = obtenerDatosMedicoHospitalDias(id);
            model.put("listaMedicoHospitalDia", listaDatosMedicoHospital);
            return new ModelAndView("Admin/lista-medico-hospital", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/eliminar-hospitalAsignado", method = RequestMethod.GET)
    public ModelAndView eliminarMedicoHospital(@RequestParam("idMedicoHospital") Long idMedicoHospital, HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            Long idMedico = this.servicioMedicoHospital.obtenerIdMedico(idMedicoHospital);
            servicioTurno.eliminarTurnosPorElMedicoHospitalId(idMedicoHospital);
            servicioDiaTrabajo.eliminarDiaTrabajoPorELMedicoHospitalId(idMedicoHospital);
            servicioMedicoHospital.eliminarMedicoHospital(idMedicoHospital);
            return new ModelAndView("redirect:/lista-medico-hospital?id="+idMedico);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }

    }

    @RequestMapping(path = "/asignar-medico-hospital", method = RequestMethod.GET)
    public ModelAndView irAAsignarHospital(@RequestParam("id") Long id, HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            model.put("datosMedicoHospital", new DatosMedicoHospitalDTO());
            model.put("hospitales", listarHospitales());
            model.put("especialidades", listaEspecialidades());
            model.put("id_medico", id);
            return new ModelAndView("Admin/asignar-medico-hospital", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }

    }

    @RequestMapping(path = "/asignar-medico-hospital", method = RequestMethod.POST)
    public ModelAndView asignarMedicoHospital(@ModelAttribute("datosMedicoHospital") DatosMedicoHospitalDTO datosMedicoHospital, HttpServletRequest request) {
       try {
           this.servicioLogin.esAdmin(request);
           servicioMedicoHospital.guardarMedicoHospital(datosMedicoHospital);
           return new ModelAndView("redirect:/asignar-dias");
       } catch (AccesoDenegadoException e){
           return new ModelAndView("redirect:/home");
       }
    }

    @RequestMapping(path = "/asignar-dias", method = RequestMethod.GET)
    public ModelAndView irAAsignarDias(HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            agregarDatosParaAsignarDias(model);
            return new ModelAndView("Admin/asignar-dias-medicoHospital", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }



    @RequestMapping(path = "/asignar-dias", method = RequestMethod.POST)
    public ModelAndView asignarDia(@ModelAttribute("datosDiaTrabajo") DatosDiaTrabajoDTO datosDiaTrabajo, HttpServletRequest request) throws HoraInicioMayorAHoraFinException {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            try{
                //guardar dia
                servicioDiaTrabajo.guardarDiaTrabajo(datosDiaTrabajo);
                agregarDatosParaAsignarDias(model);
                model.put("asignarMasDias", true);
                return new ModelAndView("Admin/asignar-dias-medicoHospital", model);
            }catch (HoraInicioMayorAHoraFinException ex){
                agregarDatosParaAsignarDias(model);
                model.put("mensajeDeError", ex.getMessage());
                model.put("datosDiaTrabajo", datosDiaTrabajo);
                return new ModelAndView("Admin/asignar-dias-medicoHospital", model);
            }
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    private List<Hospital> listarHospitales() {
        return servicioHospital.listarHospitales();
    }

    private Medico buscarMedicoPorElId(Long id) {
        return servicioMedico.buscarPorId(id);
    }

    private List<Dias> obtenerDiasDisponibles(Long idMedicoHospital) {
        return servicioDiaTrabajo.obtenerDiasDisponiblesMedicoHospital(idMedicoHospital);
    }

    private void agregarDatosParaAsignarDias(ModelMap model) {
        Medico_Hospital medicoHospital = servicioMedicoHospital.obtenerElUltimoCreado();
        model.put("datosDiaTrabajo", new DatosDiaTrabajoDTO());
        model.put("horarioLaboral", HorarioLaboral.values());
        model.put("diasDisponibles", obtenerDiasDisponibles(medicoHospital.getId()));
        model.put("medicoHospital", medicoHospital);
    }

    private List<DatosMedicoHospitalDiaDTO> obtenerDatosMedicoHospitalDias(Long medicoId) {
        return servicioMedicoHospital.obtenerDatosMedicoHospitalDias(medicoId);
    }

    @RequestMapping(path = "/lista-turnosGenerados", method = RequestMethod.GET)
    public ModelAndView irListaDeTurnos(@RequestParam("id") Long idMedicoHospital, HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            model.put("turnosMedicoHospital", listarTurnosMedicoHospital(idMedicoHospital));
            model.put("medicoHospital", servicioMedicoHospital.obtenerPorId(idMedicoHospital));
            return new ModelAndView("Admin/lista-TurnosGenerados", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    private List<Turno> listarTurnosMedicoHospital(Long idMedicoHospital) {
        return servicioMedicoHospital.obtenerTurnosMedicoHospital(idMedicoHospital);
    }

    private List<Especialidad> listaEspecialidades() {
        return servicioEspecialidad.listarEspecialidades();
    }
}
