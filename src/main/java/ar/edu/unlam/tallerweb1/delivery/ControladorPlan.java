package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Plan_Especialidad;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.PlanExisteException;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioEspecialidad;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioLogin;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlan;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlanEspecialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ControladorPlan {
    private ServicioPlan servicioPlan;
    private ServicioLogin servicioLogin;
    private ServicioEspecialidad servicioEspecialidad;
    private ServicioPlanEspecialidad servicioPlanEspecialidad;

    @Autowired
    public ControladorPlan(ServicioPlan servicioPlan, ServicioLogin servicioLogin, ServicioEspecialidad servicioEspecialidad,
                            ServicioPlanEspecialidad servicioPlanEspecialidad){
        this.servicioLogin=servicioLogin;
        this.servicioPlan=servicioPlan;
        this.servicioEspecialidad= servicioEspecialidad;
        this.servicioPlanEspecialidad=servicioPlanEspecialidad;
    }

    @RequestMapping(path = "/lista-planes")
    public ModelAndView verListaPlanes(HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            List<Plan> listaPlanes=servicioPlan.listarPlanes();
            List<Plan_Especialidad> listaPlanEspecialidad=servicioPlanEspecialidad.listarPlanEspecialidades();
            ModelMap model= new ModelMap();
            model.put("planes", listaPlanes);
            model.put("listaPlanEspecialidad", listaPlanEspecialidad);
            return new ModelAndView("/Admin/lista-planes", model);
        } catch (AccesoDenegadoException e){
            String mensaje=e.getMessage();
            return accesoDegenado(mensaje, request);
        }
    }

    @RequestMapping("/crear-plan")
    public ModelAndView irACrearPlan(HttpServletRequest request){
        ModelMap model= new ModelMap();
        try {
            this.servicioLogin.esAdmin(request);
            model.put("datosPlan", new DatosPlanDTO());
            model.put("titulo", "Crear");
            model.put("url", "crear-plan");
            model.put("listaEspecialidades", this.servicioEspecialidad.listarEspecialidades());
        } catch (AccesoDenegadoException e){
            String mensaje=e.getMessage();
            return accesoDegenado(mensaje, request);
        } return new ModelAndView("Admin/crear-plan", model);

    }

    @RequestMapping(path="/crear-plan", method= RequestMethod.POST)
    public ModelAndView registrarPlan(@ModelAttribute ("DatosPlanDTO") DatosPlanDTO datosNuevoPlan, HttpServletRequest request){
        ModelMap modelo = new ModelMap();
        String mensaje;
        try {
            this.servicioLogin.esAdmin(request);
            try{
            Plan plan = this.servicioPlan.guardarPlan(datosNuevoPlan);
            List<Especialidad> especialidadesSeleccionadas=buscarEspecialidadesSeleccionadasPorId(datosNuevoPlan.getListaEspecialidadesSeleccionadas());
            this.servicioPlanEspecialidad.guardarPlanEspecialidades(plan, especialidadesSeleccionadas);}
            catch (PlanExisteException e){
                mensaje=e.getMessage();
                return registroFallido(mensaje, request);
            }
        } catch (AccesoDenegadoException e){
            mensaje=e.getMessage();
            return accesoDegenado(mensaje, request);
        }
        mensaje="Plan añadido correctamente";
        return registroExitoso(mensaje, request);
    }

    private ModelAndView accesoDegenado(String mensaje,HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("errorAcceso", mensaje);
        return new ModelAndView("redirect:/home");
    }

    private ModelAndView registroFallido(String mensaje, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("errorPlanExistente", mensaje);
        return new ModelAndView("redirect:/crear-plan");
    }

    private ModelAndView registroExitoso(String mensaje,HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("exitoso", mensaje);
        return new ModelAndView("redirect:/lista-planes");
    }

    private List<Especialidad> buscarEspecialidadesSeleccionadasPorId(List<Long> listaEspecialidadesSeleccionadas) {
        List<Especialidad> especialidadesSeleccionadas = new ArrayList<>();
        for (Long idEspecialidad : listaEspecialidadesSeleccionadas) {
            Especialidad especialidad=this.servicioEspecialidad.getEspecialidadById(idEspecialidad);
            especialidadesSeleccionadas.add(especialidad);
        }
        return especialidadesSeleccionadas;
    }

    @RequestMapping(path = "/eliminar-plan")
    public ModelAndView eliminarPlan(@RequestParam("id") Long id, HttpServletRequest request){
        try {
            this.servicioLogin.esAdmin(request);
            servicioPlan.eliminarPlanPorId(id);
            return new ModelAndView("redirect:/lista-planes");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/modificar-plan")
    public ModelAndView modificarPlan(@RequestParam("id") Long id, HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model= new ModelMap();
            DatosPlanDTO datosPlan=servicioPlan.getDatosPlanPorId(id);
            model.put("datosPlan",datosPlan);
            model.put("titulo", "Editar");
            model.put("url", "modificar-plan");
            model.put("listaEspecialidades", this.servicioEspecialidad.listarEspecialidades());
            return new ModelAndView("/Admin/crear-plan", model);
        } catch( AccesoDenegadoException e) {
            return new ModelAndView("redirect:/login");
        }
    }

    @RequestMapping(path = "/modificar-plan", method = RequestMethod.POST)
    public ModelAndView modificarPlan(@ModelAttribute ("DatosPlanDTO") DatosPlanDTO datosPlanModificado, HttpServletRequest request){
        try {
            this.servicioLogin.esAdmin(request);
            this.servicioPlan.modificarPlan(datosPlanModificado);
            return new ModelAndView("redirect:/lista-planes");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

}
