package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Medico;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.clases.Usuario;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Controller
public class ControladorLogin {

    // La anotacion @Autowired indica a Spring que se debe utilizar el contructor como mecanismo de inyección de dependencias,
    // es decir, qeue lo parametros del mismo deben ser un bean de spring y el framewrok automaticamente pasa como parametro
    // el bean correspondiente, en este caso, un objeto de una clase que implemente la interface ServicioLogin,
    // dicha clase debe estar anotada como @Service o @Repository y debe estar en un paquete de los indicados en
    // applicationContext.xml
    private ServicioLogin servicioLogin;
    private ServicioPaciente servicioPaciente;
    private ServicioUsuario servicioUsuario;
    private ServicioMedico servicioMedico;
    private ServicioTurno servicioTurno;
    private ServicioEstudio servicioEstudio;
    @Autowired
    public ControladorLogin(ServicioLogin servicioLogin, ServicioPaciente servicioPaciente,
                            ServicioUsuario servicioUsuario, ServicioMedico servicioMedico,
                            ServicioTurno servicioTurno, ServicioEstudio servicioEstudio) {
        this.servicioPaciente = servicioPaciente;
        this.servicioLogin = servicioLogin;
        this.servicioUsuario = servicioUsuario;
        this.servicioMedico = servicioMedico;
        this.servicioTurno = servicioTurno;
        this.servicioEstudio = servicioEstudio;
    }

    @RequestMapping("/login")
    public ModelAndView irALogin() {
        ModelMap model = new ModelMap();
        model.put("datosLogin", new DatosLoginDTO());
        return new ModelAndView("Common/login", model);
    }

    @RequestMapping(path = "/validar-login", method = RequestMethod.POST)
    public ModelAndView validarLogin(@ModelAttribute("datosLogin") DatosLoginDTO datosLogin, HttpServletRequest request) {
        ModelMap model = new ModelMap();
        Usuario usuarioBuscado = servicioLogin.consultarUsuario(datosLogin.getEmail(), datosLogin.getPassword());
        if (usuarioBuscado != null) {
            Integer rolUsuario = usuarioBuscado.getRol();
            String vista;
            request.getSession().setAttribute("ROL", usuarioBuscado.getRol());
            request.getSession().setAttribute("ID", usuarioBuscado.getId());
            switch (rolUsuario) {
                case 1:
                    vista = "redirect:/home-admin";
                    break;
                case 2:
                    vista = "redirect:/home-medico";
                    break;
                case 3:
                    vista = "redirect:/home-paciente";
                    break;
                case 4:
                    vista = "redirect:/home-recepcionista";
                    break;
                default:
                    vista = "/home";
                    break;
            }
            return new ModelAndView(vista);
        } else {
            model.put("error", "Usuario o clave incorrecta");
        }
        return new ModelAndView("Common/login", model);
    }

    @RequestMapping(path = "/home", method = RequestMethod.GET)
    public ModelAndView irAHome() {
        return new ModelAndView("Common/home");
    }

    @RequestMapping(path = "/home-admin")
    public ModelAndView irAHomeAdmin(HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            Usuario usuario = this.servicioUsuario.buscarUsuarioPorId((Long) request.getSession().getAttribute("ID"));
            model.put("usuario", usuario);
            return new ModelAndView("Admin/home-admin", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/home-paciente", method = RequestMethod.GET)
    public ModelAndView irAHomePaciente(HttpServletRequest request) {
        try{
            this.servicioLogin.esPaciente(request);
            ModelMap model = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            List<Turno> turnos = this.servicioTurno.proximosTurnosPaciente(paciente);
            model.put("turnos",turnos);
            model.put("paciente", paciente);
            model.put("mensaje", "Paciente encontrado");
            model.put("estudios", this.servicioEstudio.obtenerLosUltimosTresEstudiosPorIdPaciente(paciente.getId()));
            return new ModelAndView("paciente/home-paciente", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/home-recepcionista", method = RequestMethod.GET)
    public ModelAndView irAHomeRecepcionista(HttpServletRequest request) {
        try{
            this.servicioLogin.esRecepcionista(request);
            ModelMap model = new ModelMap();
            LocalDate fechaDeHoy = LocalDate.now();
            List<Turno> turnos = this.servicioTurno.ListarturnosDelDia(fechaDeHoy);
            model.put("turnos",turnos);
            return new ModelAndView("Admin/home-recepcionista", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/cerrar-sesion", method = RequestMethod.GET)
    public ModelAndView cerrarSesion(HttpServletRequest request) {
        request.getSession().invalidate();
        return new ModelAndView("redirect:/login");
    }
}
