package ar.edu.unlam.tallerweb1.delivery;

public class DatosEstudioDTO {
    Long idMedico;
    Long idPaciente;
    Long idTipoEstudio;

    public Long getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Long idMedico) {
        this.idMedico = idMedico;
    }

    public Long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Long getIdTipoEstudio() {
        return idTipoEstudio;
    }

    public void setIdTipoEstudio(Long idTipoEstudio) {
        this.idTipoEstudio = idTipoEstudio;
    }
}
