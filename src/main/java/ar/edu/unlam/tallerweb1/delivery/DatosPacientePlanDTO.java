package ar.edu.unlam.tallerweb1.delivery;

public class DatosPacientePlanDTO {
    private Long pacienteId;
    private Long planId;

    public Long getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(Long pacienteId) {
        this.pacienteId = pacienteId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }
}
