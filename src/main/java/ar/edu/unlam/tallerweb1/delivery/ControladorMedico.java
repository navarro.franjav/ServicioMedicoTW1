package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.*;
import ar.edu.unlam.tallerweb1.domain.enums.Estudios;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

@Controller
public class ControladorMedico {

    private ServicioMedico servicioMedico;
    private ServicioEspecialidad servicioEspecialidad;
    private ServicioTurno servicioTurno;
    private ServicioLogin servicioLogin;
    private ServicioEstudio servicioEstudio;
    private ServicioRegistro servicioRegistro;
    private ServicioPaciente servicioPaciente;

    @Autowired
    public ControladorMedico(ServicioMedico servicioMedico, ServicioEspecialidad servicioEspecialidad,
                             ServicioLogin servicioLogin, ServicioTurno servicioTurno, ServicioEstudio servicioEstudio,
                             ServicioRegistro servicioRegistro, ServicioPaciente servicioPaciente ){
        this.servicioMedico = servicioMedico;
        this.servicioEspecialidad = servicioEspecialidad;
        this.servicioTurno = servicioTurno;
        this.servicioLogin = servicioLogin;
        this.servicioEstudio = servicioEstudio;
        this.servicioRegistro = servicioRegistro;
        this.servicioPaciente = servicioPaciente;
    }

    @RequestMapping(path = "lista-medicos")
    public ModelAndView irAListaMedicos(HttpServletRequest request){
        try {
            this.servicioLogin.esAdmin(request);
            List<Medico> listaDeMedicos = servicioMedico.listarMedicos();
            ModelMap modelo = new ModelMap();
            modelo.put("medicos", listaDeMedicos);
            return new ModelAndView("/Admin/lista-medicos", modelo);
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/home-medico", method = RequestMethod.GET)
    public ModelAndView irAHomeMedico(HttpServletRequest request) {
        try{
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Medico medico = servicioMedico.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            modelo.put("medico", medico);
            modelo.put("turnosAsignadosHoy", listaTurnosAsignadosHoy(medico.getId()));
            return new ModelAndView("Medico/home-medico", modelo);
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/crear-medico")
    public ModelAndView irACrearMedico(HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap modelo = new ModelMap();
            modelo.put("datosMedico", new DatosRegistroMedicoDTO());
            modelo.put("titulo", "Crear");
            modelo.put("url", "crear-medico");
            return new ModelAndView("/Admin/crear-medico", modelo);
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/crear-medico", method = RequestMethod.POST)
    public ModelAndView crearMedico(@ModelAttribute ("DatosRegistroMedicoDTO") DatosRegistroMedicoDTO datosMedico, HttpServletRequest request) throws ValidacionMedicoException {
        try {
            this.servicioLogin.esAdmin(request);
            try{
                this.servicioMedico.guardarMedico(datosMedico);
                return new ModelAndView("redirect:/lista-medicos");
            }
            catch (ValidacionMedicoException ex){
                ModelMap modelo = new ModelMap();
                modelo.put("mensajeDeError", ex.getMessage());
                modelo.put("datosMedico", datosMedico);
                return new ModelAndView("/Admin/crear-medico", modelo);
            }
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "eliminar-medico")
    public ModelAndView eliminarMedico(@RequestParam ("id") Long id, HttpServletRequest request){
        try {
            this.servicioLogin.esAdmin(request);
            servicioMedico.eliminarMedicoPorElId(id);
            return new ModelAndView("redirect:/lista-medicos");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "modificar-medico")
    public ModelAndView modificarMedico(@RequestParam ("id") Long id, HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap modelo = new ModelMap();
            DatosRegistroMedicoDTO datosMedico = servicioMedico.getDatosMedicoPorElId(id);
            modelo.put("datosMedico", datosMedico);
            modelo.put("titulo", "Editar");
            modelo.put("url", "modificar-medico");
            return new ModelAndView("/Admin/crear-medico", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "modificar-medico", method = RequestMethod.POST)
    public ModelAndView modificarMedico(@ModelAttribute ("DatosRegistroMedicoDTO") DatosRegistroMedicoDTO datosMedico, HttpServletRequest request ) throws ValidacionMedicoException {
        try{
            this.servicioLogin.esAdmin(request);
            try{
                this.servicioMedico.actualizarMedico(datosMedico);
                return new ModelAndView("redirect:/lista-medicos");
            }
            catch (ValidacionMedicoException ex){
                ModelMap modelo = new ModelMap();
                modelo.put("mensajeDeError", ex.getMessage());
                modelo.put("datosMedico", datosMedico);
                modelo.put("titulo", "Editar");
                modelo.put("url", "modificar-medico");
                return new ModelAndView("/Admin/crear-medico", modelo);
            }
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "turnos-asignados")
    public ModelAndView irATurnosAsignados(HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Medico medico = servicioMedico.buscarPorUsuarioId((Long)request.getSession().getAttribute("ID"));
            modelo.put("turnosAsignados", servicioTurno.listarTurnosAsignadosPorIdMedico(medico.getId()));
            modelo.put("medico", medico);
            return new ModelAndView("/Medico/turnos-asignados", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "mis-pacientes")
    public ModelAndView irAMisPacientes(HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Medico medico = servicioMedico.buscarPorUsuarioId((Long)request.getSession().getAttribute("ID"));
            modelo.put("pacientes", servicioTurno.listarPacientesPorIdMedico(medico));
            modelo.put("medico", medico);
            return new ModelAndView("/Medico/mis-pacientes", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "seleccionar-estudio-a-cargar")
    public ModelAndView irASeleccionarEstudioACargar(@RequestParam("idPaciente") Long idPaciente, @RequestParam("idMedico") Long idMedico, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            modelo.put("estudios", servicioEstudio.listarTiposEstudios());
            modelo.put("datosEstudiosDTO", new DatosSeleccionarEstudioDTO());
            modelo.put("idPaciente", idPaciente);
            modelo.put("idMedico", idMedico);
            return new ModelAndView("/Medico/seleccionar-estudio", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "seleccionar-estudio-a-cargar",method = RequestMethod.POST)
    public ModelAndView irASeleccionarEstudioACargar(@ModelAttribute("DatosSeleccionarEstudioDTO") DatosSeleccionarEstudioDTO datosSeleccionarEstudioDTO, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            DatosEstudioDTO datosEstudioDTO = servicioEstudio.seleccionarEstudio(datosSeleccionarEstudioDTO);
            String nombreEstudio = servicioEstudio.obtenerTipoEstudioPorElId(datosEstudioDTO.getIdTipoEstudio()).getNombreEstudio();
            modelo.put("estudioDTO",datosEstudioDTO );
            modelo.put("nombreEstudio", nombreEstudio);
            return new ModelAndView("/Medico/crear-estudio-"+ nombreEstudio, modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "cargar-hemograma",method = RequestMethod.POST)
    public ModelAndView cargarHemograma(@ModelAttribute("DatosEstudioHemogramaDTO") DatosEstudioHemogramaDTO datosEstudioHemogramaDTO, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            this.servicioEstudio.cargarHemograma(datosEstudioHemogramaDTO);
            return new ModelAndView(("redirect:/mis-pacientes"));
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "cargar-colesterol", method = RequestMethod.POST)
    public ModelAndView cargarColesterol(@ModelAttribute("DatosEstudioColesterolDTO") DatosEstudioColesterolDTO datosEstudioColesterolDTO, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            this.servicioEstudio.cargarEstudioColesterol(datosEstudioColesterolDTO);
            return new ModelAndView(("redirect:/mis-pacientes"));
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "cargar-hepatograma", method = RequestMethod.POST)
    public ModelAndView cargarHepatograma(@ModelAttribute("DatosEstudioHepatogramaDTO") DatosEstudioHepatogramaDTO datosEstudioHepatogramaDTO, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            this.servicioEstudio.cargarHepatograma(datosEstudioHepatogramaDTO);
            return new ModelAndView(("redirect:/mis-pacientes"));
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "lista-estudios")
    public ModelAndView irAListaEstudiosMedicoPaciente(@RequestParam("idPaciente") Long idPaciente, @RequestParam("idMedico") Long idMedico, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            modelo.put("estudios", servicioEstudio.obtenerEstudiosPorElIdPaciente(idPaciente));
            return new ModelAndView("/Medico/lista-estudios", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "ver-estudio-Hemograma")
    public ModelAndView irAVerEstudioHemograma(@RequestParam("idEstudio") Long idEstudio, HttpServletRequest request){
        try {
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Estudio estudio = servicioEstudio.obtenerEstudioPorELId(idEstudio);
            modelo.put("estudio", servicioEstudio.obtenerHemogramaPorElIdEstudio(idEstudio));
            return new ModelAndView("/Medico/medico-ver-estudio", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }
    @RequestMapping(path = "ver-estudio-Colesterol")
    public ModelAndView irAVerEstudioColesterol(@RequestParam("idEstudio") Long idEstudio, HttpServletRequest request){
        try {
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Estudio estudio = servicioEstudio.obtenerEstudioPorELId(idEstudio);
            modelo.put("estudio", servicioEstudio.obtenerEstudioColesterolPorELIdEstudio(idEstudio));
            return new ModelAndView("/Medico/medico-ver-estudio", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "ver-estudio-Hepatograma")
    public ModelAndView irAVerEstudioHepatograma(@RequestParam("idEstudio") Long idEstudio, HttpServletRequest request){
        try {
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Estudio estudio = servicioEstudio.obtenerEstudioPorELId(idEstudio);
            modelo.put("estudio", servicioEstudio.obtenerHepatogramaPorElIdEstudio(idEstudio));
            return new ModelAndView("/Medico/medico-ver-estudio", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }


    private List<Turno> listaTurnosAsignadosHoy(Long idMedico) {
        LocalDate fechaDeHoy = LocalDate.now();
        return servicioTurno.listarTurnosAsignadosPorIdMedicoYFecha(idMedico, fechaDeHoy);
    }

    @RequestMapping(path = "historial-clinico")
    public ModelAndView irARegistroClinico(@RequestParam("idPaciente") Long idPaciente, HttpServletRequest request){
        try {
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscar(idPaciente);
            List<RegistroClinico> historial = this.servicioPaciente.historialClinico(paciente);
            modelo.put("historial", historial);
            modelo.put("paciente", paciente);
            return new ModelAndView("Medico/historial-clinico", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "registro-clinico")
    public ModelAndView irARegistroClinico(@RequestParam("codigoRegistro") String codigoRegistro, HttpServletRequest request){
        try {
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            RegistroClinico registro = this.servicioRegistro.buscarPorCodigo(codigoRegistro);
            modelo.put("registro", registro);
            return new ModelAndView("Medico/registro-clinico", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }
    @RequestMapping(path = "nuevo-registro")
    public ModelAndView irACrearRegistro(@RequestParam("idPaciente") Long idPaciente, HttpServletRequest request){
        try {
            this.servicioLogin.esMedico(request);
            ModelMap modelo = new ModelMap();
            Medico medico = this.servicioMedico.buscarPorUsuarioId((Long)request.getSession().getAttribute("ID"));
            DatosRegistroClinicoDTO datosRegistro = new DatosRegistroClinicoDTO();
            datosRegistro.setIdMedico(medico.getId());
            datosRegistro.setIdPaciente(idPaciente);
            modelo.put("registroDTO",datosRegistro);
            return new ModelAndView("Medico/crear-registro", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "cargar-registro", method = RequestMethod.POST)
    public ModelAndView cargarNuevoRegistro(@ModelAttribute("registroDTO") DatosRegistroClinicoDTO registroDTO, HttpServletRequest request){
        try{
            this.servicioLogin.esMedico(request);
            this.servicioRegistro.crearRegistro(registroDTO);
            return new ModelAndView(("redirect:/historial-clinico?idPaciente="+registroDTO.getIdPaciente()));
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
