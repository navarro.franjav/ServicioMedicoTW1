package ar.edu.unlam.tallerweb1.delivery;



public class DatosRegistroMedicoDTO {
    private String nombre;
    private String apellido;
    private String email;
    private String password;
    private String telefono;
    private Long dni;
    private Long idMedico;

    public DatosRegistroMedicoDTO(){

    }
    public DatosRegistroMedicoDTO(String nombre, String apellido, String email, String password, String telefono, Long dni, Long idMedico) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.password = password;
        this.telefono = telefono;
        this.dni = dni;
        this.idMedico = idMedico;
    }

    public Long getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Long idMedico) {
        this.idMedico = idMedico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

}
