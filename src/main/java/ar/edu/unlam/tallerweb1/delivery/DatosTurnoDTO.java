package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;

import javax.persistence.*;
import java.util.Date;

public class DatosTurnoDTO {

    private Long id;

    private Long paciente_id;

    private Long medico_hospital_id;

    private Date fechaHora;
    private String motivoConsulta;

    public Long getPaciente_id() {
        return paciente_id;
    }

    public void setPaciente_id(Long paciente_id) {
        this.paciente_id = paciente_id;
    }

    public Long getMedico_hospital_id() {
        return medico_hospital_id;
    }

    public void setMedico_hospital_id(Long medico_hospital_id) {
        this.medico_hospital_id = medico_hospital_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }
}
