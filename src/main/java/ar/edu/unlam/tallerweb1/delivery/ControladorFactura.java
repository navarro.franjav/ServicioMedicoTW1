package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Factura;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioFactura;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioLogin;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPaciente;
import com.mercadopago.MercadoPagoConfig;
import com.mercadopago.client.preference.PreferenceBackUrlsRequest;
import com.mercadopago.client.preference.PreferenceClient;
import com.mercadopago.client.preference.PreferenceItemRequest;
import com.mercadopago.client.preference.PreferenceRequest;
import com.mercadopago.exceptions.MPApiException;
import com.mercadopago.exceptions.MPException;
import com.mercadopago.resources.preference.Preference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller

public class ControladorFactura {
    private ServicioFactura servicioFactura;
    private ServicioPaciente servicioPaciente;
    private ServicioLogin servicioLogin;
    @Autowired
    public ControladorFactura(ServicioFactura servicioFactura, ServicioPaciente servicioPaciente, ServicioLogin servicioLogin){
        this.servicioFactura =servicioFactura;
        this.servicioPaciente =servicioPaciente;
        this.servicioLogin = servicioLogin;
    }

    @RequestMapping(path="/generar-facturas")
    public ResponseEntity<String> generarFacturasMensuales(HttpServletRequest request) {
        if (request.getSession().getAttribute("ROL").equals(1)){
            servicioFactura.generarFacturasMensuales();
            return ResponseEntity.ok("Facturas generadas exitosamente.");
        }else {
            return ResponseEntity.ok("Acción no autorizada");
        }
    }

    @RequestMapping(path="/lista-facturas")
    public ModelAndView irAListaFacturas(HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            List<Factura> facturas = this.servicioFactura.verTodas();
            ModelMap modelo = new ModelMap();
            modelo.put("facturas",facturas);
            return new ModelAndView("Admin/lista-facturas",modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    /*@RequestMapping(path = "/pagar-factura", method = RequestMethod.GET)
    public ModelAndView pagarFactura(@RequestParam("id") Long id, HttpServletRequest request) {
        this.servicioFactura.pagar(id);
            return new ModelAndView("redirect:/mis-facturas");
    }*/

    @RequestMapping(path = "/pagar-factura", method = RequestMethod.GET)
    public ModelAndView pagarConMp(@RequestParam("id") Long id, HttpServletRequest request){
        Factura factura= this.servicioFactura.getFacturaById(id);
        MercadoPagoConfig.setAccessToken("TEST-3984946861104361-071118-613442c7247eae2e980ac3c70b5da7c0-127065848");
        PreferenceClient client = new PreferenceClient();
        // Crea un �tem en la preferencia
        PreferenceItemRequest item = PreferenceItemRequest.builder().title(factura.getCodigo()).quantity(1)
                .unitPrice(new BigDecimal(factura.getValor())).build();
        List<PreferenceItemRequest> items = new ArrayList<>();
        items.add(item);
        PreferenceBackUrlsRequest backUrls = PreferenceBackUrlsRequest.builder()
                .success("http://localhost:8080/proyecto_limpio_spring_war/mis-facturas")
                .failure("http://localhost:8080/proyecto_limpio_spring_war/login").build();
        PreferenceRequest prefRequest = PreferenceRequest.builder().autoReturn("approved").binaryMode(true).items(items)
                .backUrls(backUrls).build();
        Preference response = null;
        try {
            response = client.create(prefRequest);
        } catch (MPException | MPApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.servicioFactura.pagar(id);
        return new ModelAndView("redirect:" + response.getInitPoint());
    }

}
