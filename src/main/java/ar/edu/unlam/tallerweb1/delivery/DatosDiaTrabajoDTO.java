package ar.edu.unlam.tallerweb1.delivery;

import java.time.LocalTime;

public class DatosDiaTrabajoDTO {
    private String nombreDia;

    private LocalTime horaInicio;

    private LocalTime horaFin;

    private Long medicoHospitalId;

    public String getNombreDia() {
        return nombreDia;
    }

    public void setNombreDia(String nombreDia) {
        this.nombreDia = nombreDia;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public LocalTime getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(LocalTime horaFin) {
        this.horaFin = horaFin;
    }

    public Long getMedicoHospitalId() {
        return medicoHospitalId;
    }

    public void setMedicoHospitalId(Long medicoHospitalId) {
        this.medicoHospitalId = medicoHospitalId;
    }


}
