package ar.edu.unlam.tallerweb1.delivery;

public class DatosEstudioColesterolDTO extends DatosEstudioDTO {
    private double colesterolLDL;
    private double colesterolTotal;
    private double trigliceridos;
    private double colesterolHDL;
    private double colesterolNoHDL;

    public DatosEstudioColesterolDTO(){

    }

    public DatosEstudioColesterolDTO(DatosSeleccionarEstudioDTO datosSeleccionados){
        this.setIdMedico(datosSeleccionados.getIdMedico());
        this.setIdPaciente(datosSeleccionados.getIdPaciente());
        this.setIdTipoEstudio(datosSeleccionados.getIdEstudio());
    }

    public double getColesterolLDL() {
        return colesterolLDL;
    }

    public void setColesterolLDL(double colesterolLDL) {
        this.colesterolLDL = colesterolLDL;
    }

    public double getColesterolTotal() {
        return colesterolTotal;
    }

    public void setColesterolTotal(double colesterolTotal) {
        this.colesterolTotal = colesterolTotal;
    }

    public double getTrigliceridos() {
        return trigliceridos;
    }

    public void setTrigliceridos(double trigliceridos) {
        this.trigliceridos = trigliceridos;
    }

    public double getColesterolHDL() {
        return colesterolHDL;
    }

    public void setColesterolHDL(double colesterolHDL) {
        this.colesterolHDL = colesterolHDL;
    }

    public double getColesterolNoHDL() {
        return colesterolNoHDL;
    }

    public void setColesterolNoHDL(double colesterolNoHDL) {
        this.colesterolNoHDL = colesterolNoHDL;
    }
}
