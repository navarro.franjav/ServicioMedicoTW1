package ar.edu.unlam.tallerweb1.delivery;

public class DatosAsignarTurnoPacienteRecepcionDTO {
    private Long IdTurno;
    private String motivo;
    private Long DNIPaciente;

    public DatosAsignarTurnoPacienteRecepcionDTO(Long idTurno) {
        IdTurno = idTurno;
    }
    public DatosAsignarTurnoPacienteRecepcionDTO() {}



    public Long getIdTurno() {
        return IdTurno;
    }

    public void setIdTurno(Long idTurno) {
        IdTurno = idTurno;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Long getDNIPaciente() {
        return DNIPaciente;
    }

    public void setDNIPaciente(Long DNIPaciente) {
        this.DNIPaciente = DNIPaciente;
    }
}
