package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.ClienteNoEncontradoException;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioLogin;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPaciente;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlan;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioTurno;
import ar.edu.unlam.tallerweb1.domain.clases.*;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@Controller
public class ControladorPaciente {

    private ServicioPaciente servicioPaciente;
    private ServicioPlan servicioPlan;
    private ServicioTurno servicioTurno;
    private ControladorLogin controladorLogin;
    private ServicioLogin servicioLogin;
    private ServicioFactura servicioFactura;
    private ServicioPlanEspecialidad servicioPlanEspecialidad;
    private ServicioEstudio servicioEstudio;
    private ServicioRegistro servicioRegistro;
    @Autowired
    public ControladorPaciente(ServicioPaciente servicioPaciente, ServicioPlan servicioPlan, ServicioTurno servicioTurno, ControladorLogin controladorLogin, ServicioFactura servicioFactura , ServicioLogin servicioLogin, ServicioPlanEspecialidad servicioPlanEspecialidad, ServicioEstudio servicioEstudio, ServicioRegistro servicioRegistro){
        this.servicioPlanEspecialidad=servicioPlanEspecialidad;
        this.servicioPaciente = servicioPaciente;
        this.servicioPlan = servicioPlan;
        this.servicioTurno = servicioTurno;
        this.controladorLogin = controladorLogin;
        this.servicioLogin=servicioLogin;
        this.servicioFactura = servicioFactura;
        this.servicioEstudio = servicioEstudio;
        this.servicioRegistro = servicioRegistro;
    }
    
    @RequestMapping(path = "/crear-paciente")
    public ModelAndView irACrearPaciente(HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            model.put("datosPaciente", new DatosRegistroPacienteDTO());
            List<Plan> listaPlanes=servicioPlan.listarPlanes();
            model.put("planes", listaPlanes);
            return new ModelAndView("Admin/crear-paciente", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/crear-paciente", method = RequestMethod.POST)
    public ModelAndView crearPaciente(@ModelAttribute("DatosRegistroPacienteDTO") DatosRegistroPacienteDTO datosPaciente, HttpServletRequest request) throws ParseException {
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioPaciente.guardarPaciente(datosPaciente);
            return new ModelAndView("redirect:/lista-pacientes");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/lista-pacientes", method = RequestMethod.GET)
    public ModelAndView listarClientes(HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            List<Paciente> lista = this.servicioPaciente.listarPacientes();
            model.put("pacientes",lista);
            return new ModelAndView("Admin/lista-pacientes", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }
    @RequestMapping(path = "/eliminar-paciente", method = RequestMethod.GET)
    public ModelAndView eliminarPaciente(@RequestParam("id") Long id, HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            try {
                this.servicioPaciente.eliminar(id);
                model.put("mensaje", "Paciente eliminado");
            } catch (ClienteNoEncontradoException ex) {
                model.put("mensaje", "Error al eliminar el pliente: " + ex.getMessage());
            }
            List<Paciente> lista = this.servicioPaciente.listarPacientes();
            model.put("pacientes", lista);
            return new ModelAndView("Admin/lista-pacientes", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/editar-paciente", method = RequestMethod.GET)
    public ModelAndView irAEditarPaciente(@RequestParam ("id") Long id, HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            DatosRegistroPacienteDTO datosPaciente = this.servicioPaciente.cargarDatosPaciente(id);
            model.put("datosPaciente", datosPaciente);
            List<Plan> listaPlanes=servicioPlan.listarPlanes();
            model.put("planes", listaPlanes);
            return new ModelAndView("/Admin/editar-paciente", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/editar-paciente", method = RequestMethod.POST)
    public ModelAndView editarPaciente(@ModelAttribute ("DatosRegistroPacienteDTO") DatosRegistroPacienteDTO datosPaciente, HttpServletRequest request) throws ParseException {
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioPaciente.guardarPaciente(datosPaciente);
            return new ModelAndView("redirect:/lista-pacientes");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/mis-turnos", method = RequestMethod.GET)
    public ModelAndView irATurnos(HttpServletRequest request) {
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap model = new ModelMap();
            try{
                Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long)request.getSession().getAttribute("ID"));
                List<Turno> turnos = this.servicioTurno.turnosPaciente(paciente);
                model.put("turnos",turnos);
                model.put("paciente",paciente);
                return new ModelAndView("paciente/mis-turnos",model);
            }catch(RuntimeException ex){
                return new ModelAndView("redirect:/login");
            }
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }
    @RequestMapping(path = "/mi-plan", method = RequestMethod.GET)
    public ModelAndView irAPlan(HttpServletRequest request) {
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap model = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            model.put("paciente",paciente);
            List<Plan> listaPlanes=servicioPlan.listarPlanes();
            model.put("planes", listaPlanes);
            DatosPacientePlanDTO datosPacientePlan = new DatosPacientePlanDTO();
            model.put("datosPacientePlan",datosPacientePlan);
            List<Plan_Especialidad> listaPlanEspecialidad=this.servicioPlanEspecialidad.listarPlanEspecialidades();
            model.put("listaEspecialidades", listaPlanEspecialidad);
            return new ModelAndView("paciente/mi-plan",model);
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/cambiar-plan", method = RequestMethod.POST)
    public ModelAndView cambiarPlan(@ModelAttribute ("DatosPacientePlanDTO") DatosPacientePlanDTO datosPacientePlan, HttpServletRequest request) throws ParseException {
        try{
            this.servicioLogin.esPaciente(request);
            this.servicioPaciente.cambiarPlan(datosPacientePlan);
            return new ModelAndView("redirect:/mi-plan");
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }

    }

    @RequestMapping(path = "/mis-estudios", method = RequestMethod.GET)
    public ModelAndView irAEstudios(HttpServletRequest request) {
        try {
            this.servicioLogin.esPaciente(request);
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            ModelMap modelo = new ModelMap();
            modelo.put("estudios", this.servicioEstudio.obtenerEstudiosPorElIdPaciente(paciente.getId()));
            return new ModelAndView("paciente/mis-estudios", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }
    @RequestMapping(path = "/mi-perfil")
    public ModelAndView irAPerfil(HttpServletRequest request) {
        try{
            this.servicioLogin.esPaciente(request);
            ModelMap model = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            DatosRegistroPacienteDTO datosPaciente = this.servicioPaciente.cargarDatosPaciente(paciente.getId());
            model.put("datosPaciente", datosPaciente);
            return new ModelAndView("paciente/mi-perfil",model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }


    @RequestMapping(path = "/editar-perfil", method = RequestMethod.POST)
    public ModelAndView editarPerfil(@ModelAttribute ("DatosRegistroPacienteDTO") DatosRegistroPacienteDTO datosPaciente, HttpServletRequest request) throws ParseException {
        try{
           this.servicioLogin.esPaciente(request);
            this.servicioPaciente.guardarPaciente(datosPaciente);
            return new ModelAndView("redirect:/home-paciente");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/registro")
    public ModelAndView irRegistro(HttpServletRequest request) {
        ModelMap modelo = new ModelMap();
        DatosRegistroPacienteDTO datosPaciente = new DatosRegistroPacienteDTO();
        modelo.put("datosPaciente", datosPaciente);
        List<Plan> listaPlanes=servicioPlan.listarPlanes();
        modelo.put("planes", listaPlanes);
        return new ModelAndView("paciente/registro",modelo);
    }

    @RequestMapping(path = "/registrar-paciente", method = RequestMethod.POST)
    public ModelAndView registrarPaciente(@ModelAttribute("DatosRegistroPacienteDTO") DatosRegistroPacienteDTO datosPaciente, HttpServletRequest request) throws ParseException {
        this.servicioPaciente.guardarPaciente(datosPaciente);
        Paciente paciente = this.servicioPaciente.buscarPorEmail(datosPaciente.getEmail());
        this.servicioFactura.generarFacturaIndividual(paciente.getId());
        DatosLoginDTO datosLogin = new DatosLoginDTO();
        datosLogin.setEmail(datosPaciente.getEmail());
        datosLogin.setPassword(datosPaciente.getPassword());
        ModelAndView modelAndView = this.controladorLogin.validarLogin(datosLogin, request);
        return modelAndView;
    }

    @RequestMapping(path = "/mis-facturas")
    public ModelAndView irFacturas(HttpServletRequest request) {
        if (request.getSession().getAttribute("ROL") != null && request.getSession().getAttribute("ROL").equals(3)){
            ModelMap modelo = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            List<Factura> facturas = this.servicioFactura.getFacturas(paciente);
            modelo.put("paciente", paciente);
            modelo.put("facturas",facturas);
            return new ModelAndView("paciente/facturas",modelo);
        }else {
            return new ModelAndView("redirect:/login");
        }
    }

    @RequestMapping(path = "/generar-factura-individual", method = RequestMethod.GET)
    public ModelAndView generarFactura(@RequestParam("id") Long id, HttpServletRequest request) {
        if (request.getSession().getAttribute("ROL").equals(1)) {
            ModelMap model = new ModelMap();
            this.servicioFactura.generarFacturaIndividual(id);
            model.put("mensaje", "Factura generada");
            List<Paciente> lista = this.servicioPaciente.listarPacientes();
            model.put("pacientes", lista);
            return new ModelAndView("Admin/lista-pacientes", model);
        } else {
            return new ModelAndView("redirect:/login");
        }

    }

    @RequestMapping(path = "paciente-ver-estudio-Hemograma")
    public ModelAndView irAPacienteVerEstudioHemograma(@RequestParam("idEstudio") Long idEstudio, HttpServletRequest request){
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap modelo = new ModelMap();
            Estudio estudio = servicioEstudio.obtenerEstudioPorELId(idEstudio);
            modelo.put("estudio", servicioEstudio.obtenerHemogramaPorElIdEstudio(idEstudio));
            return new ModelAndView("/paciente/paciente-ver-estudio", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "paciente-ver-estudio-Colesterol")
    public ModelAndView irAPacienteVerEstudioColesterol(@RequestParam("idEstudio") Long idEstudio, HttpServletRequest request){
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap modelo = new ModelMap();
            Estudio estudio = servicioEstudio.obtenerEstudioPorELId(idEstudio);
            modelo.put("estudio", servicioEstudio.obtenerEstudioColesterolPorELIdEstudio(idEstudio));
            return new ModelAndView("/paciente/paciente-ver-estudio", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "paciente-ver-estudio-Hepatograma")
    public ModelAndView irAPacienteVerEstudioHepatograma(@RequestParam("idEstudio") Long idEstudio, HttpServletRequest request){
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap modelo = new ModelMap();
            Estudio estudio = servicioEstudio.obtenerEstudioPorELId(idEstudio);
            modelo.put("estudio", servicioEstudio.obtenerHepatogramaPorElIdEstudio(idEstudio));
            return new ModelAndView("/paciente/paciente-ver-estudio", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "mi-historial")
    public ModelAndView irHistorialPaciente( HttpServletRequest request ){
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap modelo = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            List<RegistroClinico> historial = this.servicioPaciente.historialClinico(paciente);
            modelo.put("historial",historial);
            return new ModelAndView("paciente/historial-clinico", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "mi-registro-clinico")
    public ModelAndView irARegistroClinico(@RequestParam("codigoRegistro") String codigoRegistro, HttpServletRequest request){
        try {
            this.servicioLogin.esPaciente(request);
            ModelMap modelo = new ModelMap();
            RegistroClinico registro = this.servicioRegistro.buscarPorCodigo(codigoRegistro);
            modelo.put("registro", registro);
            return new ModelAndView("paciente/registro-clinico", modelo);
        }catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }


}

