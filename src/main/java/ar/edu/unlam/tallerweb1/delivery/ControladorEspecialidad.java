package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioEspecialidad;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ControladorEspecialidad {
    private ServicioEspecialidad servicioEspecialidad;
    private ServicioLogin servicioLogin;
    @Autowired
    public ControladorEspecialidad(ServicioEspecialidad servicioEspecialidad, ServicioLogin servicioLogin){
        this.servicioEspecialidad=servicioEspecialidad;
        this.servicioLogin=servicioLogin;
    }

    @RequestMapping(path = "/lista-especialidades")
    public ModelAndView listarEspecialidades(HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            List<Especialidad> listaEspecialidades=this.servicioEspecialidad.listarEspecialidades();
            ModelMap model = new ModelMap();
            model.put("especialidades", listaEspecialidades);
            return new ModelAndView("/Admin/lista-especialidades",model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/crear-especialidad")
    public ModelAndView irACrearEspecialidad(HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            model.put("titulo", "Crear");
            model.put("datosEspecialidad", new DatosEspecialidadDTO());
            model.put("url", "crear-especialidad");
            return new ModelAndView("/Admin/crear-especialidad", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/crear-especialidad", method = RequestMethod.POST)
    public ModelAndView crearEspecialidad(@ModelAttribute("datosEspecialidad") DatosEspecialidadDTO datosNuevaEspecialidad, HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioEspecialidad.guardarEspecialidad(datosNuevaEspecialidad);
            return new ModelAndView("redirect:/lista-especialidades");
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/eliminar-especialidad")
    public ModelAndView eliminarEspecialidad(@RequestParam ("id") Long id, HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioEspecialidad.eliminarEspecialidad(id);
            return new ModelAndView("redirect:/lista-especialidades");
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "/modificar-especialidad")
    public ModelAndView irAEditarEspecialidad(@RequestParam("id") Long id, HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            Especialidad especialidad=this.servicioEspecialidad.getEspecialidadById(id);
            ModelMap model= new ModelMap();
            model.put("datosEspecialidad", especialidad);
            model.put("titulo", "Editar");
            model.put("url", "modificar-especialidad");
            return new ModelAndView("/Admin/crear-especialidad", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "modificar-especialidad", method = RequestMethod.POST)
    public ModelAndView editarEspecialidad(@ModelAttribute ("datosEspecialidad") DatosEspecialidadDTO datosEspecialidad, HttpServletRequest request ){
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioEspecialidad.actualizarEspecialidad(datosEspecialidad);
            return new ModelAndView("redirect:/lista-especialidades");
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }
}
