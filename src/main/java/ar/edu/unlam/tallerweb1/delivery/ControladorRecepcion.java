package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.configuracion.LocalDateSerializer;
import ar.edu.unlam.tallerweb1.domain.configuracion.LocalTimeSerializer;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionRecepcionException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
@Controller
public class ControladorRecepcion {

    private ServicioTurno servicioTurno;
    private ServicioEspecialidad servicioEspecialidad;
    private ServicioMedico servicioMedico;
    private ServicioLogin servicioLogin;
    private ServicioMedicoHospital servicioMedicoHospital;
    private ServicioPaciente servicioPaciente;

    @Autowired
    public ControladorRecepcion(ServicioTurno servicioTurno, ServicioMedico servicioMedico,ServicioLogin servicioLogin
            ,ServicioMedicoHospital servicioMedicoHospital, ServicioEspecialidad servicioEspecialidad, ServicioPaciente servicioPaciente) {
        this.servicioTurno = servicioTurno;
        this.servicioMedico = servicioMedico;
        this.servicioLogin=servicioLogin;
        this.servicioMedicoHospital = servicioMedicoHospital;
        this.servicioEspecialidad = servicioEspecialidad;
        this.servicioPaciente = servicioPaciente;

    }

    @RequestMapping(path = "/lista-turnosRecepcion")
    public ModelAndView listarTurnos(HttpServletRequest request) {
        try{
            this.servicioLogin.esRecepcionista(request);
            List<Turno> listaDeTurnos = servicioTurno.listarTurnos();
            ModelMap model = new ModelMap();
            model.put("turnos", listaDeTurnos);
            return new ModelAndView("Admin/lista-turnosRecepcion", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }

    }
    @RequestMapping(path = "asignar-paciente")
    public ModelAndView asignarPaciente(@RequestParam ("id") Long id, HttpServletRequest request){
        try{
            this.servicioLogin.esRecepcionista(request);
            ModelMap modelo = new ModelMap();
            DatosAsignarTurnoPacienteRecepcionDTO datosTurno = new DatosAsignarTurnoPacienteRecepcionDTO(id);
            modelo.put("datosTurno", datosTurno);
            modelo.put("titulo", "Editar");
            modelo.put("url", "asignar-paciente");
            return new ModelAndView("/Admin/AsignarPacienteAlTurnoRecepcion", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView(("redirect:/home"));
        }
    }

    @RequestMapping(path = "asignar-paciente", method = RequestMethod.POST)
    public ModelAndView asignarPaciente(@ModelAttribute ("DatosAsignarTurnoPacienteRecepcionDTO") DatosAsignarTurnoPacienteRecepcionDTO datosTurno, HttpServletRequest request ) throws ValidacionMedicoException {

                this.servicioTurno.actualizarTurno(datosTurno);
                return new ModelAndView("redirect:/lista-turnosRecepcion");


    }




}
