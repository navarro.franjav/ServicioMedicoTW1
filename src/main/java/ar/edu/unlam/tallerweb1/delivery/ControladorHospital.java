package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Hospital;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.excepciones.ValidacionMedicoException;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioHospital;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioLogin;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

@Controller
public class ControladorHospital {
    private ServicioHospital servicioHopital;
    private ServicioLogin servicioLogin;
@Autowired
 public ControladorHospital(ServicioHospital servicioHopital, ServicioLogin servicioLogin){
    this.servicioHopital=servicioHopital;
    this.servicioLogin=servicioLogin;
}
    @RequestMapping(path = "/lista-hospitales", method = RequestMethod.GET)
    public ModelAndView listarHospitales(HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            List<Hospital> listaHospitales = this.servicioHopital.listarHospitales();
            model.put("hospitales",listaHospitales);
            return new ModelAndView("Admin/lista-hospitales", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/crear-hospital", method = RequestMethod.GET)
    public ModelAndView irACrearHospital(HttpServletRequest request) {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model = new ModelMap();
            model.put("titulo", "Crear");
            model.put("datosHospital", new DatosHospitalDTO());
            model.put("url", "crear-hospital");
            return new ModelAndView("Admin/crear-hospital", model);
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/crear-hospital", method = RequestMethod.POST)
    public ModelAndView crearHospital(@ModelAttribute("datosHospital")DatosHospitalDTO datosNuevoHospital, HttpServletRequest request) throws ParseException {
        try {
            this.servicioLogin.esAdmin(request);
            ModelMap model=new ModelMap();
            try {
                this.servicioHopital.guardarHospital(datosNuevoHospital);
                model.put("mensaje", "Hospital guardado correctamente");
                return new ModelAndView("redirect:/lista-hospitales", model);
            } catch (HibernateException ex) {
                model.put("mensaje", "Error al guardar el hospital");
                return new ModelAndView("redirect:/lista-hospitales", model);
            }
        } catch (AccesoDenegadoException e) {
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "/eliminar-hospital")
    public ModelAndView eliminarHospital(@RequestParam("id") Long id, HttpServletRequest request){
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioHopital.eliminarHospital(id);
            ModelMap model=new ModelMap();
            //model.put("mensaje", "hospital eliminado");
            return new ModelAndView("redirect:/lista-hospitales", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
            }
    }

    @RequestMapping(path = "/modificar-hospital")
    public ModelAndView modificarHospital(@RequestParam("id") Long id, HttpServletRequest request){
        try {
            this.servicioLogin.esAdmin(request);
            Hospital hospitalBuscado= this.servicioHopital.getHospitalPorId(id);
            ModelMap model= new ModelMap();
            model.put("titulo", "Editar");
            //model.put("mensaje", "Hospital editado correctamente");
            model.put("datosHospital", hospitalBuscado);
            model.put("url", "modificar-hospital");
            return new ModelAndView("Admin/crear-hospital",model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "modificar-hospital", method = RequestMethod.POST)
    public ModelAndView modificarMedico(@ModelAttribute ("datosHospital") DatosHospitalDTO datosHospital, HttpServletRequest request ) throws ValidacionMedicoException {
        try{
            this.servicioLogin.esAdmin(request);
            this.servicioHopital.actualizarHospital(datosHospital);
            return new ModelAndView("redirect:/lista-hospitales");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }
}


