package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Paciente;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.configuracion.LocalDateSerializer;
import ar.edu.unlam.tallerweb1.domain.configuracion.LocalTimeSerializer;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
@Controller
public class ControladorTurnos {
    private ServicioTurno servicioTurno;
    private ServicioEspecialidad servicioEspecialidad;
    private ServicioMedico servicioMedico;
    private ServicioMedicoHospital servicioMedicoHospital;
    private ServicioPaciente servicioPaciente;
    private ServicioLogin servicioLogin;

    @Autowired
    public ControladorTurnos(ServicioTurno servicioTurno, ServicioMedico servicioMedico
                             ,ServicioMedicoHospital servicioMedicoHospital, ServicioEspecialidad servicioEspecialidad,
                             ServicioPaciente servicioPaciente, ServicioLogin servicioLogin) {
        this.servicioTurno = servicioTurno;
        this.servicioMedico = servicioMedico;
        this.servicioMedicoHospital = servicioMedicoHospital;
        this.servicioEspecialidad = servicioEspecialidad;
        this.servicioPaciente = servicioPaciente;
        this.servicioLogin=servicioLogin;
    }

    @RequestMapping(path = "generar-turnos")
    public ModelAndView generarTurnos(@RequestParam("idMedicoHospital") Long idMedicoHospital, HttpServletRequest request) {
        try{
            this.servicioLogin.esAdmin(request);
            servicioTurno.generarTurnos(idMedicoHospital);
            ModelMap modelo = new ModelMap();
            modelo.put("turnosMedicoHospital",servicioMedicoHospital.obtenerTurnosMedicoHospital(idMedicoHospital));
            modelo.put("medicoHospital", servicioMedicoHospital.obtenerPorId(idMedicoHospital));
            modelo.put("mensajeDeExito", "Se han generado los turnos correctamente");
            return new ModelAndView("Admin/lista-TurnosGenerados", modelo);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    private List<DatosMedicoHospitalDiaDTO> obtenerDatosMedicoHospitalDias(Long idMedico) {
        return servicioMedicoHospital.obtenerDatosMedicoHospitalDias(idMedico);
    }

    @RequestMapping(path = "asignar-turno")
    public ModelAndView irAsignarTurno(HttpServletRequest request) {
        try{
            this.servicioLogin.esPaciente(request);
            ModelMap model = new ModelMap();
            List<Turno> turnos = this.servicioTurno.turnosDisponibles();
            List<Especialidad> especialidades = this.servicioEspecialidad.listarEspecialidades();
            ObjectMapper objectMapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addSerializer(LocalDate.class, new LocalDateSerializer());
            module.addSerializer(LocalTime.class, new LocalTimeSerializer());
            objectMapper.registerModule(module);
            String jsonTurnos;
            try {
                jsonTurnos = objectMapper.writeValueAsString(turnos);
            } catch (JsonProcessingException e) {
                jsonTurnos = "[]"; // En caso de error, asigna un JSON vacío
            }
            model.addAttribute("turnos", jsonTurnos);
            model.addAttribute("especialidades", especialidades);
            model.addAttribute("datosAsignarTurno", new DatosAsignarTurnoDTO());
            return new ModelAndView("paciente/asignar-turno", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

    @RequestMapping(path = "asignar-turno", method = RequestMethod.POST)
    public ModelAndView asignarTurno(@ModelAttribute("datosAsignarTurno") DatosAsignarTurnoDTO datosAsignarTurno,HttpServletRequest request) throws AccesoDenegadoException {
        try{
            servicioLogin.esPaciente(request);
            ModelMap model = new ModelMap();
            Paciente paciente = this.servicioPaciente.buscarPorUsuarioId((Long) request.getSession().getAttribute("ID"));
            datosAsignarTurno.setPaciente(paciente);
            this.servicioTurno.asignar(datosAsignarTurno);
            model.addAttribute("datosAsignarTurno", new DatosAsignarTurnoDTO());
            return new ModelAndView("redirect:/mis-turnos", model);
        }catch(AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }


    @RequestMapping(path = "cancelar-turno", method = RequestMethod.GET)
    public ModelAndView cancelarTurno(@RequestParam("id") Long id, @RequestParam("uId") Long uId, HttpServletRequest request) throws AccesoDenegadoException {
        try{
            this.servicioLogin.esPaciente(request);
            this.servicioTurno.cancelarTurno(id,uId);
            return new ModelAndView("redirect:/mis-turnos");
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }



}
