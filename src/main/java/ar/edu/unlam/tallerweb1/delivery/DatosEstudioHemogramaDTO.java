package ar.edu.unlam.tallerweb1.delivery;

public class DatosEstudioHemogramaDTO extends DatosEstudioDTO {
    private double hemoglobinaSangreTotal;
    private double hematocrito;
    private double basofilos;
    private double eosinofilos;
    private double linfocitos;
    private double monocitos;

    public DatosEstudioHemogramaDTO(){

    }
    public DatosEstudioHemogramaDTO(DatosSeleccionarEstudioDTO datosSeleccionados){
        this.setIdMedico(datosSeleccionados.getIdMedico());
        this.setIdPaciente(datosSeleccionados.getIdPaciente());
        this.setIdTipoEstudio(datosSeleccionados.getIdEstudio());
    }

    public double getHemoglobinaSangreTotal() {
        return hemoglobinaSangreTotal;
    }

    public void setHemoglobinaSangreTotal(double hemoglobinaSangreTotal) {
        this.hemoglobinaSangreTotal = hemoglobinaSangreTotal;
    }

    public double getHematocrito() {
        return hematocrito;
    }

    public void setHematocrito(double hematocrito) {
        this.hematocrito = hematocrito;
    }

    public double getBasofilos() {
        return basofilos;
    }

    public void setBasofilos(double basofilos) {
        this.basofilos = basofilos;
    }

    public double getEosinofilos() {
        return eosinofilos;
    }

    public void setEosinofilos(double eosinofilos) {
        this.eosinofilos = eosinofilos;
    }

    public double getLinfocitos() {
        return linfocitos;
    }

    public void setLinfocitos(double linfocitos) {
        this.linfocitos = linfocitos;
    }

    public double getMonocitos() {
        return monocitos;
    }

    public void setMonocitos(double monocitos) {
        this.monocitos = monocitos;
    }
}
