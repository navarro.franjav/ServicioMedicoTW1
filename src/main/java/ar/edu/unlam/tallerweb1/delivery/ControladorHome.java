package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;
import ar.edu.unlam.tallerweb1.domain.clases.Plan;
import ar.edu.unlam.tallerweb1.domain.clases.Plan_Especialidad;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlan;
import ar.edu.unlam.tallerweb1.domain.servicios.ServicioPlanEspecialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;
@Controller
public class ControladorHome {

    private ServicioPlan servicioPlan;
    private ServicioPlanEspecialidad servicioPlanEspecialidad;

    @Autowired
    public ControladorHome(ServicioPlan servicioPlan, ServicioPlanEspecialidad servicioPlanEspecialidad){
        this.servicioPlan = servicioPlan;
        this.servicioPlanEspecialidad=servicioPlanEspecialidad;
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView inicio() {

        List<Plan> listaDePlanes = this.servicioPlan.listarPlanes();
        List<Plan_Especialidad> listaPlanEspecialidad=this.servicioPlanEspecialidad.listarPlanEspecialidades();
        ModelMap modelo = new ModelMap();
        modelo.put("listaDePlanes" , listaDePlanes);
        modelo.put("listaDeEspecialidades", listaPlanEspecialidad);
        return new ModelAndView("Common/home", modelo);
    }


}
