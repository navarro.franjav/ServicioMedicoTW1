package ar.edu.unlam.tallerweb1.delivery;

public class DatosMedicoHospitalDTO {
    private Long id_medico;
    private Long id_hospital;
    private Long id_especialidad;

    public Long getId_medico() {
        return id_medico;
    }

    public void setId_medico(Long id_medico) {
        this.id_medico = id_medico;
    }

    public Long getId_hospital() {
        return id_hospital;
    }

    public void setId_hospital(Long id_hospital) {
        this.id_hospital = id_hospital;
    }

    public Long getId_especialidad() {
        return id_especialidad;
    }

    public void setId_especialidad(Long id_especialidad) {
        this.id_especialidad = id_especialidad;
    }
}
