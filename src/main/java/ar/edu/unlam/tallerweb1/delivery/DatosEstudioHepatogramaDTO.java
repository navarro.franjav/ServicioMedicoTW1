package ar.edu.unlam.tallerweb1.delivery;

public class DatosEstudioHepatogramaDTO extends DatosEstudioDTO{
    private double bilirrubinaTotal;
    private double bilirrubinaDirecta;
    private double fosfatasasAlcalinas;
    private double colesterolTotal;
    private double proteinasTotales;
    private double albumina;

    public DatosEstudioHepatogramaDTO(){

    }
    public DatosEstudioHepatogramaDTO(DatosSeleccionarEstudioDTO datosSeleccionados){
        this.setIdMedico(datosSeleccionados.getIdMedico());
        this.setIdPaciente(datosSeleccionados.getIdPaciente());
        this.setIdTipoEstudio(datosSeleccionados.getIdEstudio());
    }

    public double getBilirrubinaTotal() {
        return bilirrubinaTotal;
    }

    public void setBilirrubinaTotal(double bilirrubinaTotal) {
        this.bilirrubinaTotal = bilirrubinaTotal;
    }

    public double getBilirrubinaDirecta() {
        return bilirrubinaDirecta;
    }

    public void setBilirrubinaDirecta(double bilirrubinaDirecta) {
        this.bilirrubinaDirecta = bilirrubinaDirecta;
    }

    public double getFosfatasasAlcalinas() {
        return fosfatasasAlcalinas;
    }

    public void setFosfatasasAlcalinas(double fosfatasasAlcalinas) {
        this.fosfatasasAlcalinas = fosfatasasAlcalinas;
    }

    public double getColesterolTotal() {
        return colesterolTotal;
    }

    public void setColesterolTotal(double colesterolTotal) {
        this.colesterolTotal = colesterolTotal;
    }

    public double getProteinasTotales() {
        return proteinasTotales;
    }

    public void setProteinasTotales(double proteinasTotales) {
        this.proteinasTotales = proteinasTotales;
    }

    public double getAlbumina() {
        return albumina;
    }

    public void setAlbumina(double albumina) {
        this.albumina = albumina;
    }
}
