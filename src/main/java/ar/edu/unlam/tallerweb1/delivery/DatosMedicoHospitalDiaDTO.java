package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.DiaTrabajo;
import ar.edu.unlam.tallerweb1.domain.clases.Medico_Hospital;

import java.util.List;

public class DatosMedicoHospitalDiaDTO {

    Medico_Hospital medicoHospital;
    List<DiaTrabajo> listaDeDias;

    public Medico_Hospital getMedicoHospital() {
        return medicoHospital;
    }

    public void setMedicoHospital(Medico_Hospital medicoHospital) {
        this.medicoHospital = medicoHospital;
    }

    public List<DiaTrabajo> getListaDeDias() {
        return listaDeDias;
    }

    public void setListaDeDias(List<DiaTrabajo> listaDeDias) {
        this.listaDeDias = listaDeDias;
    }
}
