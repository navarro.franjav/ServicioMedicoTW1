package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Especialidad;

import java.util.List;

public class DatosPlanDTO {
    private Long id;
    private String nombre;
    private Double precio;
    private String descripcion;
    private List<Long> listaEspecialidadesSeleccionadas;
    public List<Long> getListaEspecialidadesSeleccionadas() {
        return listaEspecialidadesSeleccionadas;
    }

    public void setListaEspecialidadesSeleccionadas(List<Long> listaEspecialidadesSeleccionadas) {
        this.listaEspecialidadesSeleccionadas = listaEspecialidadesSeleccionadas;
    }

    public String getNombre() {
        return nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
