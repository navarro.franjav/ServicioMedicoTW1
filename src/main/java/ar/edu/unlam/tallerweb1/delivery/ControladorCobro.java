package ar.edu.unlam.tallerweb1.delivery;

import ar.edu.unlam.tallerweb1.domain.clases.Cobro;
import ar.edu.unlam.tallerweb1.domain.clases.Turno;
import ar.edu.unlam.tallerweb1.domain.excepciones.AccesoDenegadoException;
import ar.edu.unlam.tallerweb1.domain.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ControladorCobro {

private ServicioTurno servicioTurno;
private ServicioCobro servicioCobro;
private ServicioLogin servicioLogin;

    @Autowired
    public ControladorCobro( ServicioTurno servicioTurno,ServicioCobro servicioCobro, ServicioLogin servicioLogin){

        this.servicioTurno = servicioTurno;
        this.servicioCobro= servicioCobro;
        this.servicioLogin = servicioLogin;

    }

     @RequestMapping(path = "/crear-cobro", method = RequestMethod.GET)
        public ModelAndView cobrar(@RequestParam("idTurno") Long idTurno, HttpServletRequest request) {
            try {
                this.servicioLogin.esRecepcionista(request);
                Turno turno= servicioTurno.buscarTurnoPorId(idTurno);
                servicioCobro.guardarCobro(turno);
                servicioTurno.cambiarEstadoDelPago(turno);
                return new ModelAndView("redirect:/home-recepcionista");
            } catch (AccesoDenegadoException e){
                return new ModelAndView("redirect:/home");
            }
    }
    @RequestMapping(path = "/Cobros-Del-Dia")
    public ModelAndView listarTurnos(HttpServletRequest request) {
        try {
            this.servicioLogin.esRecepcionista(request);
            List <Cobro> listaDeCobros = servicioCobro.listarCobrosDelDia();
            ModelMap model = new ModelMap();
            model.put("cobros", listaDeCobros);
            return new ModelAndView("Admin/Cobros-Del-Dia", model);
        } catch (AccesoDenegadoException e){
            return new ModelAndView("redirect:/home");
        }
    }

}

