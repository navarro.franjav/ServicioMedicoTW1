package ar.edu.unlam.tallerweb1.delivery;

public class DatosEspecialidadDTO {
     private Long id;
     private String descripcion;
     private Double costoEspecialidad;

    public Double getCostoEspecialidad() {
        return costoEspecialidad;
    }

    public void setCostoEspecialidad(Double costoEspecialidad) {
        this.costoEspecialidad = costoEspecialidad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
