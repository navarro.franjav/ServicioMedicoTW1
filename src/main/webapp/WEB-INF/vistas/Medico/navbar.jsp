<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>


<html lang="es">
<head>
    <script src="https://kit.fontawesome.com/d29453ffcf.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <title>Medico</title>
</head>
<body>
<header style="background-color: #1b6d85 ">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container">
            <ul class="navbar-nav">
                <li class="nav-item" style="color:white">
                    Linea de Emergencia: 011-2323-2323
                </li>
                <li class="nav-item ms-3" style="color:white">
                    Email: medicina@UNLAM.edu.ar
                </li>
            </ul>
            <div class="d-flex flex-row-reverse">
                <div class="p-2"><a><i class="fa-brands fa-facebook-f" style="color:white"></i></a></div>
                <div class="p-2"><a><i class="fa-brands fa-instagram" style="color:white"></i></a></div>
                <div class="p-2"><a><i class="fa-brands fa-whatsapp" style="color:white"></i></a></div>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a href="home-paciente">
                <img width="50px" class="mx-auto d-block" src="<c:url value="/Images/logo-unlam.png"/>"/>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-4">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="home-medico">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="turnos-asignados">Mis Turnos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="mis-pacientes">Mis Pacientes</a>
                    </li>
                </ul>
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="cerrar-sesion">Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
