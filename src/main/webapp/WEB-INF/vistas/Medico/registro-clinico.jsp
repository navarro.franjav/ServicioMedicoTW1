<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
  <div class="container mt-3">
    <h3 class="mt-5">Registro Clinico</h3>
    <p class="mb-5">
      El/la paciente <b>${registro.paciente.usuario.apellido},${registro.paciente.usuario.nombre}</b> con DNI <b>${registro.paciente.usuario.dni}</b>, <br> fue atendido/a por el/la profesional <b>${registro.medico.usuario.apellido},${registro.medico.usuario.nombre}</b>        el dia <b>${registro.fecha}</b> <br> por el motivo de "<b>${registro.motivo}</b>" y fue diagnosticado/a con <b> ${registro.diagnostico}</b>.
    </p>

    <h3>Observaciones:</h3>
    <p>${registro.observaciones}</p>
  </div>
</main>

</body>
</html>
