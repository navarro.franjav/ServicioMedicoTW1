<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
  <div class="container mt-3">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>Cargar Registro</h1>
      </div>
      <div class="col-lg-12">
        <form:form action="cargar-registro" method="POST" modelAttribute="registroDTO" id="formRegistro">
          <form:input path="idPaciente" value="${registroDTO.idPaciente}"  type="hidden"/>
          <form:input path="idMedico" value="${registroDTO.idMedico}" type="hidden"/>
          <div class="row mt-3">
            <div class="col-6">
              <div class="mb-3">
                <label for="fecha" class="form-label">Fecha</label>
                <form:input path="fecha" required="true" class="form-control" type="date"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="motivo" class="form-label">Motivo</label>
                <form:input path="motivo" required="true" class="form-control" type="text" placeholder="Ingrese el motivo de la consulta"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="diagnostico" class="form-label">Diagnostico</label>
                <form:input path="diagnostico" required="true" class="form-control" type="text" placeholder="Ingrese el diagnostico"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="observaciones" class="form-label">Observaciones</label>
                <form:input path="observaciones" required="true" class="form-control" type="text" placeholder="Ingrese Observaciones"/>
              </div>
            </div>
            <div class="col-6">
              <a href="historial-clinico?idPaciente=${registroDTO.idPaciente}">
                <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
              </a>
            </div>
            <div class="col-6">
              <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
            </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>


</main>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  let form = document.getElementById("formRegistro");

  form.addEventListener("submit", function(event) {
    event.preventDefault();

    if (form.checkValidity()) {
      Swal.fire({
        title: '¿Estás Seguro?',
        text: "El registro no se puede modificar ni borrar una vez creado",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy seguro',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          form.submit();
        }
      })
    } else {
      form.reportValidity();
    }
  });
</script>

</body>
</html>
