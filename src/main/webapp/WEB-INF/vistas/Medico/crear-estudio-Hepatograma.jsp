<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
  <div class="container mt-3">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>Cargar ${nombreEstudio}</h1>
      </div>
      <div class="col-lg-12">
        <form:form action="cargar-hepatograma" method="POST" modelAttribute="estudioDTO" id="formHepatograma">
          <div class="row mt-3">
            <div class="col-6">
              <div class="mb-3">
                <label for="bilirrubinaTotal" class="form-label">Bilirrubina Total</label>
                <form:input path="bilirrubinaTotal" required="true" type="text" class="form-control"
                            placeholder="Ingrese un valor"/>
              </div>
              <form:input path="idTipoEstudio" type="hidden" value="${estudioDTO.idTipoEstudio}"/>
              <form:input type="hidden" path="idMedico" value="${estudioDTO.idMedico}"></form:input>
              <form:input type="hidden" path="idPaciente" value="${estudioDTO.idPaciente}"></form:input>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="bilirrubinaDirecta" class="form-label">Bilirrubina Directa</label>
                <form:input path="bilirrubinaDirecta" required="true" type="text" class="form-control"
                            placeholder="Ingrese un valor"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="fosfatasasAlcalinas" class="form-label">Fosfotasas Alcalinas</label>
                <form:input path="fosfatasasAlcalinas" required="true" type="text" class="form-control"
                            placeholder="Ingrese un valor"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="colesterolTotal" class="form-label">Colesterol Total</label>
                <form:input path="colesterolTotal" required="true" type="text" class="form-control"
                            placeholder="Ingrese un valor"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="proteinasTotales" class="form-label">Proteinas Totales</label>
                <form:input path="proteinasTotales" required="true" type="text" class="form-control"
                            placeholder="Ingrese un valor"/>
              </div>
            </div>
            <div class="col-6">
              <div class="mb-3">
                <label for="albumina" class="form-label">Albumina</label>
                <form:input path="albumina" required="true" type="text" class="form-control"
                            placeholder="Ingrese un valor"/>
              </div>
            </div>
            <div class="col-6">
              <a href="mis-pacientes">
                <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
              </a>
            </div>
            <div class="col-6">
              <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
            </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>

</main>
<script>
  var inputs = document.querySelectorAll('#formHepatograma input[type="text"]');
  inputs.forEach(function(input) {
    input.addEventListener("keypress", function(event) {
      var allowedCharacters = /^[0-9.,]+$/;
      var inputChar = event.key;

      if (!allowedCharacters.test(inputChar)) {
        event.preventDefault();
      } else if (inputChar === ',') {
        event.preventDefault();
        var inputValue = input.value;
        var cursorPosition = input.selectionStart;
        input.value = inputValue.slice(0, cursorPosition) + '.' + inputValue.slice(cursorPosition + 1);
        input.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
      }
    });
  });
</script>
</body>
</html>
