<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
    <div class="container mt-3">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Seleccionar estudio</h1>
            </div>
            <div class="col-lg-12">
                <form:form action="seleccionar-estudio-a-cargar" method="POST" modelAttribute="datosEstudiosDTO">
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="estudio" class="form-label">Estudio</label>
                                <form:select class="form-select" required="true" aria-label="Default select example" path="idEstudio" name="estudio">
                                    <option value="">Seleccione estudio</option>
                                    <c:forEach items="${estudios}" var="estudio">
                                        <option value=<c:out value="${estudio.id}"></c:out>><c:out value="${estudio.nombreEstudio}"></c:out></option>
                                    </c:forEach>
                                </form:select>
                            </div>
                                <form:input type="hidden" path="idMedico" value="${idMedico}"></form:input>
                                <form:input type="hidden" path="idPaciente" value="${idPaciente}"></form:input>
                        </div>
                        <div class="col-6">
                            <a href="mis-pacientes">
                                <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
                            </a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>

</main>

</body>
</html>
