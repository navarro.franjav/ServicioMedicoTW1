<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
  <div class="container mt-3">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>Lista de Turnos</h1>
      </div>
      <div class="col-lg-12">
        <c:choose>
          <c:when test="${turnosAsignados.size() > 0}">
            <table class="table table-bordered dt-responsive nowrap w-100">
              <thead cclass="thead-dark">
              <th scope="col"> Fecha </th>
              <th scope="col"> Hora </th>
              <th scope="col">Hospital </th>
              <th scope="col"> Medico </th>
              <th scope="col"> Motivo </th>
              <th scope="col"> Control </th>
              </thead>
              <tbody>
              <c:forEach items="${turnosAsignados}" var="turno">
                <tr>
                  <td><c:out value="${turno.fecha}"></c:out></td>
                  <td><c:out value="${turno.hora}"></c:out></td>
                  <td><c:out value="${turno.medicoHospital.hospital.nombre}"></c:out></td>
                  <td><c:out value="${turno.medicoHospital.medico.usuario.nombre}">,</c:out><c:out value="${turno.medicoHospital.medico.usuario.apellido}"></c:out></td>
                  <td><c:out value="${turno.motivo}"></c:out></td>
                  <td><a href="cancelar-turno?id=${turno.id}&uId=${turno.paciente.usuario.id}">Cancelar turno</a></td>
                </tr>
              </c:forEach>
              </tbody>
            </table>
          </c:when>
          <c:otherwise>
            <h2>Usted no posee ningún turno asignado</h2>
          </c:otherwise>
        </c:choose>
      </div>


    </div>
  </div>




</main>

</body>
</html>
