<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
    <div class="container mt-3">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Lista de pacientes</h1>
            </div>
            <div class="col-lg-12">
                <c:choose>
                    <c:when test="${pacientes.size() > 0}">
                        <table class="table table-bordered dt-responsive nowrap w-100">
                            <thead cclass="thead-dark">
                            <th scope="col"> Paciente </th>
                            <th scope="col">  </th>
                            <th scope="col">  </th>
                            </thead>
                            <tbody>
                            <c:forEach items="${pacientes}" var="paciente">
                                <tr>
                                    <td><c:out value="${paciente.usuario.apellido}, ${paciente.usuario.nombre}"></c:out></td>
                                    <td><a href="lista-estudios?idPaciente=${paciente.id}&idMedico=${medico.id}">Lista de estudios</a></td>
                                    <td><a href="seleccionar-estudio-a-cargar?idPaciente=${paciente.id}&idMedico=${medico.id}">Cargar estudio</a></td>
                                    <td><a href="historial-clinico?idPaciente=${paciente.id}">Historia Clinica</a></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <h2>Usted no tiene ningun paciente</h2>
                    </c:otherwise>
                </c:choose>
            </div>


        </div>
    </div>


</main>
</body>
</html>
