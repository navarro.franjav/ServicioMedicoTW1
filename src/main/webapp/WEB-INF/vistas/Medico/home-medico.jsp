<%@ include file="navbar.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<main>

    <div class="container">
        <div class="row">
            <div class="col-3 text-center text-white pt-5" style="height: 60vh; background-color: #1b6d85; border-radius: 20px;">
                <div class="d-flex justify-content-center align-items-center mb-5">
                    <div class="d-block rounded-circle" style="width: 125px; height: 125px; overflow: hidden;">
                        <img src="<c:url value="/Images/medico.png"/>" alt="profile" style="max-width: 100%;">
                    </div>
                </div>
                <h5 class="mt-5"><c:out value="${medico.usuario.apellido}"/>, <c:out
                        value="${medico.usuario.nombre}"/></h5>
                <h5>DNI: <c:out value="${medico.usuario.dni}"/></h5>
                <h5>Telefono: <c:out value="${medico.usuario.telefono}"/></h5>
            </div>
            <div class="col-5" style="padding-inline: 4rem;">
                <div class="row">
                    <div class="col-8">
                        <h3>Turnos de hoy</h3>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${turnosAsignadosHoy.size() > 0}">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Hora</th>
                                <th>Motivo</th>
                                <th>Paciente</th>
                                <th>Especialidad</th>
                                <th>Lugar</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${turnosAsignadosHoy}" var="turno">
                                <tr>
                                    <td><c:out value="${turno.hora}"/></td>
                                    <td><c:out value="${turno.motivo}"/></td>
                                    <td><c:out
                                            value="${turno.paciente.usuario.apellido}, ${turno.paciente.usuario.nombre}"/>,
                                    </td>
                                    <td><c:out value="${turno.medicoHospital.especialidad.descripcion}"/></td>
                                    <td><c:out value="${turno.medicoHospital.hospital.nombre}"/></td>
                                </tr>
                            </c:forEach>

                            </tbody>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <h3>No tenes ningun turno agendado hoy</h3>
                    </c:otherwise>
                </c:choose>

            </div>
        </div>
    </div>
</main>

</body>
</html>
