<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
    <div class="container mt-3">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Cargar Estudio ${nombreEstudio}</h1>
            </div>
            <div class="col-lg-12">
                <form:form action="cargar-colesterol" method="POST" modelAttribute="estudioDTO" id="formColesterol">
                    <div class="row mt-3">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="colesterolLDL" class="form-label">Colesterol LDL</label>
                                <form:input path="colesterolLDL" required="true" class="form-control" type="text"
                                            placeholder="Ingrese un valor"/>
                            </div>
                            <form:input path="idTipoEstudio" type="hidden" value="${estudioDTO.idTipoEstudio}"/>
                            <form:input type="hidden" path="idMedico" value="${estudioDTO.idMedico}"></form:input>
                            <form:input type="hidden" path="idPaciente" value="${estudioDTO.idPaciente}"></form:input>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="colesterolTotal" class="form-label">Colesterol Total</label>
                                <form:input path="colesterolTotal" required="true" class="form-control" type="text"
                                            placeholder="Ingrese un valor"/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="trigliceridos" class="form-label">Trigliceridos</label>
                                <form:input path="trigliceridos" required="true" class="form-control" type="text"
                                            placeholder="Ingrese un valor"/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="colesterolHDL" class="form-label">Colesterol HDL</label>
                                <form:input path="colesterolHDL" required="true" class="form-control" type="text"
                                            placeholder="Ingrese un valor"/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="colesterolNoHDL" class="form-label">Colesterol No HDL</label>
                                <form:input path="colesterolNoHDL" required="true" class="form-control" type="text"
                                            placeholder="Ingrese un valor"/>
                            </div>
                        </div>
                        <div class="col-6">

                        </div>
                        <div class="col-6">
                            <a href="mis-pacientes">
                                <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
                            </a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>


</main>
<script>
    var inputs = document.querySelectorAll('#formColesterol input[type="text"]');
    inputs.forEach(function(input) {
        input.addEventListener("keypress", function(event) {
            var allowedCharacters = /^[0-9.,]+$/;
            var inputChar = event.key;

            if (!allowedCharacters.test(inputChar)) {
                event.preventDefault();
            } else if (inputChar === ',') {
                event.preventDefault();
                var inputValue = input.value;
                var cursorPosition = input.selectionStart;
                input.value = inputValue.slice(0, cursorPosition) + '.' + inputValue.slice(cursorPosition + 1);
                input.setSelectionRange(cursorPosition + 1, cursorPosition + 1);
            }
        });
    });
</script>

</body>
</html>
