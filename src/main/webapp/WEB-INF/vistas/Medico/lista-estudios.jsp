<%@ include file="navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<main>
  <div class="container mt-3">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>Lista de estudios</h1>
      </div>
      <div class="col-lg-12">
        <c:choose>
          <c:when test="${estudios.size() > 0}">
            <table class="table table-bordered dt-responsive nowrap w-100">
              <thead cclass="thead-dark">
              <th scope="col"> Fecha </th>
              <th scope="col"> Estudio </th>
              <th scope="col"> Profesional </th>
              <th scope="col">  </th>
              </thead>
              <tbody>
              <c:forEach items="${estudios}" var="estudio">
                <tr>
                  <td><c:out value="${estudio.fecha}"/></td>
                  <td><c:out value="${estudio.tipoEstudio.nombreEstudio}"></c:out></td>
                  <td><c:out value="${estudio.medico.usuario.apellido}, ${estudio.medico.usuario.nombre}"></c:out></td>
                  <td><a href="ver-estudio-${estudio.tipoEstudio.nombreEstudio}?idEstudio=${estudio.id}">Ver estudio</a></td>
                </tr>
              </c:forEach>
              </tbody>
            </table>
          </c:when>
          <c:otherwise>
            <h2>No hay ningun estudio cargado para este paciente</h2>
          </c:otherwise>
        </c:choose>
      </div>


    </div>
  </div>




</main>

</body>
</html>
