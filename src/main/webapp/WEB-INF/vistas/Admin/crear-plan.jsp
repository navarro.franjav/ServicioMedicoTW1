<%--
  Created by IntelliJ IDEA.
  User: maria
  Date: 10/5/2023
  Time: 13:20
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<div class="container mt-3">
    <h1>${titulo} Plan</h1>
    <c:if test="${not empty sessionScope.errorPlanExistente}">
        <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                    ${sessionScope.errorPlanExistente}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </c:if>
            <form:form action="${url}" method="POST" modelAttribute="datosPlan">
                <div class="row mt-3">
                    <div class="col-6">
                        <div class="mb-3">
                            <label class="form-label" for="nombre">Nombre</label>
                            <form:input path="nombre" type="text" id="nombre" class="form-control" placeholder="Ingrese Nombre" />
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            <label class="form-label" for="precio">Precio</label>
                            <form:input path="precio" type="text" id="precio" class="form-control" placeholder="Ingrese Precio" />
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mb-3">
                            <label class="form-label" for="descripcion">Descripcion</label>
                            <form:input path="descripcion" type="text" id="descripcion" class="form-control" placeholder="Ingrese Descripción"/>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="mb-3">
                            <label class="form-label">Seleccionar Especialidades:</label>
                            <form:checkboxes path="listaEspecialidadesSeleccionadas" items="${listaEspecialidades}" itemLabel="descripcion" itemValue="id"/>
                        </div>
                    </div>
                    <form:input type="hidden" path="id"></form:input>
                    <div class="col-6">
                        <a href="lista-planes"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                    </div>
                </div>
            </form:form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
