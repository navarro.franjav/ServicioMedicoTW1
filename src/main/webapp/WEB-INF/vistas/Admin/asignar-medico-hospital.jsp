<%@ include file="headerAdmi.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Hospitales Asignados</h1>
        </div>
        <div class="col-lg-12">
            <form:form action="asignar-medico-hospital" method="POST" modelAttribute="datosMedicoHospital">
                <div class="row mt-3">
                    <div class="col-6">
                        <div class="mb-3">
                            <input required type="hidden" value="${id_medico}" class="form-control" id="id_medico"
                                   name="id_medico">
                            <label for="id_hospital" class="form-label">Hospital</label>
                            <select class="form-select" required="true" aria-label="Default select example" id="id_hospital"
                                    name="id_hospital">
                                <option value=null selected disabled>Seleccione Hospital</option>
                                <c:forEach items="${hospitales}" var="hospital">
                                    <option value=<c:out value="${hospital.id}"></c:out>><c:out value="${hospital.nombre}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="id_especialidad" class="form-label">Especialidad</label>
                            <select class="form-select" required="true" id="id_especialidad" name="id_especialidad">
                                <option value=null>Seleccione especialidad</option>
                                <c:forEach items="${especialidades}" var="especialidad">
                                    <option value="<c:out value="${especialidad.id}"/>"><c:out value="${especialidad.descripcion}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <a href="<c:url value="lista-medico-hospital?id=${id_medico}"/>">
                            <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
                        </a>
                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>




