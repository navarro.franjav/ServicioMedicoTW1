<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 5/7/2023
  Time: 16:21
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerRecepcionista.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container mt-3">
  <div class="row">
    <div class="col-lg-12 text-center">
      <h1>Cobros del día</h1>
    </div>
    <div class="col-lg-12">
      <c:choose>
        <c:when test="${cobros.size() > 0}">
          <table class="table table-bordered dt-responsive nowrap w-100">
            <thead cclass="thead-dark">
            <th scope="col">
              Paciente
            </th>
            <th scope="col">
              Especiadidad
            </th>
            <th scope="col">
              Monto
            </th>


            </thead>
            <tbody>
            <c:forEach items="${cobros}" var="cobro" >
              <tr>

                <td><c:out value="${cobro.turno.paciente.usuario.nombre} ${cobro.turno.paciente.usuario.apellido}"></c:out></td>
                <td><c:out value="${cobro.turno.medicoHospital.especialidad.descripcion}"></c:out></td>
                <td><c:out value="${cobro.turno.valorConsulta}" ></c:out></td>

              </tr>
            </c:forEach>
            <p> </p>
            </tbody>
          </table>
        </c:when>
        <c:otherwise>
          <h2>No hay ningún cobro registrado</h2>
        </c:otherwise>
      </c:choose>
    </div>


  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
