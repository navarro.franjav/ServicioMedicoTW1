<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 14/6/2023
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de Turnos para el Medico: ${medicoHospital.medico.usuario.nombre}  ${medicoHospital.medico.usuario.apellido}

                en el Hospital: ${medicoHospital.hospital.nombre}</h1>
        </div>
        <c:if test="${not empty mensajeDeExito}">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                    ${mensajeDeExito}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </c:if>
        <a href="<c:url value="/generar-turnos?idMedicoHospital=${medicoHospital.id}"/>">Generar turnos</a>

        <div class="col-lg-12">
            <c:choose>
                <c:when test="${turnosMedicoHospital.size() > 0}">
                    <table class="table table-bordered dt-responsive nowrap w-100">
                        <thead cclass="thead-dark">
                        <th scope="col">
                            IdTurno
                        </th>
                        <th scope="col">
                            Fecha
                        </th>
                        <th scope="col">
                            Hora
                        </th>
                        <th scope="col">
                            Paciente
                        </th>
                        <th scope="col">
                            Motivo
                        </th>
                        </thead>
                        <tbody>
                        <c:forEach items="${turnosMedicoHospital}" var="turno">
                            <tr>
                                <td><c:out value="${turno.id}"></c:out></td>
                                <td><c:out value="${turno.fecha} ${medico.usuario.nombre}"></c:out></td>
                                <td><c:out value="${turno.hora}"></c:out></td>
                                <td><c:out value="${turno.paciente.usuario.apellido} ${turno.paciente.usuario.nombre}"></c:out></td>
                                <td><c:out value="${turno.motivo}"></c:out></td></tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2>No hay ningún turno.</h2>

                </c:otherwise>
            </c:choose>
        </div>


    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>

