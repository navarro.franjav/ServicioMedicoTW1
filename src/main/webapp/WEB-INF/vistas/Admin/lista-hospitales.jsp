<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 9/5/2023
  Time: 18:33
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de Hospitales</h1>
        </div>
        <div class="col-lg-12">
            <c:choose>
            <c:when test="${hospitales.size() > 0}">
            <table class="table table-bordered dt-responsive nowrap w-100">
                <thead cclass="thead-dark">
                <th scope="col">Id </th>
                <th scope="col">Nombre </th>
                <th scope="col">Dirección </th>
                <th scope="col">Ciudad </th>
                <th scope="col">Código Postal </th>
                <th scope="col">Teléfono </th>
                </thead>
                <tbody>
                <c:forEach items="${hospitales}" var="hospital">
                    <tr>
                        <td><c:out value="${hospital.id}"></c:out></td>
                        <td><c:out value="${hospital.nombre}"></c:out></td>
                        <td><c:out value="${hospital.direcion}"></c:out></td>
                        <td><c:out value="${hospital.ciudad}"></c:out></td>
                        <td><c:out value="${hospital.codigoPostal}"></c:out></td>
                        <td><c:out value="${hospital.telefono}"></c:out></td>
                        <td>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#ModalEliminar">Eliminar</a>
                            <!-- Modal -->
                            <div class="modal fade" id="ModalEliminar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Hospital</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Se borrará el Hospital.¿Está seguro que desea eliminarlo?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                            <a href="<c:url value="/eliminar-hospital?id=${hospital.id}"/>" class="btn btn-primary">Eliminar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td><a href="<c:url value="/modificar-hospital?id=${hospital.id}"/>">Modificar</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <a href="crear-hospital">Nuevo Hospital</a>
            </c:when>
            <c:otherwise>
                <h2>No hay ningún hospital registrado</h2>
                <a href="crear-hospital">Crear Hospital</a>
            </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
<script>
    //capturar mensaje
    let mensaje = '<%= request.getAttribute("mensaje") %>';
    if (mensaje !== null && mensaje !== 'null') alert(mensaje)
</script>
</body>
</html>
