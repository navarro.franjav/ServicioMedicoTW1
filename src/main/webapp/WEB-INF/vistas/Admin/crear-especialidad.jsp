<%@ include file="headerAdmi.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<body>
<div class="container mt-3">
  <div class="row">
    <div class="col-6">
      <h1>${titulo} Especialidad</h1>
    </div>

    <c:if test="${not empty mensajeDeError}">
      <div class="col-9">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            ${mensajeDeError}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      </div>
    </c:if>
  </div>
  <form:form action="${url}" method="POST" modelAttribute="datosEspecialidad">
    <div class="row mt-3">
      <div class="col-6">
        <div class="mb-3">
          <label for="descripcion" class="form-label">Descripción</label>
          <form:input path="descripcion" required="true" type="text" class="form-control" id="descripcion"
                      placeholder="Ingrese la Descripción"/>
        </div>
      </div>
      <div class="col-6">
        <div class="mb-3">
          <label for="costo" class="form-label">Costo</label>
          <form:input path="costoEspecialidad" required="true" type="text" class="form-control" id="costo"
                      placeholder="Ingrese el Costo"/>
        </div>
      </div>
      <form:input type="hidden" path="id"></form:input>
      <div class="col-6">
        <a href="lista-especialidades">
          <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
        </a>
      </div>
      <div class="col-6">
        <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
      </div>

    </div>
  </form:form>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>

