<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 5/7/2023
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerRecepcionista.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mt-3">
  <div class="row">
    <div class="col-lg-12 text-center">
      <h1>Lista de Turnos</h1>
    </div>
    <div class="col-lg-12">
      <c:choose>
        <c:when test="${turnos.size() > 0}">
          <table class="table table-bordered dt-responsive nowrap w-100">
            <thead cclass="thead-dark">
            <th scope="col">
              Fecha y Hora

            </th>
            <th scope="col">
              Hospital
            </th>
            <th scope="col">
              Paciente
            </th>
            <th scope="col">
              Motivo
            </th>

            </thead>
            <tbody>
            <c:forEach items="${turnos}" var="turno">
              <tr>

                <td><c:out value="${turno.fecha} ${turno.hora}"></c:out></td>
                <td><c:out value="${turno.medicoHospital.hospital.nombre}"></c:out></td>

                <c:choose>

                  <c:when test="${turno.paciente != null}">
                    <td>  <c:out value="${turno.paciente.usuario.nombre} ${turno.paciente.usuario.apellido}"></c:out>  </td>
                  </c:when>
                  <c:otherwise>
                    <td>   <a style="font-size: small" href="<c:url value="/asignar-paciente?id=${turno.id}"/>">Asignar Paciente</a></td>
                  </c:otherwise>

                </c:choose>
                <td><c:out value="${turno.motivo}"></c:out></td>
                </td>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </c:when>
      </c:choose>
    </div>


  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
