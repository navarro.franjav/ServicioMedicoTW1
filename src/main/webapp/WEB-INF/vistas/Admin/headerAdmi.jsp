<%--
  Created by IntelliJ IDEA.
  User: maria
  Date: 27/5/2023
  Time: 18:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <script src="https://kit.fontawesome.com/d29453ffcf.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css">
    <link href="css/home3.css" rel="stylesheet">
    <link href="css/home-admi.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <title>Admi</title>
    <style>
        body{
            background-color: unset;
        }
    </style>
</head>
<body>
    <header style="background-color: #1b6d85 ">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
                <ul class="navbar-nav">
                    <li class="nav-item" style="color:white">
                        Linea de Emergencia: 011-2323-2323
                    </li>
                    <li class="nav-item ms-3" style="color:white">
                        Email: medicina@UNLAM.edu.ar
                    </li>
                </ul>
                <div class="d-flex flex-row-reverse">
                    <div class="p-2"><a><i class="fa-brands fa-facebook-f" style="color:white"></i></a></div>
                    <div class="p-2"><a><i class="fa-brands fa-instagram" style="color:white"></i></a></div>
                    <div class="p-2"><a><i class="fa-brands fa-whatsapp" style="color:white"></i></a></div>
                </div>
            </div>
        </nav>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a href="home-admin">
                    <img style="max-width:50px" class="mx-auto d-block" src="<c:url value="/Images/logo-unlam.png"/>"/>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="lista-planes">Planes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="lista-medicos">Médicos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="lista-pacientes">Pacientes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="lista-hospitales">Hospitales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="lista-especialidades">Especialidades</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="lista-facturas">Lista de Facturas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="cerrar-sesion">Cerrar Sesión</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
