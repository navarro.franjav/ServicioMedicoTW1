<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de los medicos registrados</h1>
        </div>
        <div class="col-lg-12">
            <c:choose>
                <c:when test="${medicos.size() > 0}">
                    <table class="table table-bordered dt-responsive nowrap w-100">
                        <thead cclass="thead-dark">
                        <th scope="col">
                            IdUsuario
                        </th>
                        <th scope="col">
                            Nombre completo
                        </th>
                        <th scope="col">
                            Dni
                        </th>
                        <th scope="col">
                            Email
                        </th>
                        <th scope="col">
                            Telefono
                        </th>
                        <th scope="col">
                        </th>
                        <th scope="col">

                        </th>
                        <th scope="col">

                        </th>
                        </thead>
                        <tbody>
                        <c:forEach items="${medicos}" var="medico">
                            <tr>
                                <td><c:out value="${medico.usuario.id}"></c:out></td>
                                <td><c:out value="${medico.usuario.apellido}, ${medico.usuario.nombre}"></c:out></td>
                                <td><c:out value="${medico.usuario.dni}"></c:out></td>
                                <td><c:out value="${medico.usuario.email}"></c:out></td>
                                <td><c:out value="${medico.usuario.telefono}"></c:out></td>
                                <td><a href="<c:url value="lista-medico-hospital?id=${medico.id}"/>">Hospital asignados</a>
                                </td>
                                <td>
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#ModalEliminar${medico.id}">Eliminar</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalEliminar${medico.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Médico</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    Se borrará el Médico.¿Está seguro que desea eliminarlo?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                    <a href="<c:url value="/eliminar-medico?id=${medico.id}"/>" class="btn btn-primary">Eliminar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><a href="<c:url value="/modificar-medico?id=${medico.id}"/>">Modificar</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <a href="crear-medico">Nuevo medico</a>
                </c:when>
                <c:otherwise>
                    <h2>No hay ningún medico registrado</h2>
                    <a href="crear-medico">Crear medico</a>
                </c:otherwise>
            </c:choose>
        </div>


    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
