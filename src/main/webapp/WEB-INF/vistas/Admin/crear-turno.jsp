<%--
  Created by IntelliJ IDEA.
  User: maria
  Date: 31/5/2023
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<div class="container mt-3">
  <h1>Nuevo Turno</h1>
  <form:form action="guardar-turno" method="POST" modelAttribute="datosTurno">
    <div class="row mt-3">
      <div class="col-6">
        <div class="mb-3">
          <label class="form-label" for="paciente">Paciente</label>
          <form:input path="cliente_id" type="text" id="paciente" class="form-control" placeholder="Ingrese Paciente" />
        </div>
      </div>

      <div class="col-6">
        <div class="mb-3">
          <label class="form-label" for="hospital">Hospital</label>
          <form:input path="hospital_id" type="text" id="hospital" class="form-control" placeholder="Ingrese Hospital" />
        </div>
      </div>
      <div class="col-12">
        <div class="mb-3">
          <label class="form-label" for="medico">Médico</label>
          <form:input path="medico_id" type="text" id="medico" class="form-control" placeholder="Ingrese Médico"/>
        </div>
      </div>
      <div class="col-12">
        <div class="mb-3">
          <label class="form-label" for="fechaHora">Fecha y Hora</label>
          <form:input path="fechaHora" type="date" id="fechaHora" class="form-control" placeholder="Ingrese Fecha y Hora"/>
        </div>
      </div>
      <div class="col-12">
        <div class="mb-3">
          <label class="form-label" for="motivoConsulta">Motivo de la Consulta</label>
          <form:input path="motivoConsulta" type="text" id="motivoConsulta" class="form-control" placeholder="Ingrese Motivo de la Consulta"/>
        </div>
      </div>
      <div class="col-6">
        <a href="${pageContext.request.contextPath}/"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
      </div>
      <div class="col-6">
        <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
      </div>
    </div>
  </form:form>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
