<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="headerAdmi.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <body>
        <div class="container mt-3">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1>Lista de Facturas</h1>
                </div>
                <div class="col-lg-12">
                    <c:choose>
                        <c:when test="${facturas.size() > 0}">
                            <table class="table table-bordered dt-responsive nowrap w-100">
                                <thead class="thead-dark">
                                <th scope="col"> Código </th>
                                <th scope="col"> Paciente </th>
                                <th scope="col"> Emisión </th>
                                <th scope="col"> Vencimiento </th>
                                <th scope="col"> Valor </th>
                                <th scope="col"> Condición </th>
                                </thead>
                                <tbody>
                                <c:forEach items="${facturas}" var="factura">
                                    <tr>
                                        <td><c:out value="${factura.codigo}"></c:out></td>
                                        <td><c:out value="${factura.paciente.usuario.apellido} ${factura.paciente.usuario.nombre}"></c:out><br>
                                            <span style="font-weight: bold">( ${factura.paciente.usuario.dni}, ${factura.paciente.usuario.email} )</span>
                                        </td>
                                        <td><c:out value="${factura.emision}"></c:out></td>
                                        <td><c:out value="${factura.vencimiento}"></c:out></td>
                                        <td>$ <c:out value="${factura.valor}"></c:out></td>
                                        <c:choose>
                                            <c:when test="${factura.pago == true}">
                                                <td class="text-success">Paga</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td class="text-danger">Impaga</td>
                                            </c:otherwise>
                                        </c:choose>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <h2>No existe ningúna factura registrada</h2>
                        </c:otherwise>
                    </c:choose>
                </div>


            </div>
        </div>
    </body>
</html>
