<%@ include file="headerAdmi.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Asignar dia de trabajo para ${medicoHospital.medico.usuario.apellido} en ${medicoHospital.hospital.nombre}</h1>
        </div>
        <c:if test="${asignarMasDias == true}">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <div class="row">
                    <div class="col-8">
                        <h3>Si no desea agregar mas dias de trabajo presione aqui</h3>
                    </div>
                    <div class="col-3">
                        <a href="<c:url value="lista-medico-hospital?id=${medicoHospital.medico.id}"/>">
                            <button type="button" class="btn btn-primary">Volver a la lista</button>
                        </a>
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </c:if>
        <c:if test="${not empty mensajeDeError}">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ${mensajeDeError}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
        </c:if>
        <div class="col-lg-12">
            <form:form action="asignar-dias" method="POST" modelAttribute="datosDiaTrabajo">
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="mb-3">
                            <label for="nombreDia" class="form-label">Dia</label>
                            <form:select path="nombreDia" required="true" class="form-select">
                                <form:option value="">Seleccione dia</form:option>
                                <c:forEach items="${diasDisponibles}" var="dia">
                                    <form:option value="${dia}" label="${dia}"></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <input required type="hidden" value="${medicoHospital.id}" class="form-control"
                                   id="medicoHospitalId" name="medicoHospitalId">
                            <label for="horaInicio" class="form-label">Hora Inicio</label>
                            <form:select path="horaInicio" required="true" class="form-select">
                                <form:option value="">Seleccion hora inicio</form:option>
                                <c:forEach items="${horarioLaboral}" var="hora">
                                    <form:option value="${hora.hora}" label="${hora.descripcion}"></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="horaFin" class="form-label">Hora Fin</label>
                            <form:select path="horaFin" required="true" class="form-select">
                                <form:option value="">Selecciona hora fin</form:option>
                                <c:forEach items="${horarioLaboral}" var="hora">
                                    <form:option value="${hora.hora}" label="${hora.descripcion}"></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-6">
                      <%--  <a href="${pageContext.request.contextPath}/">
                            <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
                        </a>--%>
                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>

