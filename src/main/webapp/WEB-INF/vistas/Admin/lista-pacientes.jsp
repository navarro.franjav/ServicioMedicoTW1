<%--
  Created by IntelliJ IDEA.
  User: Pablo
  Date: 06/05/2023
  Time: 12:34
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de Pacientes</h1>
        </div>
        <div class="col-lg-12">
            <a href="crear-paciente">Nuevo Paciente</a>
            <c:choose>
            <c:when test="${pacientes.size() > 0}">
                <table class="table table-bordered dt-responsive nowrap w-100">
                <thead cclass="thead-dark">
                <th scope="col"> DNI </th>
                <th scope="col">Nombre </th>
                <th scope="col">Apellido </th>
                <th scope="col"> Email </th>
                <th scope="col">Plan </th>
                <th scope="col"> Control </th>
                </thead>
                <tbody>
                <c:forEach items="${pacientes}" var="paciente">
                    <tr>
                        <td><c:out value="${paciente.usuario.dni}"></c:out></td>
                        <td><c:out value="${paciente.usuario.nombre}"></c:out></td>
                        <td><c:out value="${paciente.usuario.apellido}"></c:out></td>
                        <td><c:out value="${paciente.usuario.email}"></c:out></td>
                        <td><c:out value="${paciente.plan.nombre}"></c:out></td>

                        <td>
                            <a href="<c:url value="/eliminar-paciente?id=${paciente.id}"/>">Eliminar</a>
                            <a href="<c:url value="/editar-paciente?id=${paciente.id}"/>">Modificar</a>
                            <a href="<c:url value="/generar-factura-individual?id=${paciente.id}"/>">Generar Factura</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                </table>
            </c:when>
                <c:otherwise>
                    <h2>No hay ningún paciente registrado</h2>
                </c:otherwise>
            </c:choose>
        </div>


    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
<script>
    //capturar mensaje
    let mensaje = '<%= request.getAttribute("mensaje") %>';
    if (mensaje !== null && mensaje !== 'null') alert(mensaje)
</script>
</body>
</html>

