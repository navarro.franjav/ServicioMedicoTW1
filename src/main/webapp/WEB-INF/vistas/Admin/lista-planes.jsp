<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mt-3">
    <div class="row">

        <div class="col-lg-12 text-center">
            <h1>Lista de PLanes</h1>
        </div>
        <div class="col-lg-12">
            <c:if test="${not empty sessionScope.mensaje}">
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${sessionScope.mensaje}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </c:if>
            <c:choose>
                <c:when test="${listaPlanEspecialidad.size() > 0}">
                    <a class="btn btn-secondary" href="crear-plan">Nuevo Plan</a>

                    <table class="table table-bordered dt-responsive nowrap w-100">
                        <thead cclass="thead-dark">
                        <th scope="col">
                            Id
                        </th>
                        <th scope="col">
                            Nombre
                        </th>
                        <th scope="col">
                            Descripción
                        </th>
                        <th scope="col">
                            Precio
                        </th>
                        <th scope="col">
                            Especialidad
                        </th>
                        <th scope="col">

                        </th>
                        </thead>
                        <tbody>
                        <c:forEach items="${planes}" var="plan">
                            <tr>
                                <td><c:out value="${plan.id}"></c:out></td>
                                <td><c:out value="${plan.nombre}"></c:out></td>
                                <td><c:out value="${plan.descripcion}"></c:out></td>
                                <td><c:out value="${plan.precio}"></c:out></td>

                                <td>
                                    <c:forEach items="${listaPlanEspecialidad}" var="especialidad">
                                        <c:if test="${plan.id == especialidad.plan.id}">
                                            <c:out value="${especialidad.especialidad.descripcion}"></c:out>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td><a href="<c:url value="/eliminar-plan?id=${plan.id}"/>">Eliminar</a>
                                </td>
                                <td><a href="<c:url value="/modificar-plan?id=${plan.id}"/>">Modificar</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2>No hay ningún plan registrado</h2>
                    <a class="btn btn-secondary" href="crear-plan">Crear Plan</a>
                </c:otherwise>
            </c:choose>
        </div>


    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>


