<%--
  Created by IntelliJ IDEA.
  User: FranciscoJavierNavar
  Date: 5/7/2023
  Time: 6:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <main>
        <h1 class="text-center my-3">Bienvenido ${usuario.nombre} </h1>
        <div class="d-flex flex-wrap justify-content-around">
            <div class="card mb-3 col-2" style="max-width: 20rem;">
                    <div class="card-header d-flex justify-content-center">
                        <a href="crear-plan"><i style="font-size: 100px" class="fa-solid fa-notes-medical"></i> </a>
                    </div>
                    <div class="card-body d-flex justify-content-center">
                        <a class="card-tittle" href="crear-plan">Nuevo Plan</a>
                    </div>
            </div>
            <div class="card mb-3 col-2" style="max-width: 20rem;">
                <div class="card-header d-flex justify-content-center">
                    <a href="crear-medico"><i style="font-size: 100px" class="fa-solid fa-user-doctor"></i> </a>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <a href="crear-medico"> Nuevo Doctor</a>
                </div>
            </div>

            <div class="card mb-3 col-2" style="max-width: 20rem;">
                <div class="card-header d-flex justify-content-center">
                    <a href="crear-paciente"> <i style="font-size: 100px" class="fa-regular fa-pen-to-square"></i></a>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <a href="crear-paciente "> Nuevo Paciente </a>
                </div>
            </div>

            <div class="card mb-3 col-2" style="max-width: 20rem;">
                <div class="card-header d-flex justify-content-center">
                    <a href="crear-hospital"><i style="font-size: 100px" class="fa-solid fa-hospital"></i></a>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <a href="crear-hospital"> Nuevo Hospital</a>
                </div>
            </div>
            <div class="card mb-3 col-2" style="max-width: 20rem;">
                <div class="card-header d-flex justify-content-center">
                    <a href="crear-turno"> <i style="font-size: 100px" class="fa-solid fa-bell"></i></a>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <a href="crear-turno"> Nuevo Turno </a>
                </div>
            </div>

        </div>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" ></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
