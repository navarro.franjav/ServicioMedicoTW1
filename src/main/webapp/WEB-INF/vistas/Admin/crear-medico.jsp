<%@ include file="headerAdmi.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-3">
            <h1>${titulo} Médico</h1>
        </div>

        <c:if test="${not empty mensajeDeError}">
            <div class="col-9">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ${mensajeDeError}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </c:if>
    </div>
    <form:form action="${url}" method="POST" modelAttribute="datosMedico">
        <div class="row mt-3">
            <div class="col-6">
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <form:input path="nombre" required="true" type="text" class="form-control" id="nombre"
                                placeholder="Ingrese el Nombre"/>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="apellido" class="form-label">Apellido</label>
                    <form:input path="apellido" required="true" type="text" class="form-control" id="apellido"
                                placeholder="Ingrese el Apellido"/>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <form:input path="email" required="true" type="email" class="form-control" id="email"
                                placeholder="Ingrese Email"/>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="telefono" class="form-label">Teléfono</label>
                    <form:input path="telefono" required="true" type="tel" class="form-control" id="telefono"
                                placeholder="Ingrese Teléfono"/>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="password" class="form-label">Contraseña</label>
                    <form:input path="password" required="true" type="password" min="1" max="10" class="form-control"
                                id="password" placeholder="Ingrese Contraseña"/>
                </div>
            </div>
            <div class="col-3">
                <div class="mb-3">
                    <label for="dni" class="form-label">DNI</label>
                    <form:input path="dni" required="true" type="number" class="form-control" id="dni"
                                placeholder="Ingrese DNI"/>
                </div>
            </div>
                <form:input type="hidden" path="idMedico"></form:input>
            <div class="col-6">
                <a href="lista-medicos">
                    <button type="button" class="btn btn-danger mt-3 col-12">Cancelar</button>
                </a>
            </div>
            <div class="col-6">
                <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
            </div>

        </div>
    </form:form>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
