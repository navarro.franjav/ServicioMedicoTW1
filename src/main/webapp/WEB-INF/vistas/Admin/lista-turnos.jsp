<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de Turnos</h1>
        </div>
        <div class="col-lg-12">
            <c:choose>
                <c:when test="${turnos.size() > 0}">
                    <a href="crear-turno">Nuevo Turno</a>
                    <table class="table table-bordered dt-responsive nowrap w-100">
                        <thead cclass="thead-dark">
                        <th scope="col">
                            Id
                        </th>
                        <th scope="col">
                            Fecha
                        </th>
                        <th scope="col">
                            Hora
                        </th>
                        <th scope="col">
                            MedicoHospital
                        </th>
                        <th scope="col">
                            Motivo
                        </th>
                        <th scope="col">
                            Fecha y Hora
                        </th>
                        <th scope="col">
                            Motivo
                        </th>
                        <th scope="col">
                            Paciente
                        </th>
                        <th scope="col">
                            Valor Consulta
                        </th>
                        <th scope="col">

                        </th>
                        </thead>
                        <tbody>
                        <c:forEach items="${turnos}" var="turno">
                            <tr>
                                <td><c:out value="${turno.paciente.usuario.nombre}${turno.paciente.usuario.apellido}"></c:out></td>
                                <td><c:out value="${turno.medicoHospital.hospital.nombre}"></c:out></td>
                                <td><c:out value="${turno.fecha}${turno.hora}"></c:out></td>
                                <td><c:out value="${turno.motivo}"></c:out></td>
                                <td><a href="<c:url value="/eliminar-turno?id=${turno.id}"/>">Eliminar</a>
                                </td>
                                <td><a href="<c:url value="/modificar-turno?id=${turno.id}"/>">Modificar</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2>No hay ningún turno registrado</h2>
                    <a href="crear-turno">Crear Turno</a>
                </c:otherwise>
            </c:choose>
        </div>


    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
