<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 11/7/2023
  Time: 22:20
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerRecepcionista.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<div class="container mt-3">
  <div class="row">
    <div class="col-3">
      <h1>Editar Turno</h1>
    </div>

    <c:if test="${not empty mensajeDeError}">
      <div class="col-9">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            ${mensajeDeError}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      </div>
    </c:if>
  </div>
  <form:form action="asignar-paciente" method="POST" modelAttribute="datosTurno">
  <div class="row mt-3">
    <div class="col-6">
      <div class="mb-3">
        <label for="dni" class="form-label">DNI</label>
        <form:input path="DNIPaciente" required="true" type="dni" class="form-control" id="dni" placeholder="Ingrese el DNI"/>
        <form:input path="idTurno"  type="hidden" value="${datosTurno.idTurno}"/>

      </div>
    </div>

    <div class="col-6">
      <div class="mb-3">
        <label for="motivo" class="form-label">Motivo</label>
        <form:input path="motivo" required="true" type="text" class="form-control" id="motivo" placeholder="Ingrese el Motivo"/>
      </div>
    </div>

    <div class="col-6">
      <a href="lista-turnosRecepcion"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
    </div>
    <div class="col-6">
      <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
    </div>

  </div>
</form:form>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>

