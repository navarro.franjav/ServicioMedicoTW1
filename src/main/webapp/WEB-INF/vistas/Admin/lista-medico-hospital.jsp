
<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <body>
        <div class="container mt-3">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1>Hospitales Asignados de : ${medico.usuario.apellido}, ${medico.usuario.nombre}</h1>
                </div>

                <a href="asignar-medico-hospital?id=${medico.id}">Asignar nuevo hospital</a>
                <div class="col-lg-12">
                    <c:choose>
                        <c:when test="${listaMedicoHospitalDia.size() > 0}">
                            <table class="table table-bordered dt-responsive nowrap w-100">
                                <thead cclass="thead-dark">
                                <th scope="col"> Hospital </th>
                                <th scope="col"> Especialidad</th>
                                <th scope="col"> Dias y horarios </th>
                                <th scope="col"> Control </th>
                                </thead>
                                <tbody>
                                <c:forEach items="${listaMedicoHospitalDia}" var="medicoHospitalDia">
                                    <tr>
                                        <td><c:out value="${medicoHospitalDia.medicoHospital.hospital.nombre}"></c:out></td>
                                        <td><c:out value="${medicoHospitalDia.medicoHospital.especialidad.descripcion}"></c:out></td>
                                        <td>
                                            <c:forEach items="${medicoHospitalDia.listaDeDias}" var="dia">
                                                ${dia.nombreDia} : ${dia.horaInicio} - ${dia.horaFin} <br>
                                            </c:forEach>
                                        </td>
                                        <td>
                                            <a href="<c:url value="lista-turnosGenerados?id=${medicoHospitalDia.medicoHospital.id}"/>">Administrar Turnos</a>
                                         <!--   <a href="<c:url value="/generar-turnos?idMedicoHospital=${medicoHospitalDia.medicoHospital.id}"/>">Generar turnos</a> -->

                                            <a href="#" data-bs-toggle="modal" data-bs-target="#ModalEliminar${medicoHospitalDia.medicoHospital.id}">Eliminar</a>

                                            <!-- Modal -->
                                            <div class="modal fade" id="ModalEliminar${medicoHospitalDia.medicoHospital.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Medico-Hospital</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Se borraran todos los turnos generados y dias de trabajo asignados para este Medico-Hospital.¿Esta seguro que desea eliminar?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                            <a href="<c:url value="/eliminar-hospitalAsignado?idMedicoHospital=${medicoHospitalDia.medicoHospital.id}"/>" class="btn btn-primary">Eliminar</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <h2>Este medico no tiene ningun hospital asignado</h2>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
    </body>
</html>

