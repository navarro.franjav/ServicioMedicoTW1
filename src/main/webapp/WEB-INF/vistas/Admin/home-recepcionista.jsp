<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 20/6/2023
  Time: 17:11
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerRecepcionista.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container mt-3">
  <div class="row">
    <div>
<a href="Cobros-Del-Dia">Caja del Día</a>
    </div>
    <div class="col-lg-12 text-center">
      <h1>Lista de Turnos de hoy</h1>
    </div>
    <div class="col-lg-12">
      <c:choose>
        <c:when test="${turnos.size() > 0}">

          <table class="table table-bordered dt-responsive nowrap w-100">
            <thead cclass="thead-dark">
            <th scope="col">
              Paciente
            </th>
            <th scope="col">
              Hospital
            </th>
            <th scope="col">
              Fecha y Hora
            </th>
            <th scope="col">
              Motivo
            </th>
            <th scope="col">
              Valor de la consulta
            </th>
            <th scope="col">

            </th>
            </thead>
            <tbody>
            <c:forEach items="${turnos}" var="turno">
              <tr>
            <td>  <c:out value="${turno.paciente.usuario.nombre} ${turno.paciente.usuario.apellido}"></c:out></td>

                <td><c:out value="${turno.medicoHospital.hospital.nombre}"></c:out></td>
                <td><c:out value="${turno.fecha} ${turno.hora}"></c:out></td>
                <td><c:out value="${turno.motivo}"></c:out></td>
                <td><c:out value="${turno.valorConsulta}"></c:out></td>
                <c:choose>
                <c:when test="${turno.pagado != true}">
                  <td>  <a href="#"data-bs-toggle="modal" data-bs-target="#ModalCobrar${turno.id}">Cobrar</a></td>

                </c:when>
                  <c:otherwise>
                <td>    <p>Cobrado</p></td>
                  </c:otherwise>
                </c:choose>


              </tr>

              <!-- Modal -->
              <div class="modal fade" id="ModalCobrar${turno.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Cobrar</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        ¿Confirma el pago?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                      <a href="<c:url value="/crear-cobro?idTurno=${turno.id}"/>" class="btn btn-primary">Confirmar</a>
                    </div>
                  </div>
                </div>
              </div>
            </c:forEach>
            </tbody>
          </table>
        </c:when>
        <c:otherwise>
          <h2>No hay ningún turno registrado para hoy</h2>
        </c:otherwise>
      </c:choose>
    </div>


  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
</body>
</html>
