<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="headerAdmi.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de Especialidades</h1>
        </div>
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="crear-especialidad">Nueva Especialidad</a>
            <c:choose>
                <c:when test="${especialidades.size() > 0}">
                    <table class="table table-bordered dt-responsive nowrap w-100">
                        <thead cclass="thead-dark">
                        <th scope="col">Id </th>
                        <th scope="col">Descripción </th>
                        <th scope="col">Costo </th>
                        </thead>
                        <tbody>
                        <c:forEach items="${especialidades}" var="especialidad">
                            <tr>
                                <td><c:out value="${especialidad.id}"></c:out></td>
                                <td><c:out value="${especialidad.descripcion}"></c:out></td>
                                <td><c:out value="${especialidad.costoEspecialidad}"></c:out></td>
                                <td>
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#ModalEliminar">Eliminar</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalEliminar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Especialidad</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    Se borrará la especialidad. ¿Está seguro que desea eliminar?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                    <a href="<c:url value="/eliminar-especialidad?id=${especialidad.id}"/>" class="btn btn-primary">Eliminar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="<c:url value="/modificar-especialidad?id=${especialidad.id}"/>" >Modificar</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2>No hay ninguna especialidad registrada</h2>
                    <a href="crear-especialidad">Crear Especialidad</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
<script>
    //capturar mensaje
    let mensaje = '<%= request.getAttribute("mensaje") %>';
    if (mensaje !== null && mensaje !== 'null') alert(mensaje)
</script>
</body>
</html>

