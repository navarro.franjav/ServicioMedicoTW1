<%--
  Created by IntelliJ IDEA.
  User: AndreaGiseleSosaOrti
  Date: 7/5/2023
  Time: 22:50
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="headerAdmi.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

</body>
    <div class="container mt-3">
        <h1>${titulo} Hospital</h1>
        <form:form action="${url}" method="POST" modelAttribute="datosHospital">
        <div class="row mt-3">
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label" for="nombre">Nombre</label>
                    <form:input path="nombre" type="text" class="form-control" id="nombre" name="nombre"  placeholder="Introduzca el nombre del Hospital" />
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label" for="direccion">Dirección</label>
                    <form:input path="direcion"  type="text" class="form-control" id="direccion" name="direccion"  placeholder="Introduzca la Dirección" />
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label" for="ciudad">Ciudad</label>
                    <form:input path="ciudad"  type="text" class="form-control" id="ciudad" name="ciudad"  placeholder="Introduzca la Ciudad" />
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label" for="codigoPostal">Código Postal</label>
                    <form:input path="codigoPostal"  type="text" class="form-control"  id="codigoPostal" name="codigoPostal"  placeholder="Introduzca el Código Postal" />
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label" for="telefono">Télefono</label>
                    <form:input path="telefono"  type="text" class="form-control" id="telefono" name="telefono"  placeholder="Introduzca el Teléfono" />
                </div>
            </div>
            <div class="col-6">
            </div>
            <form:input type="hidden" path="id"></form:input>
            <div class="col-6">
                <a href="lista-hospitales"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
            </div>
            <div class="col-6">
                <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
            </div>
        </div>
        </form:form>

    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>

<script>

    let mensaje = '<%= request.getAttribute("mensaje") %>';
    console.log(mensaje)
    if (mensaje != null && mensaje != 'null') {
        alert(mensaje);
    }

</script>

</body>
</html>