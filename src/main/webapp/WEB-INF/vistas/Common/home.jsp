<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>


<html lang="es">
<head>
    <script src="https://kit.fontawesome.com/d29453ffcf.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css">
    <link href="css/home3.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="css/home.css">
    <title>Servicio Médico</title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <ul class="navbar-nav">
                <li class="nav-item" style="color:white">
                    Linea de Emergencia: 011-2323-2323
                </li>
                <li class="nav-item ms-3" style="color:white">
                    Email: medicina@UNLAM.edu.ar
                </li>
            </ul>
            <div class="d-flex flex-row-reverse">
                <div class="p-2"><a><i class="fa-brands fa-facebook-f" style="color:white"></i></a></div>
                <div class="p-2"><a><i class="fa-brands fa-instagram" style="color:white"></i></a></div>
                <div class="p-2"><a><i class="fa-brands fa-whatsapp" style="color:white"></i></a></div>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a href="home-paciente">
                <img style="max-width:50px" class="mx-auto d-block" src="<c:url value="/Images/logo-unlam.png"/>"/>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="login"> Portal Ingreso </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#TitulosPlanes">Planes de Salud</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#encontranos">Encontranos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#contactanos">Contactanos</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<main>
    <div class="container py-2">
        <div class="row justify-content-center align-items-center">

            <div class="col-10">
                <h5 style="color:white; text-align: right; padding-top: 6px">CAMPAÑA DE VACUNACIÓN ANTIGRIPAL 2023</h5>
            </div>
            <div class="col-2">
                <button type="button" class="btn" style="background-color: #122b40; color: white; border-radius: 10px">Conocé Más</button>
            </div>
        </div>

    </div>


    <div id="carouselExample" class="carousel slide p-0 mb-5" style="max-height: 70vh">
        <div class="carousel-inner" style="max-height: 75vh">
            <div class="carousel-item active">
                <img src="<c:url value="/Images/cientificos-tiro-medio-batas-laboratorio.jpg"/>"
                     style="max-height: 75vh; object-fit: cover;object-position: top" class="d-block w-100"
                     alt="1">
                <div class="carousel-caption text-start row" style="color: black; bottom: 20vh">
                    <div class="col-6">
                        <h1>¿Querés asociarte?</h1>
                        <h3>Contratá la Mejor cobertura Médica para vos y tu familia</h3>
                        <button type="button" class="btn" style="background-color: #122b40; color: white">Conocé Más
                        </button>
                    </div>

                </div>
            </div>
            <div class="carousel-item">
                <img src="<c:url value="/Images/doctor-paciente.jpg"/>" class="d-block w-100"
                     style="max-height: 75vh; object-fit: cover; object-position: top" alt="2">
                <div class="carousel-caption text-start row" style="color: black; bottom: 20vh">
                    <div class="col-6">
                        <h1>¡Cuida tu salud y ahorra! </h1>
                        <h3> Descubre nuestros planes médicos accesibles y servicios hospitalarios de calidad</h3>
                        <button type="button" class="btn" style="background-color: #122b40; color: white">Conocé Más
                        </button>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img src="<c:url value="/Images/medico-enfermeras-equipos-especiales.jpg"/>"
                     style="max-height: 75vh; object-fit: cover; object-position: top" class="d-block w-100"
                     alt="3">
                <div class="carousel-caption text-start row" style="color: black; bottom: 20vh">
                    <div class="col-6">
                        <h1>Gestión hospitalaria eficiente y planes médicos personalizados.</h1>
                        <h3>Tu bienestar es nuestra prioridad.</h3>
                        <button type="button" class="btn" style="background-color: #122b40; color: white">Conocé Más
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>

    </div>

        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"

              crossorigin="anonymous">
    <div class="container my-4 mb-5" id="TitulosPlanes">
        <div class="row">
            <div class="col-12 text-center mt-3">
                <h1 style="color:white">PLANES</h1>
            </div>
            <c:forEach items="${listaDePlanes}" var="plan">
                <div class="d-flex col-4 justify-content-center">
                            <div class="container" >
                                <div class="card text-center" style="background-color: #1b6d85" >
                                    <div class="slide slide1">
                                        <div class="content">
                                            <div class="title">
                                                    <img src="<c:url value="/Images/plan${plan.id}.png"/>" class="card-img-top">
                                                    <h5 class="card-title"><c:out value="${plan.nombre}"></c:out></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide slide2">
                                        <div class="content">
                                            <p>
                                                <c:out value="${plan.descripcion}"></c:out>
                                            </p>
                                            <p>$<c:out value="${plan.precio}"></c:out></p>
                                            <p>
                                                Incluye:
                                                <c:forEach items="${listaDeEspecialidades}" var="especialidad">
                                                    <c:if test="${plan.id == especialidad.plan.id}">
                                                        <c:out value="${especialidad.especialidad.descripcion}"></c:out>
                                                    </c:if>
                                                </c:forEach>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>

    <div class="container" id="encontranos">
        <div class="row mb-5">
            <h2  class="mb-4" style="color: white;text-align: center">¡Nos encontrarmos acá para vos!</h2>
            <div class="col-6" style="color:white">
                <p>Nos complace informarte que nuestra central se encuentra ubicada en Florencio Varela 1903, B1754 San Justo, Provincia de Buenos Aires. Estamos comprometidos en ofrecerte servicios de calidad en la gestión de planes médicos, hospitales y profesionales de la salud.</p>
                <p>Nuestro horario de atención es de 10:00 am a 20:00 pm, de lunes a viernes. Estamos disponibles para brindarte la asistencia y la orientación que necesites en cuanto a tus planes médicos, consultas hospitalarias y servicios de profesionales de la salud.</p>
                <p>Si tienes alguna pregunta o necesitas más información, no dudes en visitarnos en nuestra ubicación mencionada anteriormente o comunicarte con nuestro equipo de atención al paciente. Estamos aquí para ayudarte a cuidar de tu salud y brindarte un servicio excepcional.</p>
                <p>¡Esperamos poder servirte y ser tu aliado en el cuidado de tu bienestar!</p>
            </div>
            <div class="col-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3281.3641589074264!2d-58.564993884768576!3d-34.67075758044217!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcc62cc3ef7083%3A0x8867107f425fade5!2sUniversidad%20Nacional%20de%20La%20Matanza!5e0!3m2!1ses!2sar!4v1685573802053!5m2!1ses!2sar" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </div>

    <div class="container-fluid" style="background-color: #424C59">

        <div class="container" style="color: white" >
            <div class="row py-2">
                <div class="col-12 text-center pt-3 mb-4">
                    <h3>Para casos de emergencia 0800-2323-2323 </h3>
                </div>
                <div class="col-6" id="contactanos">
                    <h5>Dejanos tu mensaje</h5>
                    <form id="formularioConsulta" class="d-flex-column">
                        <div>
                            <label class="form-label">Email</label>
                            <input class="form-control" type="email" placeholder="Ingrese su email">
                        </div>
                        <div>
                            <label class="form-label">Consulta</label>
                            <textarea class="form-control" type="textarea" rows="4"
                                      placeholder="Escriba su mensaje"></textarea>
                        </div>
                        <div class="d-flex justify-content-end mt-1">
                            <button class="btn" style="background-color: #122b40; color: white; border-radius: 10px" type="submit">Enviar
                            </button>
                        </div>

                    </form>
                </div>
                <div class="col-6">
                    <p>Ponerse en contacto</p>
                    <p>Florencio Varela 1903, B1754 San Justo, Provincia de Buenos Aires</p>
                    <p>11-2323-2323</p>
                    <p>medicina@unlam.edu.ar</p>
                </div>
            </div>
        </div>

    </div>

    <a class="mensaje"><i class="fa-solid fa-message"></i></a>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.center').slick({
                centerMode: true,
                centerPadding: '60px',
                arrows: false,
                slidesToShow: 3,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    </script>
</main>
<footer>
    <p id="textoFooter">Taller Web 1. Aquino Mariana - Lickey Pablo - Navarro Francisco - Sosa Ortiz Andrea </p>
</footer>
</body>

</html>