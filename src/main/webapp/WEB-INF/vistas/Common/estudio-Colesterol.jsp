<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container" id="contentidoAImprimir">
    <div class="row">
        <div class="col-8">
            <h1>Estudio ${estudio.estudio.tipoEstudio.nombreEstudio}</h1>
        </div>
        <div class="col-6">
            <h5>
                Nombre paciente: ${estudio.estudio.paciente.usuario.apellido}, ${estudio.estudio.paciente.usuario.nombre}
            </h5>
        </div>
        <div class="col-6">
            <h5>
                Nombre medico: ${estudio.estudio.medico.usuario.apellido}, ${estudio.estudio.medico.usuario.nombre}
            </h5>
        </div>
        <div class="col-6">
            <h5>
                Fecha: ${estudio.estudio.fecha}
            </h5>
        </div>
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Estudio</th>
                    <th scope="col">Resultado</th>
                    <th scope="col">Unidades</th>
                    <th scope="col">Referencias</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Colesterol LDL</td>
                    <td>${estudio.colesterolLDL}</td>
                    <td>mg/dL</td>
                    <td>- Hasta 21 años: Deseable: Menor de 110 En el límite: 110 a 129 No deseable: Mayor/igual a 130 - Mayor de 21 años: Deseable: Menor de 130 En el límite: 130 a 159 No deseable: Mayor/igual a 160</td>
                </tr>
                <tr>
                    <td>Colesterol Total</td>
                    <td>${estudio.colesterolTotal}</td>
                    <td>mg/dL</td>
                    <td>- Hasta 21 años: Deseable: Menor de 170 En el límite: 170 a 199 No deseable: Mayor/igual a 200 - Mayor de 21 años: Deseable: Menor de 200 En el límite: 200 a 239 No deseable: Mayor/igual a 240</td>
                </tr>
                <tr>
                    <td>Trigliceridos</td>
                    <td>${estudio.trigliceridos}</td>
                    <td>mg/dL</td>
                    <td>- Hasta 21 años: Deseable: Menor de 125 No deseable: Mayor/igual a 160 - Mayor de 21 años: Deseable: Menor de 150 En el límite: 150 a 199 No deseable: Mayor/igual a 200</td>
                </tr>
                <tr>
                    <td>Colesterol HDL</td>
                    <td>${estudio.colesterolHDL}</td>
                    <td>mg/dL</td>
                    <td>- Hasta 21 años: Deseable: Mayor de 45 En el límite: 35 a 45 No deseable: Menor de 35 - Mayor de 21 años: Deseable: Mayor o igual de 40 No deseable: Menor de 40</td>
                </tr>
                <tr>
                    <td>Colesterol No HDL</td>
                    <td>${estudio.colesterolNoHDL}</td>
                    <td>mg/dL</td>
                    <td>Adultos: Deseable: Menor de 130 En el límite: Menor de 160 No deseable: Mayor de 190</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>