<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container" id="contentidoAImprimir">
    <div class="row">
        <div class="col-8">
            <h1>Estudio ${estudio.estudio.tipoEstudio.nombreEstudio}</h1>
        </div>
        <div class="col-6">
            <h5>
                Nombre
                paciente: ${estudio.estudio.paciente.usuario.apellido}, ${estudio.estudio.paciente.usuario.nombre}
            </h5>
        </div>
        <div class="col-6">
            <h5>
                Nombre medico: ${estudio.estudio.medico.usuario.apellido}, ${estudio.estudio.medico.usuario.nombre}
            </h5>
        </div>
        <div class="col-6">
            <h5>
                Fecha: ${estudio.estudio.fecha}
            </h5>
        </div>
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Estudio</th>
                    <th scope="col">Resultado</th>
                    <th scope="col">Unidades</th>
                    <th scope="col">Referencias</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Bilirrubina Total</td>
                    <td>${estudio.bilirrubinaTotal}</td>
                    <td>(H)mg/dL</td>
                    <td>0.10 - 1.40</td>
                </tr>
                <tr>
                    <td>Bilirrubina Directa</td>
                    <td>${estudio.bilirrubinaDirecta}</td>
                    <td>mg/dL</td>
                    <td>0.00 - 0.40</td>
                </tr>
                <tr>
                    <td>Fosfotasas Alcalinas</td>
                    <td>${estudio.fosfatasasAlcalinas}</td>
                    <td>UI/L</td>
                    <td>31 - 100</td>
                </tr>
                <tr>
                    <td>Colesterol Total</td>
                    <td>${estudio.colesterolTotal}</td>
                    <td>mg/dL</td>
                    <td>- Hasta 21 años: Deseable: Menor de 170 En el límite: 170 a 199 No deseable: Mayor/igual a 200 -
                        Mayor de 21 años: Deseable: Menor de 200 En el límite: 200 a 239 No deseable: Mayor/igual a 240
                    </td>
                </tr>
                <tr>
                    <td>Proteinas Totales</td>
                    <td>${estudio.proteinasTotales}</td>
                    <td>g/dL</td>
                    <td>6.30 - 7.80</td>
                </tr>
                <tr>
                    <td>Albumina</td>
                    <td>${estudio.albumina}</td>
                    <td>g/dL</td>
                    <td>3.20 - 5.00</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>