<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <script src="https://kit.fontawesome.com/d29453ffcf.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/home3.css" rel="stylesheet">
</head>
<body>
<div id="barraSuperior">
    <div class="encabezadoElementos">
        <p id="Emergencia">Linea de Emergencia: 011-2323-2323</p>


    </div>
    <div class="encabezadoElementos">

        <p id="EmergenciaMail">Email: medicina@UNLAM.edu.ar</p>

    </div>
    <div class="encabezadoElementos">
        <a id="facebook"><i class="fa-brands fa-facebook-f"></i></a>
        <a id="insta"><i class="fa-brands fa-instagram"></i></a>
        <a id="twitter"><i class="fa-brands fa-whatsapp"></i></a>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">UNLAM</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="login"> Portal Ingreso </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#TitulosPlanes">Planes de Salud</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#emerg">Contactenos</a>
                </li>

            </ul>
        </div>
    </div>
</nav>

<div class="CAMPANA">
    <p class="parrafoCampana">CAMPANA DE VACUNACIÓN ANTIGRIPAL 2023</p>
    <button class="vacunacion">Conocé Más</button>
</div>

<div class="carrusel">
    <div id="carouselExample" class="carousel slide">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<c:url value="/Images/cientificos-tiro-medio-batas-laboratorio.jpg"/>" class="d-block w-100" alt="1">
            </div>
            <div class="carousel-item">
                <img src="<c:url value="/Images/doctor-paciente.jpg"/>" class="d-block w-100" alt="2">
            </div>
            <div class="carousel-item">
                <img src="<c:url value="/Images/medico-enfermeras-equipos-especiales.jpg"/>" class="d-block w-100" alt="3">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
        <div class="parrafoVentaCarrusel">
            <p id="pregunta">¿Querés asociarte?</p>
            <p id="texto">Contratá la Mejor cobertura Médica para vos y tu familia</p>
            <button id="conoceMas">Conocé Más</button>
        </div>
    </div>

</div>
<div id="padrePlanes">
    <p id="TitulosPlanes">PLANES</p>
    <div class="row divpadre">
        <div class="card three-column-summary" style="width: 25rem;" >
            <img src="<c:url value="/Images/instagram.png"/>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Contratar</a>
            </div>
        </div>
        <div class="card three-column-summary" style="width: 25rem;">
            <img src="<c:url value="/Images/facebook.png"/>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Contratar</a>
            </div>
        </div>
        <div class="card three-column-summary" style="width: 25rem;">
            <img src="<c:url value="/Images/gorjeo.png"/>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Contratar</a>
            </div>
        </div>
    </div>
</div>
<div id="emerg">
    <p>Para casos de emergencia 0800-2323-2323</p></div>
<div id="padreContacto">

    <div class="mensajecontacto" id="contacto">
        <p>Dejanos tu Mensaje</p>
        <form id="formulario">
            <input>
        </form>
        <button type="submit">Enviar</button>
    </div>
    <div class="mensajecontacto" >
        <p>Ponerse en contacto</p>
        <p>Florencio Varela 1903, B1754 San Justo, Provincia de Buenos Aires</p>
        <p>11-2323-2323</p>
        <p>medicina@unam.edu.ar</p>


    </div>

</div>
<a class="mensaje"><i class="fa-solid fa-message"></i></a>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" ></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
    <p id="textoFooter">Taller Web 1. Sosa Ortiz Andrea - Navarro Francisco - Aquino Mariana - Lickey Pablo</p>
</footer>
</html>