<div class="container" id="contentidoAImprimir">
  <div class="row">
    <div class="col-8">
      <h1>${estudio.estudio.tipoEstudio.nombreEstudio}</h1>
    </div>
    <div class="col-6">
      <h5>
        Nombre paciente: ${estudio.estudio.paciente.usuario.apellido}, ${estudio.estudio.paciente.usuario.nombre}
      </h5>
    </div>
    <div class="col-6">
      <h5>
        Nombre medico: ${estudio.estudio.medico.usuario.apellido}, ${estudio.estudio.medico.usuario.nombre}
      </h5>
    </div>
    <div class="col-6">
      <h5>
        Fecha: ${estudio.estudio.fecha}
      </h5>
    </div>
    <div class="col-12">
      <table class="table">
        <thead>
        <tr>
          <th scope="col">Estudio</th>
          <th scope="col">Resultado</th>
          <th scope="col">Unidades</th>
          <th scope="col">Referencias</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Hemoglobina sangre total</td>
          <td>${estudio.hemoglobinaSangreTotal}</td>
          <td>g/dL</td>
          <td>13.0 - 17.0</td>
        </tr>
        <tr>
          <td>Hematocrito</td>
          <td>${estudio.hematocrito}</td>
          <td>%</td>
          <td>40.0 - 53.0</td>
        </tr>
        <tr>
          <td>Basofilos</td>
          <td>${estudio.basofilos}</td>
          <td>%</td>
          <td>0.00 - 1.00</td>
        </tr>
        <tr>
          <td>Eosinofilos</td>
          <td>${estudio.eosinofilos}</td>
          <td>(H)%</td>
          <td>2.00 - 4.00</td>
        </tr>
        <tr>
          <td>Linfocitos</td>
          <td>${estudio.linfocitos}</td>
          <td>(L)%</td>
          <td>30.00 - 40.00</td>
        </tr>
        <tr>
          <td>Monocitos</td>
          <td>${estudio.monocitos}</td>
          <td>(L)%</td>
          <td>4.00 - 12.00</td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>