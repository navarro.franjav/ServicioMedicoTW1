<%--
  Created by IntelliJ IDEA.
  User: Pablo
  Date: 10/06/2023
  Time: 13:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="navbar.jsp"%>
<html>
<head>
    <title>Nuevo Turno</title>
    <style>
        .form-select{
            font-size: 1.5rem;
        }
    </style>
</head>
    <body>

        <div class="container pt-10">
            <h1>Nuevo Turno</h1>
            <div class="d-flex justify-content-center pt-5">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12 mt-5 mb-3">
                            <label for="especialidadId" class="form-label">Seleccione una Especialidad</label>
                            <select  id="especialidadId" class="form-select" name="especialidadId">
                                <option value="">Seleccione Especialidad</option>
                                <c:forEach items="${especialidades}" var="especialidad">
                                    <option value=<c:out value="${especialidad.id}"></c:out>><c:out value="${especialidad.descripcion}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div id="mensajeError" style="display: none">
                            <h3>No hay turnos disponibles para esta especialidad</h3>
                        </div>
                        <form:form action="asignar-turno" method="POST" modelAttribute="datosAsignarTurno" id="form" style="visibility:hidden">
                            <div class="col-12 mb-3">
                                <label for="turnoId" class="form-label">Seleccione una Horario</label>
                                <select  class="form-select" name="turnoId" id="turnoId"></select>
                            </div>
                            <div class="col-12">
                                <label for="motivo" class="form-label">Motivo de consulta</label>
                                <input required class="form-control" type="text" id="motivo" name="motivo">
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>


        <script>
            window.onload = function () {
                let turnos = JSON.parse('${turnos}')

                function filtrarTurnosPorEspecialidad() {
                    const especialidadId = selectEspecialidad.value;

                    const turnosFiltrados = turnos.filter(turno => turno.medicoHospital.especialidad.id == especialidadId);

                    const selectTurno = document.getElementById("turnoId");
                    selectTurno.innerHTML = "";

                    let mensajeError = document.getElementById("mensajeError");

                    let form = document.getElementById("form");

                    if(turnosFiltrados.length>0){
                        form.style.visibility="unset";
                        mensajeError.style.display = "none";
                        console.log(turnosFiltrados)
                        turnosFiltrados.forEach(turno => {
                            const opcion = document.createElement("option");
                            let diaTurno = turno.fecha.dayOfMonth.toString();
                            let mesTurno = turno.fecha.monthValue.toString();
                            let añoTurno = turno.fecha.year.toString();
                            let horaTurno = turno.hora.hour.toString().padStart(2, '0');
                            let minutoTurno = turno.hora.minute.toString().padStart(2, '0');
                            let horaMinutoTurno = horaTurno + ':' + minutoTurno;
                            let hospitalTurno = turno.medicoHospital.hospital.nombre;
                            opcion.value = turno.id;
                            opcion.textContent = "Fecha: "+diaTurno+"/"+mesTurno+"/"+añoTurno+" - Hora:"+horaMinutoTurno+" - Hospital:"+hospitalTurno;
                            selectTurno.appendChild(opcion);
                        });
                    }else{
                        form.style.visibility="hidden";
                        mensajeError.style.display = "unset";
                    }

                }

                let selectEspecialidad = document.getElementById("especialidadId");

                selectEspecialidad.onchange = filtrarTurnosPorEspecialidad;


            };
        </script>

    </body>
</html>
