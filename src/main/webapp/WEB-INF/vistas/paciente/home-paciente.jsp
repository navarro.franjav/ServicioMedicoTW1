<%--
  Created by IntelliJ IDEA.
  User: FranciscoJavierNavar
  Date: 4/26/2023
  Time: 10:31 PM
  To change this template use File | Settings | File Templates.
--%>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ include file="navbar.jsp"%>
            <html>

            <head>
                <title>Home</title>
            </head>

            <body>
            <h2 class="bg-danger text-white" id="mensajeNoActivo" style="display: none">Su estado pasará a Activo una vez abonada la factura mensual, luego de un periodo de hasta 24 hs.</h2>
            <div class="container">
                    <div class="row">
                        <div class="col-3 text-center text-white pt-5" style="background-color: #1b6d85; border-radius: 20px;">
                            <div class="d-flex justify-content-center align-items-center mb-5">
                                <div class="d-block rounded-circle" style="width: 125px; height: 125px; overflow: hidden;">
<%--                                    <img src="https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="profile" style="max-width: 100%;">--%>
                                    <img src="<c:url value="/Images/paciente.jpg"/>" alt="profile" style="max-width: 100%;">
                                </div>
                            </div>
                            <h5 class="mt-5"><c:out value="${paciente.usuario.nombre}" /> <c:out value="${paciente.usuario.apellido}" /></h5>
                            <h5>DNI: <c:out value="${paciente.usuario.dni}" /></h5>
                            <h5>Tel: <c:out value="${paciente.usuario.telefono}" /></h5>
                            <h5>Plan: <c:out value="${paciente.plan.nombre}" /></h5>
                            <h5 class="bg-black">Estado : <span id="estado"></span></h5>

                        </div>
                        <div class="col-5" style="padding-inline: 4rem;">
                            <div class="row">
                                <div class="col-8">
                                    <h3>Próximos turnos</h3>
                                </div>
                                <div class="col-4 pt-3">
                                    <a class="btn btn-primary btn-block" id="nuevoTurno" href="asignar-turno">Nuevo Turno</a>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Motivo</th>
                                        <th>Médico</th>
                                        <th>Lugar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${turnos}" var="turno">
                                        <tr>
                                            <td><c:out value="${turno.fecha}"></c:out></td>
                                            <td><c:out value="${turno.motivo}"></c:out></td>
                                            <td><c:out value="${turno.medicoHospital.medico.usuario.nombre}">,</c:out><c:out value="${turno.medicoHospital.medico.usuario.apellido}"></c:out></td>
                                            <td><c:out value="${turno.medicoHospital.hospital.nombre}"></c:out></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="col-8">
                                    <h3>Últimos Estudios</h3>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Nombre de estudio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${estudios}" var="estudio">
                                    <tr>
                                        <td>${estudio.fecha}</td>
                                        <td>${estudio.tipoEstudio.nombreEstudio}</td>
                                        <td><a href="paciente-ver-estudio-${estudio.tipoEstudio.nombreEstudio}?idEstudio=${estudio.id}">Ver</a></td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            <script>
                document.addEventListener("DOMContentLoaded", function(event) {
                    let nuevoTurno = document.getElementById("nuevoTurno");
                    let mensaje = document.getElementById("mensajeNoActivo")
                    let estado = document.getElementById("estado");
                    if(!${paciente.activo}){
                        nuevoTurno.style.display="none";
                        estado.innerText = "INACTIVO";
                        estado.classList.add("text-danger");
                        mensaje.style.display="block";
                    }else{
                        estado.innerText = "Activo";
                        estado.classList.add("text-success");
                    }

                });
            </script>

            </body>

            </html>