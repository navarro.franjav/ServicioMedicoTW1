<%--
  Created by IntelliJ IDEA.
  User: Pablo
  Date: 30/05/2023
  Time: 21:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="navbar.jsp"%>
<html>
    <head>
        <title>Plan</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <h3>Cambiar Plan</h3>
                    <form:form action="cambiar-plan" method="POST" modelAttribute="datosPacientePlan">
                        <input required type="hidden"  value="${paciente.id}" class="form-control" id="pacienteId" name="pacienteId" >
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label for="planId" class="form-label">Plan</label>
                                    <select class="form-select" style="font-size: unset;" aria-label="Default select example" id="planId" name="planId">
                                        <option value=null selected disabled>Seleccione Plan</option>
                                        <c:forEach items="${planes}" var="plan">
                                            <option value=<c:out value="${plan.id}"></c:out>><c:out value="${plan.nombre}"></c:out></option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                            </div>
                            <div class="col-6">
                                <a href="${pageContext.request.contextPath}/"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
                            </div>
                        </div>
                    </form:form>
                </div>
                <div class="col-8">
                    <h3>Mi Plan</h3>
                    <hr>
                    <h4>${paciente.plan.nombre} <span class="text-info" style="font-size:14px">$${paciente.plan.precio} Finales Por Mes</span></h4>
                    <p>${paciente.plan.descripcion}</p>
                    <hr>
                    <h4>Lista de especialidades</h4>
                    <ul>
                        <c:forEach items="${listaEspecialidades}" var="especialidad">
                            <c:if test="${paciente.plan.id==especialidad.plan.id}">
                                <li>
                                    <c:out value="${especialidad.especialidad.descripcion}"></c:out>
                                </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
