<%--
  Created by IntelliJ IDEA.
  User: maria
  Date: 27/5/2023
  Time: 18:53
  To change this template use File | Settings | File Templates.
--%>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

            <html>

            <head>
                <script src="https://kit.fontawesome.com/d29453ffcf.js" crossorigin="anonymous"></script>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
                      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
                <!-- Bootstrap core CSS -->
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css">
                <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
                <style>
                    #barraSuperior{
                        background-color: #1b6d85;
                        color: white;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                        position: relative;
                    }
                    #Emergencia{
                        padding-left: 5%;
                        max-width: max-content;
                    }
                    .encabezadoElementos{
                        width: 50%;
                    }
                    #facebook{
                        position: absolute;
                        top: 0;
                        right: 10%;
                    }
                    #insta{
                        position: absolute;
                        top: 0;
                        right: 12%;
                    }
                    #twitter{
                        position: absolute;
                        right: 14%;
                        top: 0;

                    }

                </style>
            </head>

            <body>
                <header>
                    <div id="barraSuperior">
                        <div class="encabezadoElementos pt-2">
                            <p id="Emergencia">Linea de Emergencia: 011-2323-2323</p>
                        </div>
                        <div class="encabezadoElementos">
                            <a id="facebook" style="top:6px!important;"><i class="fa-brands fa-facebook-f"></i></a>
                            <a id="insta" style="top:6px!important;"><i class="fa-brands fa-instagram"></i></a>
                            <a id="twitter" style="top:6px!important;"><i class="fa-brands fa-whatsapp"></i></a>
                        </div>
                    </div>
                </header>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <a href="home-paciente">
      <img width="50px" class="mx-auto d-block" src="<c:url value="/Images/logo-unlam.png"/>"/>
    </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="home-paciente">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="mis-turnos">Mis Turnos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="mi-plan">Mi Plan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="mis-estudios">Mis Estudios</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="mi-perfil">Mi Perfil</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="mis-facturas">Mis facturas</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="mi-historial">Historial Clínico</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="cerrar-sesion">Cerrar Sesión</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </body>

            </html>