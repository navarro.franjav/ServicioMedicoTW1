<%--
  Created by IntelliJ IDEA.
  User: Pablo
  Date: 25/06/2023
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="navbar.jsp"%>
<html>
<head>
    <title>Mis Facturas</title>
</head>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Lista de Facturas</h1>
        </div>
        <div class="col-lg-12">
            <c:choose>
                <c:when test="${facturas.size() > 0}">
                    <table class="table table-bordered dt-responsive nowrap w-100">
                        <thead cclass="thead-dark">
                        <th scope="col"> Código </th>
                        <th scope="col"> Emisión </th>
                        <th scope="col"> Vencimiento </th>
                        <th scope="col"> Valor </th>
                        <th scope="col"> Condición </th>
                        <th scope="col"> Control </th>
                        </thead>
                        <tbody>
                        <c:forEach items="${facturas}" var="factura">
                            <tr>
                                <td><c:out value="${factura.codigo}"></c:out></td>
                                <td><c:out value="${factura.emision}"></c:out></td>
                                <td><c:out value="${factura.vencimiento}"></c:out></td>
                                <td>$ <c:out value="${factura.valor}"></c:out></td>
                                <c:choose>
                                    <c:when test="${factura.pago == true}">
                                        <td class="text-success">Paga</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="text-danger">Impaga</td>
                                        <td><a href="javascript:void(0)" onclick="pagar(${factura.id})" >Pagar</a></td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <h2>Usted no posee ningúna factura registrada</h2>
                </c:otherwise>
            </c:choose>
        </div>


    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function pagar(facturaId){
        Swal.fire({
            title: '¿Estás Seguro?',
            text: "El pago no puede ser cancelado y no es reembolsable",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, estoy seguro',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "pagar-factura?id=" + facturaId;
            }
        })
    }
</script>
</body>
</html>
