<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="navbar.jsp"%>
<main>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-2">
                <button onclick="convertirAPDF()" class="btn btn-secondary">Descargar estudio</button>
            </div>
        </div>
    </div>

    <c:choose>
        <c:when test="${estudio.estudio.tipoEstudio.nombreEstudio.equals('Hemograma')}">
            <%@ include file="../Common/estudio-Hemograma.jsp" %>
        </c:when>
        <c:when test="${estudio.estudio.tipoEstudio.nombreEstudio.equals('Colesterol')}">
            <%@ include file="../Common/estudio-Colesterol.jsp" %>
        </c:when>
        <c:when test="${estudio.estudio.tipoEstudio.nombreEstudio.equals('Hepatograma')}">
            <%@ include file="../Common/estudio-Hepatograma.jsp" %>
        </c:when>
    </c:choose>

</main>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.1/dist/html2canvas.min.js"></script>
<script>
    window.jsPDF = window.jspdf.jsPDF;

    function convertirAPDF() {
        var doc = new jsPDF();

        var elementHTML = document.querySelector("#contentidoAImprimir");

        let nombreEstudio = "${estudio.estudio.tipoEstudio.nombreEstudio}";
        let nombrePaciente = "${estudio.estudio.paciente.usuario.apellido}${estudio.estudio.paciente.usuario.nombre}";
        nombrePaciente = nombrePaciente.replace(/\s/g, '');
        doc.html(elementHTML, {
            callback: function (doc) {
                doc.save('Estudio' + nombreEstudio + '-' + nombrePaciente + '.pdf');
            },
            margin: [10, 10, 10, 10],
            autoPaging: 'text',
            x: 0,
            y: 0,
            width: 190, //target width in the PDF document
            windowWidth: 675 //window width in CSS pixels
        });
    }

</script>

</body>
</html>
