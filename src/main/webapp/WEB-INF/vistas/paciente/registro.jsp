<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Pablo
  Date: 21/06/2023
  Time: 17:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registro</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/header.css" rel="stylesheet">
</head>
    <body>
        <div class="container">
            <h1>Registro</h1>
            <form:form action="registrar-paciente" method="POST" modelAttribute="datosPaciente">
                <input type="hidden" name="activo" id="activo" value="false">
                <div class="row mt-3">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input required type="text"  class="form-control" id="nombre" name="nombre" placeholder="Ingrese el Nombre">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="apellido" class="form-label">Apellido</label>
                            <input required type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingrese el Apellido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input required type="email" class="form-control" id="email" name="email" placeholder="Ingrese Email">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="telefono" class="form-label">Teléfono</label>
                            <input required type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ingrese Teléfono">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="password" class="form-label">Contraseña</label>
                            <input required type="password" min="1" max="10" class="form-control" id="password" name="password" placeholder="Ingrese Contraseña">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="cp" class="form-label">Código Postal</label>
                            <input required type="number" class="form-control" id="cp" name="cp" placeholder="Ingrese Código Postal">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="dni" class="form-label">DNI</label>
                            <input required type="number" class="form-control" id="dni"  name="dni" placeholder="Ingrese DNI">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fechanac" class="form-label">Fecha de Nacimiento</label>
                            <input required type="date" class="form-control" id="fechanac" name="fechanac" placeholder="Seleccione Fecha de Nacimiento">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="planId" class="form-label">Plan</label>
                            <select class="form-select" style="font-size: unset;" aria-label="Default select example" id="planId" name="planId">
                                <option value=null selected disabled>Seleccione Plan</option>
                                <c:forEach items="${planes}" var="plan">
                                    <option value=<c:out value="${plan.id}"></c:out>><c:out value="${plan.nombre}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="col-6">
                        <a href="${pageContext.request.contextPath}/"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                    </div>
                </div>
            </form:form>
        </div>
        <script src="https://kit.fontawesome.com/d29453ffcf.js" crossorigin="anonymous"></script>
    </body>
</html>
