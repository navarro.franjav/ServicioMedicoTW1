<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="navbar.jsp"%>
<main>
    <div class="container mt-3">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Historial Clínico</h1>
            </div>
            <div class="col-lg-12">
                <c:choose>
                    <c:when test="${historial.size() > 0}">
                        <table class="table table-bordered dt-responsive nowrap w-100">
                            <thead cclass="thead-dark">
                            <th scope="col"> Fecha </th>
                            <th scope="col"> Motivo </th>
                            <th scope="col"> Diagnostico </th>
                            <th scope="col"> Profesional </th>
                            <th scope="col">  </th>
                            </thead>
                            <tbody>
                            <c:forEach items="${historial}" var="registro">
                                <tr>
                                    <td><c:out value="${registro.fecha}"/></td>
                                    <td><c:out value="${registro.motivo}"></c:out></td>
                                    <td><c:out value="${registro.diagnostico}"></c:out></td>
                                    <td><c:out value="${registro.medico.usuario.apellido}, ${registro.medico.usuario.nombre}"></c:out></td>
                                    <td><a href="mi-registro-clinico?codigoRegistro=${registro.codigo}">Ver Registro</a></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <h2>No tiene ningun registro cargado</h2>
                    </c:otherwise>
                </c:choose>
            </div>


        </div>
    </div>




</main>

</body>
</html>
