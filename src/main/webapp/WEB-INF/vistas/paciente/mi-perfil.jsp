<%--
  Created by IntelliJ IDEA.
  User: Pablo
  Date: 30/05/2023
  Time: 21:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="navbar.jsp"%>
<html>
<head>
    <title>Perfil</title>
</head>
<body>
    <div class="container mt-3">
        <h1>Editar Cliente</h1>
        <form:form action="editar-perfil" method="POST" modelAttribute="datosPaciente">
            <input required type="hidden"  value="${datosPaciente.idPaciente}" class="form-control" id="idPaciente" name="idPaciente" >
            <input required type="hidden"  value="${datosPaciente.planId}" class="form-control" id="planId" name="planId" >
            <div class="row mt-3">
                <div class="col-6">
                    <div class="mb-3">
                        <label for="nombre" class="form-label">Nombre</label>
                        <input required type="text"  value="${datosPaciente.nombre}" class="form-control" id="nombre" name="nombre" placeholder="Ingrese el Nombre">
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="apellido" class="form-label">Apellido</label>
                        <input required type="text" value="${datosPaciente.apellido}" class="form-control" id="apellido" name="apellido" placeholder="Ingrese el Apellido">
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input required type="email" value="${datosPaciente.email}" class="form-control" id="email" name="email" placeholder="Ingrese Email">
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="telefono" class="form-label">Teléfono</label>
                        <input required type="tel" value="${datosPaciente.telefono}" class="form-control" id="telefono" name="telefono" placeholder="Ingrese Teléfono">
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="password" class="form-label">Contraseña</label>
                        <input required type="password" value="${datosPaciente.password}" class="form-control" id="password" name="password" placeholder="Ingrese Contraseña">
                    </div>
                </div>
                <div class="col-3">
                    <div class="mb-3">
                        <label for="cp" class="form-label">Código Postal</label>
                        <input required type="number" value="${datosPaciente.cp}" class="form-control" id="cp" name="cp" placeholder="Ingrese Código Postal">
                    </div>
                </div>
                <div class="col-3">
                    <div class="mb-3">
                        <label for="dni" class="form-label">DNI</label>
                        <input required type="number" class="form-control" value="${datosPaciente.dni}" id="dni"  name="dni" placeholder="Ingrese DNI">
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="fechanac" class="form-label">Fecha de Nacimiento</label>
                        <input required type="date" class="form-control" value="${datosPaciente.fechanac}" id="fechanac" name="fechanac" placeholder="Seleccione Fecha de Nacimiento">
                    </div>
                </div>

                <div class="col-6">
                    <button type="submit" class="btn btn-info  mt-3 col-12">Confirmar</button>
                </div>
                <div class="col-6">
                    <a href="home-paciente"> <button type="button" class="btn btn-danger mt-3 col-12" >Cancelar</button></a>
                </div>
            </div>
        </form:form>
    </div>
</body>
</html>
